#pragma once
#include "octree/octree.h"
#include "octree/util.h"
#include <algorithm>
#include <boost/variant.hpp>

namespace octree {

enum MetaOctreeType {
  INVALID=-1,
  EMPTY=0,
  POINT_DATA_V1=1,
  POINT_DATA_V2=2,
  ADE_SEG=3,
};

MetaOctreeType stoe(std::string const& s);
std::string etos(MetaOctreeType t);

struct PointDataV1 {
  public:
    float score;
    float size;
    float x, y, z;
    int32_t count;
};

struct PointDataV2 {
  public:
    float score;
    float size;
    float x, y, z;
    int32_t count;
    float val;
};

struct ADESeg {
  public:
    float probs[150];
    int32_t count;
};

class MetaOctree {
  public:
    // Data
    Octree tree;
    std::vector<OctreeKey> metamap;
    boost::variant<std::vector<PointDataV1>, std::vector<PointDataV2>, std::vector<ADESeg>> metadata_var;
    MetaOctreeType datatype;

    // Functions

    MetaOctree(float scale, float x, float y, float z, MetaOctreeType type=EMPTY) : tree(scale, {x, y, z}), datatype(type) {
      tree.version = "metaoctree-score-size-count";
      initializeType(datatype);
    }
    MetaOctree(float scale=0.05, vec3 origin={0, 0, 0}, MetaOctreeType type=EMPTY) : tree(scale, origin), datatype(type) {
      tree.version = "metaoctree-score-size-count";
      initializeType(datatype);
    };
    MetaOctree(std::string fname, size_t max_chunk_size=2e9);
    int save(std::string fname, size_t max_chunk_size=2e9);
    void initializeType(MetaOctreeType t);
    size_t getTypeSize() {
      switch (datatype) {
        case POINT_DATA_V1:
          return sizeof(PointDataV1);
        case POINT_DATA_V2:
          return sizeof(PointDataV2);
        case ADE_SEG:
          return sizeof(ADESeg);
        default:
          return 0;
      }
    }
    void *metadata() {
      switch (datatype) {
        case POINT_DATA_V1:
          return boost::get<std::vector<PointDataV1>>(metadata_var).data();
        case POINT_DATA_V2:
          return boost::get<std::vector<PointDataV2>>(metadata_var).data();
        case ADE_SEG:
          return boost::get<std::vector<ADESeg>>(metadata_var).data();
        default:
          return NULL;
      }
    }
    size_t metadata_size() { return metamap.size() * getTypeSize(); }
    // Initialize from existing octree. Unimplemented
    // MetaOctree(Octree tree);

    // Simple functions that route to octree base
    bool withinBounds(vec3 pt) {return tree.withinBounds(pt);};
    float getRadius() {return tree.getRadius();};
    int getDepth() {return tree.depth;}
    float getScale() {return tree.scale;}
    vec3 getOrigin() {return tree.origin;}
    size_t numNodes() {return tree.numNodes();};
    OctreeNode * getNode(int64_t index) {return tree.getNode(index);};
    void incrementDepth() {tree.incrementDepth();};
    Octree::OctreeIterator begin() {return tree.begin();}
    Octree::OctreeIterator end() {return tree.end();}
    std::vector<OctreeKey> neighborhood(vec3 center, float radius) {
      return tree.neighborhood(center, radius);
    }

    size_t numLeafs() {return metamap.size();};
    //// Things reimplemented

    template <typename T>
    std::vector<T> &
    get_metadata();

    std::string generateHeader();

    // Main functions for use
    void *get(size_t ind);
    void *get(vec3 pt);
    int64_t insert(vec3 pt, void *data);
};

}
