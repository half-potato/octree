#include <fstream>
#include <sstream>
#include <iostream>

#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include "octree/octree.h"
#include "octree/metaoctree.h"

namespace octree {

typedef struct OctreeConfig {
  int max_depth;
  float res;
  glm::vec3 origin;
} OctreeConfig;

typedef struct CameraConfig {
  int im_rows, im_cols;
  float fx, fy, cx, cy;
  float transform_matrix[16];
} CameraConfig;

class CLRaycaster {
  public:
    cl::Device device;

    cl::Buffer buffer_data, buffer_camera_cfg, buffer_octree_cfg, buffer_image, buffer_test;
    cl::CommandQueue queue;
    cl::Kernel scoreKernel, depthKernel, countKernel, indexKernel;
    cl::Context context;

    OctreeConfig octree_config;

    CLRaycaster() {};
    CLRaycaster(octree::MetaOctree tree);
    cl::Platform getPlatform();
    cl::Device getDevice(cl::Platform platform);
    cl::Kernel
    createKernel(
        std::string kernel_path,
        std::string prog_name,
        cl::Context context,
        cl::Device device,
        int max_tree_depth,
        int numNodes);
    cl::CommandQueue
    initQueue(cl::Context context, cl::Device device, octree::MetaOctree tree);

    void renderScore(CameraConfig camera_config, float * image);
    void renderCount(CameraConfig camera_config, int * image);
    void renderDepth(CameraConfig camera_config, float * image);
    void renderIndex(CameraConfig camera_config, int64_t * image);
};
};
