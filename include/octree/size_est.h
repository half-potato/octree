#include <torch/extension.h>

torch::Tensor
cuda_get_face_inds(
    torch::Tensor & pts,
    torch::Tensor & faces,
    torch::Tensor & vertices);
