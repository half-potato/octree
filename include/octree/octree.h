// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#include <stdint.h>
#include <string>
#include <vector>
#include <limits>
#include "glm/glm.hpp"
#include "octree/util.h"
#include "cpptoml.h"

// typedef struct vec3 { float x, y, z; } vec3;

namespace octree {

using glm::vec3;

const int64_t NULL_VOX = -1;
const float EPS = 1e-6;

const std::string PREHEADER = 
"# PyOctree File\n"
"# gitlab.com/half-potato/octree\n";

// Here is how the voxels are numbered
const vec3 VOXEL_NUMBERING[8] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};

typedef struct OctreeNode {
  int64_t children[8];
  // int64_t data_ptr() {return children[8];};
  // int64_t parent_idx() {return children[9];};
  int64_t data_ptr;
  int64_t parent_idx;
  int64_t leaf_idx;
} OctreeNode;

OctreeNode emptyNode(int64_t parent_idx=NULL_VOX, int64_t leaf_idx=NULL_VOX);

struct OctreeKey {
  int64_t node_idx, child_idx;
};

class Octree {
  public:
    float scale;
    vec3 origin;
    int depth;
    std::string version;

    // init and save functions
    Octree(float scale, vec3 origin={0, 0, 0});
    Octree(std::string fname, size_t max_chunk_size=2e9);
    Octree() : scale(0), version("octree-v2") {};
    int save(std::string fname, size_t max_chunk_size=2e9);

    // Simple access functions
    bool withinBounds(vec3 pt);
    void incrementDepth();
    float getRadius();
    int getDepth() {return depth;}
    // These functions are abstracted for easy porting to the GPU
    OctreeNode * getNode(int64_t index) {return &data[index];};
    size_t numNodes() {return data.size();};

    // Index generation functions
    float voxelCoord(float v, int depth); // finds the local coordinates of v wrt to the voxel it is in at depth
    int childIndex(vec3 pt, int depth); // finds the child the pt is located in at depth
    int64_t * indexTree(std::vector<uint8_t> key, int max_depth=-1); // uses a series of child indices to get location
    std::vector<uint8_t> keyFromPt(vec3 pt);
    vec3 ptFromKey(std::vector<uint8_t> key);

    // Main functions for use
    int64_t * get(vec3 pt);
    int64_t * get(OctreeKey key);
    float getScale() {return scale;}
    vec3 getOrigin() {return origin;}
    OctreeKey insert(vec3 pt, int64_t data);
    int64_t * raycast(const vec3 & source, const vec3 & dir, float * dist);
    std::vector<OctreeKey> neighborhood(vec3 center, float radius);
    OctreeKey convertKey(std::vector<uint8_t> key);
    std::vector<uint8_t> convertKey(OctreeKey key);

    // Reading functions
    void binarize(char * buffer);
    int initFromBinary(char * buffer, size_t length);
    std::string generateHeader();
    std::shared_ptr<cpptoml::table> parseHeader(std::ifstream & is, int *header_size, int *num_nodes);

    struct OctreeIterator
    {
        using iterator_category = std::input_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = std::vector<uint8_t>;
        using pointer           = value_type*;  // or also value_type*
        using reference         = value_type&;  // or also value_type&
        std::vector<uint8_t> key;
        int root_size;
        Octree *tree;
        int64_t root_ind;

        OctreeIterator (std::vector<uint8_t> rootkey, std::vector<uint8_t> k, Octree *tree)
          : key(k), root_size(rootkey.size()), tree(tree) {
          assert(key.size()+rootkey.size() == size_t(tree->depth+1));
          // Get root_ind
          root_ind = 0;
          for (size_t i=0; i<rootkey.size(); i++) {
            root_ind = tree->getNode(root_ind)->children[rootkey[i]];
          }

          // Don't increment key to start if this is the end key
          bool not_end = false;
          for (size_t i=0; i<key.size(); i++) {
            not_end |= (key[i] != 7);
          }
          key.insert(key.begin(), rootkey.begin(), rootkey.end());

          if (((tree->indexTree(key) == NULL) || (*tree->indexTree(key) == NULL_VOX)) && not_end) {
            goDown(root_ind, root_size);
          }
        };
        reference operator*() {
          return key;
        }
        bool incrementKey(int64_t ind, int depth);
        bool goDown(int64_t ind, int depth);
        bool goUp(int64_t ind, int depth);

        OctreeIterator operator++() {
          if (!incrementKey(root_ind, root_size)) {
            // Return end if increment key stops
            key = std::vector<uint8_t>(key.size(), 7);
          }
          // if (!incrementKey(0, key.size()-1)) {
          //   // Return end if increment key stops
          //   key = std::vector<uint8_t>(key.size(), 7);
          // }
          return *this;
        }
        OctreeIterator operator++(int) { OctreeIterator tmp = *this; ++(*this); return tmp; }

        friend bool operator== (const OctreeIterator& a, const OctreeIterator& b) {
          if (a.key.size() != b.key.size()) {
            return false;
          }
          for (size_t i=0; i<a.key.size(); i++) {
            if (a.key[i] != b.key[i])
              return false;
          }
          return true;
        }
        friend bool operator!= (const OctreeIterator& a, const OctreeIterator& b) { return !(a==b); }
    };

    // This iterator iterates over the leaves of the tree
    OctreeIterator begin() {
      return OctreeIterator(std::vector<uint8_t>(0, 0), std::vector<uint8_t>(depth+1, 0), this);
    }
    OctreeIterator end() {
      return OctreeIterator(std::vector<uint8_t>(0, 0), std::vector<uint8_t>(depth+1, 7), this);
    }

    std::vector<OctreeNode> data;
};

};

