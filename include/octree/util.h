#pragma once
#include "glm/glm.hpp"

#include <algorithm> 
#include <cctype>
#include <locale>

typedef struct FLinkedList {
  float score;
  int address;
  int child;
  FLinkedList(float score, int address, int child) : score(score), address(address), child(child) {};
  FLinkedList() : score(0), address(-1), child(-1) {};
} FLinkedList;

int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score);

using glm::vec3;

bool rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, float * ray_length);
void vec3print(vec3 a);

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}
