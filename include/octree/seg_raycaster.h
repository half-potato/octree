#pragma once
#include <torch/extension.h>
#include "octree/octree.h"
#include "octree/metaoctree.h"
#include "octree/cuda_raycaster.h"
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>

namespace octree {
// METADATA_TAG. Must match other metadata tags
typedef ADESeg SegOctreeData;
const size_t NUM_LABELS=150;

void init_seg_module(py::module_ &m);

class SegRaycaster {
  public:
    long *doctree_data;
    octree::MetaOctree *octree;
    SegOctreeData *dmetadata;
    octree::OctreeKey *dmetamap;
    OctreeConfig *dtree_cfg;
    CameraConfig cfg;
    CameraConfig *dcfg; // device side
    curandState_t *states;
    int tx, ty, state_width;

    float *dseg, *ddepth;
    int32_t *dcount;

    SegRaycaster(octree::MetaOctree & tree, CameraConfig & cfg) {
      init(tree, cfg);
    };
    SegRaycaster(std::string &tree_path, std::vector<float> &calib, int h, int w) {
      CameraConfig cfg {
        .im_rows = h, .im_cols=w,
        .fx = calib[0], .fy = calib[1], .cx = calib[2], .cy = calib[3],
      };
      octree::MetaOctree tree (tree_path);
      init(tree, cfg);
    };
    void init(octree::MetaOctree & tree, CameraConfig & cfg);
    SegRaycaster() {};
    ~SegRaycaster();
    void renderDepth(CameraConfig & cfg, float *image);
    void projectSegmentation(
        const CameraConfig & cfg, const float *depth, const float *seg, const int n);
    void projectSegmentationTh(
        const torch::Tensor &pose, const torch::Tensor &depth, const torch::Tensor &seg, const int n);
    void renderSegmentation(const CameraConfig & cfg, const float *depth, float *seg, int32_t *count, const int n);
    std::vector<torch::Tensor>
    renderSegmentationTh(
        const torch::Tensor &pose, const torch::Tensor &depth, const int n);
};
};

