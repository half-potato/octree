#pragma once
#include <cstdio>

#include <cuda.h>
#include <iostream>
#include <curand.h>
#include <curand_kernel.h>

#include "octree/octree.h"
#include "octree/seg_raycaster.h"

using namespace octree;
namespace seg_kernels {

__global__ void
seg_render_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    SegOctreeData *dmetadata,
    float *depth,
    float *seg, int32_t *total_counts,
    curandState_t* states,
    int state_width);

__global__ void
seg_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    SegOctreeData *dmetadata,
    float *depth,
    float *seg,
    curandState_t* states,
    int state_width);

}

