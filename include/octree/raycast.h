#pragma once
#include "octree/cuda_raycaster.h"
#include "octree/octree.h"

using namespace octree;

__device__
inline float sq_norm(float3 v);

__device__
inline float norm(float3 v);

__device__
inline float3 operator-(const float3 &a, const float3 &b);

__device__
inline float3 operator+(const float3 &a, const float3 &b);

__device__
inline float3 operator+(const float3 &a, const float &b);

__device__
inline float3 operator*(const float &a, const float3 &b);

__device__
inline float sq_distance(float3 a, float3 b);

__device__
inline float distance(float3 a, float3 b);

__device__
inline float sign(float v);

namespace cudatree {

enum NEXT_KEY_MOVE {
  KEY_INC = 0,
  KEY_UP = 1,
  KEY_DOWN = 2,
  KEY_RETURN = 3,
  KEY_FINISHED = 4,
};

/* typedef float3 float3; */

typedef struct FLinkedList {
  float score;
  int address;
  int child;
  int parent;
} FLinkedList;

// Here is how the voxels are numbered
__device__ float3 dVOXEL_NUMBERING[] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};

__device__ const int64_t NULL_VOX = -1;
__device__ const float EPS = 0;
__device__ const size_t MAX_DEPTH = 20;
__device__ const size_t STACK_SIZE = MAX_DEPTH*3+1;
__device__ const size_t NUM_ELEMENT_PER_NODE = 11;

__device__
int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score);

__device__
bool
rayBoxIntersect ( float3 rpos, float3 rdir, float3 vmin, float3 vmax, float * ray_length);

__device__
int64_t getNodeChild(int64_t * data, int64_t i, int64_t j);

__device__
OctreeNode * getNode(const int64_t *data, const int64_t node_idx);
// pure
__device__
bool
withinBounds(OctreeConfig* tree_cfg, float3 pt);

// pure
__device__
float
voxelCoord(
    const OctreeConfig* tree_cfg, 
    const float v, 
    const int local_depth);

// pure
__device__
int
childIndex(
    const OctreeConfig* tree_cfg, 
    const float3 pt, 
    const int local_depth);

__device__
int64_t
get(OctreeConfig* tree_cfg,
    int64_t *data, float3 pt);

__device__
int
convertKey(
    OctreeConfig* tree_cfg,
    long *data,
    OctreeKey & okey, uint8_t * key);

__device__
float3
ptFromKey(
    const OctreeConfig * tree_cfg,
    const uint8_t * key,
    const int max_depth = -1);

__device__
int64_t
raycast(OctreeConfig* tree_cfg,
        const float3 pos,
        const float3 dir,
        float * dist,
        int64_t * data);

// Helper functions for projection

__device__
float3
rotateVec(float * mat, float3 v);

__device__ void
calcRayPos(CameraConfig * cfg, float i, float j, float3 * pos, float3 * ray);

__device__ void
print_mat(float *mat);

__device__ float
sphere_intersection(float d, float R, float r);

__device__
bool
withinRadius(
    const OctreeConfig * tree_cfg,
    const long * data,
    const uint8_t * key,
    const float radius,
    const float3 center,
    const int local_depth);

__device__
NEXT_KEY_MOVE
keyInc(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth);

// Mutable recursion
__device__
NEXT_KEY_MOVE
keyUp(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth);

// Mutable recursion
__device__
NEXT_KEY_MOVE
keyDown(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth);

__device__
bool
traverse(
    const OctreeConfig * tree_cfg,
    const long * data,
    const float radius,
    const float3 center,
    uint8_t * key,
    long *parent_ind,
    int *local_depth,
    NEXT_KEY_MOVE start_move);

__device__
int 
init_neighborhood_key(
    OctreeConfig * tree_cfg,
    int64_t * data,
    float3 center,
    float radius,
    // output
    long * parent_ind,
    int * local_depth,
    uint8_t * key);

};
