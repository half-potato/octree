#pragma once
#include <torch/extension.h>
#include "octree/octree.h"
#include "octree/metaoctree.h"

#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>

#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )

void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line);

namespace octree {
void init_point_module(py::module_ &m);

// METADATA_TAG. Must match other metadata tags
typedef PointDataV2 OctreeData;

__device__ const size_t MAX_DEPTH = 20;

typedef struct CameraConfig {
  int im_rows, im_cols;
  float fx, fy, cx, cy;
  float transform_matrix[16];
} CameraConfig;

typedef struct OctreeConfig {
  int max_depth;
  size_t num_leafs;
  size_t num_nodes;
  float res;
  float3 origin;
} OctreeConfig;

class CudaRaycaster {
  public:
    long *doctree_data;
    octree::MetaOctree octree;
    OctreeData *dmetadata;
    octree::OctreeKey *dmetamap;
    OctreeConfig *dtree_cfg;
    CameraConfig cfg;
    CameraConfig *dcfg; // device side
    curandState_t *states;
    int tx, ty, state_width;

    float *dcount, *dscore, *ddepth, *dsubx, *dsuby, *dsize;

    CudaRaycaster(std::string &tree_path, std::vector<float> &calib, int h, int w, int cell_size=1) {
      CameraConfig cfg {
        .im_rows = h, .im_cols=w,
        .fx = calib[0], .fy = calib[1], .cx = calib[2], .cy = calib[3],
      };
      octree::MetaOctree tree (tree_path);
      init(tree, cfg, cell_size);
    };

    CudaRaycaster(octree::MetaOctree tree, CameraConfig & cfg, size_t cell_size=1) {
      init(tree, cfg, cell_size);
    };
    // CudaRaycaster(octree::MetaOctree * tree, CameraConfig & cfg, size_t cell_size=1);

    void init(octree::MetaOctree tree, CameraConfig & cfg, size_t cell_size=1);
    ~CudaRaycaster();

    void renderDepth(CameraConfig & cfg, float *image);
    void projectScore(
        const CameraConfig & cfg, const float *depth,
        const float *score, const float *subx, const float *suby, const float *size,
        const float threshold,
        const float min_seen_size,
        const float max_unseen_size,
        const float cell_size);
    std::vector<torch::Tensor>
    renderScoreCountTh(
        const torch::Tensor &pose,
        const torch::Tensor &depth,
        const int n);

    void projectFeaturesTh(
        const torch::Tensor &pose, const torch::Tensor &depth, const torch::Tensor &feature_map,
        const float threshold,
        const float min_seen_size,
        const float max_unseen_size,
        const float cell_size);
    void renderScoreCount(CameraConfig & cfg, float *depth, float *score, float *count, int n);
    torch::Tensor extract_points(float threshold, int max_num_points=-1);
    torch::Tensor runNMS(
        float min_nms_radius,
        float max_nms_radius,
        float size_radius_multi,
        float iou_thres,
        float allowed_count_ratio);
    void convolve3d(
        float * kernel,
        size_t kernel_dim,
        float kernel_res);
    void convolve3dTh(
        const torch::Tensor kernel,
        float kernel_res);
    torch::Tensor runSparsityEnforcer(
        float neighborhood_radius,
        int max_features_in_neighborhood);
};
};
