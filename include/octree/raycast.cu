// the reason this is in an include file is because cuda prefers to have device code nonrelocatable
// We get this error when turning on Relocatable Device Code:
// undefined symbol: __cudaRegisterLinkedBinary_48_tmpxft_0000201d_00000000_7_pyoctree_cuda_cpp1_ii_cdbb8430
#pragma once
#include "octree/cuda_raycaster.h"
#include "octree/octree.h"
#include "octree/raycast.h"

using namespace octree;

__device__
inline float sq_norm(float3 v)
{
  return v.x*v.x+v.y*v.y+v.z*v.z;
}

__device__
inline float norm(float3 v)
{
  return sqrt(sq_norm(v));
}

__device__
inline float3 operator-(const float3 &a, const float3 &b) {
  return {a.x-b.x, a.y-b.y, a.z-b.z};
}

__device__
inline float3 operator+(const float3 &a, const float3 &b) {
  return {a.x+b.x, a.y+b.y, a.z+b.z};
}

__device__
inline float3 operator+(const float3 &a, const float &b) {
  return {a.x+b, a.y+b, a.z+b};
}

__device__
inline float3 operator*(const float &a, const float3 &b) {
  return {a*b.x, a*b.y, a*b.z};
}

__device__
inline float sq_distance(float3 a, float3 b)
{
  return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
}

__device__
inline float distance(float3 a, float3 b)
{
  return norm(a-b);
}

__device__
inline float sign(float v) {
  return (v >= 0) ? 1 : -1;
}

namespace cudatree {

__device__
int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score)
{
  // printf("0: %i, %i, %i, %f\n", list[0].address, list[0].child, list[0].parent, list[0].score);
  // printf("1: %i, %i, %i, %f\n", list[1].address, list[1].child, list[1].parent, list[1].score);
  // printf("2: %i, %i, %i, %f\n", list[2].address, list[2].child, list[2].parent, list[2].score);
  // Returns the new head index
  // The head always points to the lowest element
  int new_head_index = head_index;
  int selection = head_index;
  int parent = head_index;
  int child = -1;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    list[0].address = desired_address;
    list[0].child = child;
    list[0].score = score;
    list[0].parent = -1;
    // printf("0: %i, %i, %i, %f\n", list[0].address, list[0].child, list[0].parent, list[0].score);
    // printf("1: %i, %i, %i, %f\n", list[1].address, list[1].child, list[1].parent, list[1].score);
    // printf("2: %i, %i, %i, %f\n", list[2].address, list[2].child, list[2].parent, list[2].score);
    return 0;
  } else if (list[selection].score >= score) {
    // new lowest
    parent = -1;
    child = head_index;
    list[head_index].parent = desired_address;
    new_head_index = desired_address;
  } else {
    // Select first item that is higher
    while (selection != -1 && list[selection].score < score) {
      // store previous selection as parent
      parent = selection;
      // select next
      selection = list[selection].child;
    }
    child = selection;
    list[parent].child = desired_address;
    if (selection != -1) {
      list[selection].parent = desired_address;
    }
  }
  list[desired_address].address = desired_address;
  list[desired_address].child = child;
  list[desired_address].score = score;
  list[desired_address].parent = parent;

  // printf("0: %i, %i, %i, %f\n", list[0].address, list[0].child, list[0].parent, list[0].score);
  // printf("1: %i, %i, %i, %f\n", list[1].address, list[1].child, list[1].parent, list[1].score);
  // printf("2: %i, %i, %i, %f\n", list[2].address, list[2].child, list[2].parent, list[2].score);
  return new_head_index;
}

__device__
bool
rayBoxIntersect ( float3 rpos, float3 rdir, float3 vmin, float3 vmax, float * ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  float3 dirfrac = {1/rdir.x, 1/rdir.y, 1/rdir.z};
  // rpos is ORIGIN of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      *ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      *ray_length = tmax;
      return false;
  }

  *ray_length = tmin;
  return true;
}

__device__
int64_t getNodeChild(int64_t * data, int64_t i, int64_t j) {
  return data[i*NUM_ELEMENT_PER_NODE+j];
}

__device__
OctreeNode * getNode(const int64_t *data, const int64_t node_idx) {
  return &((OctreeNode *)data)[node_idx];
}

// pure
__device__
bool
withinBounds(OctreeConfig* tree_cfg, float3 pt)
{
  float radius = float(pow(2, tree_cfg->max_depth)) * tree_cfg->res;
  float min_x = tree_cfg->origin.x - radius;
  float max_x = tree_cfg->origin.x + radius;

  float min_y = tree_cfg->origin.y - radius;
  float max_y = tree_cfg->origin.y + radius;

  float min_z = tree_cfg->origin.z - radius;
  float max_z = tree_cfg->origin.z + radius;
  return (min_x < pt.x && max_x > pt.x) &&
         (min_y < pt.y && max_y > pt.y) &&
         (min_z < pt.z && max_z > pt.z);
}

// pure
__device__
float
voxelCoord(
    const OctreeConfig* tree_cfg, 
    const float v, 
    const int local_depth) {
  // assumes 0 origin
  // this is like 2 * extent
  float res = tree_cfg->res*pow(2, tree_cfg->max_depth-local_depth+1);
  float voxelCenter = (floorf(v/res)+0.5)*res;
  if (local_depth == 0) {
    return v/res;
  }
  return (v - voxelCenter)/res;
}

// pure
__device__
int
childIndex(
    const OctreeConfig* tree_cfg, 
    const float3 pt, 
    const int local_depth) {
  // Function assumes point is within bounds
  // First, get location within the parent voxel
  float vx = voxelCoord(tree_cfg, pt.x-tree_cfg->origin.x, local_depth);
  float vy = voxelCoord(tree_cfg, pt.y-tree_cfg->origin.y, local_depth);
  float vz = voxelCoord(tree_cfg, pt.z-tree_cfg->origin.z, local_depth);

  int index = 0;
  index |= (vx <= 0) << 2;
  index |= (vy <= 0) << 1;
  index |= (vz <= 0) << 0;
  return index;
}

__device__
int64_t
get(OctreeConfig* tree_cfg,
    int64_t *data, float3 pt) {
  if (!withinBounds(tree_cfg, pt)) {
    return NULL_VOX;
  }
  int64_t current_branch = 0;
  for (int i=0; i < tree_cfg->max_depth; i++) {
    int j = childIndex(tree_cfg, pt, i);

    if (getNode(data, current_branch)->children[j] == NULL_VOX) {
      return NULL_VOX;
    }
    current_branch = getNode(data, current_branch)->children[j];
  }
  // set data
  int val_j = childIndex(tree_cfg, pt, tree_cfg->max_depth);
  return getNode(data, current_branch)->children[val_j];
}

__device__
int
convertKey(
    OctreeConfig* tree_cfg,
    long *data,
    OctreeKey & okey, uint8_t * key)
{
  key[tree_cfg->max_depth] = okey.child_idx;

  OctreeNode * node = getNode(data, okey.node_idx);
  for (int i=tree_cfg->max_depth-1; i>=0; i--) {
    key[i] = node->leaf_idx;
    if (node->parent_idx == 0)
      return 0;
    node = getNode(data, node->parent_idx);
  }
  return -1;
}

__device__
float3
ptFromKey(
    const OctreeConfig * tree_cfg,
    const uint8_t * key,
    const int max_depth) {
  float3 output = tree_cfg->origin;
  int mdepth = (max_depth == -1) ? tree_cfg->max_depth+1 : max_depth;
  for (size_t i=0; i<mdepth; i++) {
    float extent = tree_cfg->res*pow(2, tree_cfg->max_depth-i);
    float3 & bound = dVOXEL_NUMBERING[key[i]];
    output.x += extent*bound.x/2.0f;
    output.y += extent*bound.y/2.0f;
    output.z += extent*bound.z/2.0f;
  }
  return output;
}

__device__
int64_t
raycast(OctreeConfig* tree_cfg,
        const float3 pos,
        const float3 dir,
        float * dist,
        int64_t * data)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with ORIGIN and depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  float length = norm(dir);
  if (length == 0) {
    return NULL_VOX;
  }
  float3 ray = {
    dir.x/length,
    dir.y/length,
    dir.z/length,
  };
  // const size_t STACK_SIZE = 7*tree_cfg->max_depth + 1;

  // initialize stack
  float3 voxel_centers[STACK_SIZE];
  int depths[STACK_SIZE];
  // Initialize the depths so we know when we run out
  for (int i=0; i<STACK_SIZE; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[STACK_SIZE];

  int stack_head = 0;
  voxel_centers[stack_head] = tree_cfg->origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;
  int stop_depth = tree_cfg->max_depth;

  while (depths[stack_head] != -1) {
    float3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int depth = depths[stack_head];
    // float mul = (depth == stop_depth) ? 1 : -1;
    float mul = 1;

    int head = -1;
    float extent = tree_cfg->res*pow(2.0f, tree_cfg->max_depth-depth);
    FLinkedList list[8];
    // for (int i=0; i<8; i++) {
    //   list[i].child = -1;
    //   list[i].parent = -1;
    //   list[i].score = -9999;
    // }
    for (int i=0; i<8; i++) {
      list[i].child = -1;
      list[i].score = 999999;
      list[i].address = -1;
    }
    float3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNode(data, voxel_ind)->children[i] == NULL_VOX) {
        continue;
      }
      float3 bound = dVOXEL_NUMBERING[i];

      /* centers[i] = voxel_origin + extent*bound/2.0f; */
      float3 center = {
        voxel_origin.x + extent*bound.x/2.0f,
        voxel_origin.y + extent*bound.y/2.0f,
        voxel_origin.z + extent*bound.z/2.0f,
      };
      centers[i] = center;
      /* float3 vmin = centers[i] - extent/2.0f-EPS; */
      float3 vmin = {
        centers[i].x - extent/2.0f - EPS,
        centers[i].y - extent/2.0f - EPS,
        centers[i].z - extent/2.0f - EPS,
      };
      /* float3 vmax = centers[i] + extent/2.0f+EPS; */
      // float offset = -tree_cfg->res * 0.0;
      float3 vmax = {
        centers[i].x + extent/2.0f + EPS,
        centers[i].y + extent/2.0f + EPS,
        centers[i].z + extent/2.0f + EPS,
      };
      // float3 vmax = {
      //   centers[i].x + extent/2.0f+offset,
      //   centers[i].y + extent/2.0f+offset,
      //   centers[i].z + extent/2.0f+offset,
      // };

      float ray_length;
      bool collision = rayBoxIntersect(pos, ray, vmin, vmax, &ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(list, head, i, mul*ray_length);
    }
    int selection = head;
    int prev_sel = selection;

    // Iterate through the sorted voxels
    int c1=0, c2=0, c3=0, c4=0;
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      float d = list[selection].score;
      prev_sel = selection;
      selection = list[selection].child;
      int64_t new_ind = getNode(data, voxel_ind)->children[j];

      // Check if voxel is empty
      c3++;
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum depth, return
      if (depth == stop_depth) {
        *dist = mul*d;
        return voxel_ind*NUM_ELEMENT_PER_NODE+j;
      }

      // voxel_centers[stack_head] = centers[j];
      // voxel_inds[stack_head] = new_ind;
      // depths[stack_head] = depth+1;
      // stack_head = (stack_head-1 + STACK_SIZE) % STACK_SIZE;
      c1++;
    }
    // if (depth == stop_depth) {
    //   selection = prev_sel;
    // } else {
    //   selection = head;
    // }
    selection = prev_sel;

    // Iterate through the sorted voxels in reverse to add them to the top of the stack
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      // float d = list[selection].score;
      // if (depth == stop_depth) {
      //   selection = list[selection].parent;
      // } else {
      //   selection = list[selection].child;
      // }
      selection = list[selection].parent;
      // selection = list[selection].parent;
      int64_t new_ind = getNode(data, voxel_ind)->children[j];

      // Check if voxel is empty
      c4++;
      if (new_ind == NULL_VOX) {
        continue;
      }

      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = depth+1;
      stack_head = (stack_head-1 + STACK_SIZE) % STACK_SIZE;
      c2++;
    }
    // printf("for: %i/%i, back: %i/%i, head: %i, prev: %i\n", c1, c3, c2, c4, head, prev_sel);
    stack_head = (stack_head+1)%STACK_SIZE;
  }
  *dist = 0;
  return NULL_VOX;
}


// Helper functions for projection

__device__
float3
rotateVec(float * mat, float3 v) {
  float3 out;
  out.x = mat[0] * v.x + mat[1] * v.y + mat[2] * v.z;
  out.y = mat[4] * v.x + mat[5] * v.y + mat[6] * v.z;
  out.z = mat[8] * v.x + mat[9] * v.y + mat[10] * v.z;
  return out;
}

__device__ void
calcRayPos(CameraConfig * cfg, float i, float j, float3 * pos, float3 * ray) {
  float3 forward_ray = {
    (j - cfg->cx) / cfg->fx,
    (i - cfg->cy) / cfg->fy,
    1.0f};
  *ray = rotateVec(cfg->transform_matrix, forward_ray);
  *pos = {cfg->transform_matrix[3], cfg->transform_matrix[7], cfg->transform_matrix[11]};
}

__device__ void
print_mat(float *mat) {
  printf("%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n",
      mat[0], mat[1], mat[2], mat[3], 
      mat[4], mat[5], mat[6], mat[7], 
      mat[8], mat[9], mat[10], mat[11], 
      mat[12], mat[13], mat[14], mat[15]);
}

__device__ float
sphere_intersection(float d, float R, float r)
{
  // d is the distance between the spheres
  // R, r are the radii of the spheres, respectively

  // The idea is to center and rotate the spheres such that a lies at origin and b lies along
  // some translation along the x axis
  // Check if either sphere is whole contained by the other
  if (R > d+r) return 4*M_PI*r*r*r/3;
  if (r > d+R) return 4*M_PI*R*R*R/3;
  // Then, we find the plane of intersection
  float x = (d*d - r*r + R*R)/2/d;
  if (x > d) {
    return 0;
  }
  // This allows us to find the intersection volume as the sum of the two "caps"
  // (cut a sphere with a plane and it gives you a cap shape)
  // cap heights
  float h_a = R - x;
  float h_b = x - d + r;
  // cap volumes
  float v_a = M_PI*h_a*h_a/3*(3*R-h_a);
  float v_b = M_PI*h_b*h_b/3*(3*r-h_b);
  if (v_a < 0 || v_b < 0) {
    return 0;
  }
  return v_a + v_b;
}

__device__
bool
withinRadius(
    const OctreeConfig * tree_cfg,
    const long * data,
    const uint8_t * key,
    const float radius,
    const float3 center,
    const int local_depth)
{
  // local_depth lies between [0, tree_cfg->max_depth]
  // When local_depth = 0, this is just origin

  // ptFromKey gives us the center of the node we are evaluating.
  float3 node_loc = ptFromKey(tree_cfg, key, local_depth+1);
  float dist = distance(node_loc, center);

  // This is the extant of the voxel. this is half the side length of the voxel. I am not sure about this line. The extent is not well defined
  float extent = tree_cfg->res*pow(2, tree_cfg->max_depth-local_depth-1);
  if (local_depth == tree_cfg->max_depth) {
  }

  float3 qp = {
    abs(center.x - node_loc.x),
    abs(center.y - node_loc.y),
    abs(center.z - node_loc.z),
  };
  // within broad square
  if (max(max(qp.x, qp.y), qp.z) <= extent + radius) {
    // we check whether the point is contained with a square with a rounded right corner
    if ((min(min(qp.x, qp.y), qp.z) <= extent) || (pow(qp.x-extent, 2) + pow(qp.y-extent, 2) + pow(qp.z-extent, 2) <= radius * radius)) {
      if (local_depth == tree_cfg->max_depth && distance(center, node_loc) > radius) {
        printf("(%f, %f, %f), (%f, %f, %f), extent: %f, dist: %f, %f, true\n", center.x, center.y, center.z, node_loc.x, node_loc.y, node_loc.z, extent, distance(center, node_loc), radius);
      }
      return true;
    }
  }
  if (local_depth == tree_cfg->max_depth) {
    printf("(%f, %f, %f), (%f, %f, %f), extent: %f, dist: %f, %f, false\n", center.x, center.y, center.z, node_loc.x, node_loc.y, node_loc.z, extent, distance(center, node_loc), radius);

  }
  return false;

  /*
  // When local_depth = 0, we are asking whether a child of the origin contains a node we need
  // At local_depth = max_depth+1, we are asking about whether a leaf node
  if (dist <= radius) {
    return true;
  }
  float3 closest_pt = (radius/dist)*(node_loc - center) + center;
  int ci = childIndex(tree_cfg, closest_pt, local_depth);
  return (ci == key[local_depth]);
  */
}

__device__
bool
traverse(
    const OctreeConfig * tree_cfg,
    const long * data,
    const float radius,
    const float3 center,
    uint8_t * key,
    long *parent_ind,
    int *local_depth,
    NEXT_KEY_MOVE start_move)
{
  NEXT_KEY_MOVE next_move = start_move;
  // Run initialization
  while (true) {
    switch (next_move) {
      case KEY_INC:
        next_move = keyInc(
            tree_cfg, data, key, radius, center,
            parent_ind, local_depth);
        break;
      case KEY_DOWN:
        next_move = keyDown(
            tree_cfg, data, key, radius, center,
            parent_ind, local_depth);
        break;
      case KEY_UP:
        next_move = keyUp(
            tree_cfg, data, key, radius, center,
            parent_ind, local_depth);
        break;
      case KEY_RETURN:
        return true;
      case KEY_FINISHED:
        return false;
    }
  }
}

// Mutable recursion
__device__
NEXT_KEY_MOVE
keyDown(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth)
{
  int64_t new_ind = getNode(data, *parent_ind)->children[key[*local_depth]];
  // Bounds check
  if (new_ind != NULL_VOX && (new_ind < 0 || new_ind >= tree_cfg->num_nodes)) {
    return KEY_FINISHED;
  }
  if (new_ind == NULL_VOX) {
    // If we can't go down, then go to the side
    return KEY_INC;
  } else {
    *parent_ind = new_ind;
    (*local_depth)++;
    if ((*local_depth) == tree_cfg->max_depth) {
      return KEY_RETURN;
    } else {
      // If we can go down, try to go down again
      key[*local_depth] = 0;
      return KEY_DOWN;
    }
  }
}

// Mutable recursion
__device__
NEXT_KEY_MOVE
keyUp(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth)
{
  // Go up a level, then try to go to the side
  for (size_t j=*local_depth; j<tree_cfg->max_depth+1; j++) {
    key[j] = 0;
  }
  // printf("%zu, %i\n", tree->getNode(parent_ind)->parent_idx, local_depth-1);
  OctreeNode * result = getNode(data, *parent_ind);
  if (result == NULL) {
    return KEY_FINISHED;
  }
  *parent_ind = result->parent_idx;
  if (*parent_ind != NULL_VOX && (*parent_ind < 0 || *parent_ind >= tree_cfg->num_nodes)) {
    return KEY_FINISHED;
  }
  (*local_depth)--;
  return KEY_INC;
}

// Mutable recursion
__device__
NEXT_KEY_MOVE
keyInc(
    const OctreeConfig * tree_cfg,
    const long * data,
    uint8_t * key,
    const float radius,
    const float3 center,
    long *parent_ind,
    int *local_depth)
{
  // We wouldn't want to increment past 7
  if (key[*local_depth] >= 7) {
    if (*local_depth == 0) {
      // Finished iterating
      // printf("%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i \n",
      //     key[0], key[1], key[2], key[3], key[4], key[5], key[6], key[7], key[8], key[9], key[10]);
      return KEY_FINISHED;
    } else {
      return KEY_UP;
    }
  } else {
    // Go to the side
    key[*local_depth]++;
    for (int i=(*local_depth)+1; i<tree_cfg->max_depth+1; i++) {
      key[i] = 0;
    }
    if (getNode(data, *parent_ind)->children[key[*local_depth]] == NULL_VOX ||
        !withinRadius(tree_cfg, data, key, radius, center, *local_depth))
    {
      // Go to the side again
      return KEY_INC;
    } else {
      if ((*local_depth)==tree_cfg->max_depth) {
        return KEY_RETURN;
      } else {
        return KEY_DOWN;
      }
    }
  }
}

__device__
int 
init_neighborhood_key(
    OctreeConfig * tree_cfg,
    int64_t * data,
    float3 center,
    float radius,
    // output
    long * parent_ind,
    int * local_depth,
    uint8_t * key)
{
  // int vox_radius = radius / tree_cfg->res;
  // int levels_up = fmin(int(ceil(log(vox_radius)/log(2))+2), tree_cfg->max_depth);
  int levels_up = tree_cfg->max_depth+1;

  if (levels_up < 1) {
    // Then there is no space to iterate
    return tree_cfg->max_depth;
  }
  // Copy root over
  for (int i=0; i<tree_cfg->max_depth+1; i++) {
    key[i] = 0;
  }
  *parent_ind = 0;
  *local_depth = 0;
  traverse(
      tree_cfg, data,
      radius, center,
      key, parent_ind, local_depth, KEY_DOWN);

  return 0;
}

__device__
int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score);

};

__device__
float sq_norm(float3 v);

