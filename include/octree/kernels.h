#pragma once
#define USE_RAYCASTING false
#include <cstdio>

#include <cuda.h>
#include <iostream>
#include <curand.h>
#include <curand_kernel.h>

#include "octree/octree.h"
#include "octree/cuda_raycaster.h"

using namespace octree;

__global__ void
init_randoms(
    unsigned int seed,
    curandState_t* states,
    int state_width);

__global__ void
depth_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    float *depth,
    curandState_t *states,
    int state_width);

namespace point_kernels {

__global__ void
nms_3d(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    bool *selected_points, // initialize to 1s
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio,
    int start_i);

__global__ void
sparsity_enforcer(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    bool *selected_points, // initialize to 1s
    float neighborhood_radius,
    int max_features_in_neighborhood,
    int start_i);

__global__ void
score_count_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    float *depth,
    float *score, float *total_counts,
    curandState_t* states,
    int state_width);

__global__ void
map_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    float *depth,
    float *score, float *subx, float *suby, float *size,
    curandState_t* states,
    int state_width,
    float threshold,
    float min_seen_size,
    float max_unseen_size,
    int cell_size);

__global__ void
convolve3d(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    OctreeData *dnew_metadata,
    float *kernel,
    size_t kernel_dim,
    float kernel_res,
    size_t start_i);

}
