#pragma once

#include <torch/extension.h>
#include <vector>
#include <string>

torch::Tensor
cuda_raycast_nms(
    std::string octomap_path,
    std::vector<torch::Tensor> & depth_maps,
    std::vector<torch::Tensor> & feature_maps, // [(score, subx, suby, size), im_rows, im_cols]
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib,
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio,
    float threshold,
    float min_seen_size,
    float max_unseen_size,
    int kernel_size, 
    int cell_size);

std::vector<torch::Tensor>
cuda_render_depth(
    std::string octomap_path,
    int w, int h,
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib);

torch::Tensor
cuda_neighborhood_sparsity_enforcement(
    torch::Tensor features,
    float neighborhood_radius,
    int max_features_in_neighborhood,
    float res);

void init_cuda_module(py::module_ &m);
