from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CUDAExtension, CppExtension
import os
from pathlib import Path
from pybind11.setup_helpers import naive_recompile, ParallelCompile
import torch

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


current_dir = Path(__file__).parent.absolute()

ParallelCompile("NPY_NUM_BUILD_JOBS", needs_recompile=naive_recompile).install()

if torch.cuda.is_available():
    setup(
        name='pyoctree',
        ext_modules=[
            CUDAExtension(
                'pyoctree',
                sources=[
                    'src/pyoctree_cuda.cu',
                    'src/pyoctree_cuda_reg.cpp',
                    'src/kernels.cu',
                    'src/size_est.cu',
                    'src/cuda_raycaster.cu',
                    'src/seg_raycaster.cu',
                    'src/pyoctree.cpp',
                    'src/metaoctree.cpp',
                    'src/octree.cpp',
                    'src/util.cpp'
                ],
                include_dirs=[os.path.join(current_dir, 'include'), '/usr/include/eigen3/'],
                define_macros=[('BUILD_CUDA', '1')],
                extra_compile_args={'cxx': ['-std=c++17', '-O2'], 'nvcc': ['-std=c++17', '-O2', '-g']},
            )
        ],
        cmdclass={
            'build_ext': BuildExtension
        })
else:
    print(bcolors.OKGREEN + "Building without CUDA" + bcolors.ENDC)
    setup(
        name='pyoctree',
        ext_modules=[
            CppExtension(
                'pyoctree',
                sources=[
                    'src/pyoctree.cpp',
                    'src/metaoctree.cpp',
                    'src/octree.cpp',
                    'src/util.cpp'
                ],
                include_dirs=[os.path.join(current_dir, 'include')],
                #  extra_compile_args={'cxx': ['-g'], 'nvcc': ['-O2']},
                extra_compile_args={'cxx': ['-O0', '-fopenmp']},
            ),
        ],
        cmdclass={
            'build_ext': BuildExtension
        })
