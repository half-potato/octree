import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from icecream import ic

M_PI = np.pi
# Inter: 0.198677, CVol: 0.008033, OVol: 0.008070
a = (-3.928210, -0.580466, -2.994390)
R = 0.124242
b = (-3.834301, -0.629840, -2.994160)
r = 0.124432

dist = np.linalg.norm(np.array(a)-np.array(b))
print(dist)

def sphere_intersection(d, R, r):
    if R > d+r:
        return M_PI*r**3*4/3
    if r > d+R:
        return M_PI*R**3*4/3
    x = (d*d - r*r + R*R)/2/d
    if (x > d):
        return 0
    a_a = x;
    a_b = d - x;
    # cap heights
    h_a = R - x;
    h_b = x - d + r;
    ic(a_a, a_b, h_a, h_b)
    # cap volumes
    v_a = M_PI*h_a*h_a/3*(3*R-h_a)
    v_b = M_PI*h_b*h_b/3*(3*r-h_b)
    #  v_a = M_PI/6/h_a*(3*a_a*a_a+h_a*h_a);
    #  v_b = M_PI/6/h_b*(3*a_b*a_b+h_b*h_b);
    if (v_a < 0 or v_b < 0):
        return 0
    return v_a + v_b

fig = plt.figure()
ax = fig.gca(projection='3d')

def draw_sphere(r, c, col='r'):
    # draw sphere
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x = np.cos(u)*np.sin(v)*r+c[0]
    y = np.sin(u)*np.sin(v)*r+c[1]
    z = np.cos(v)*r+c[2]
    ax.plot_wireframe(x, y, z, color=col)

vinter = sphere_intersection(dist, R, r)
cvol = 4*M_PI*R**3/3;
ovol = 4*M_PI*r**3/3;
vunion = cvol + ovol - vinter;
iou = vinter / vunion;
ic(vinter, cvol, ovol, vunion, iou)

draw_sphere(R, a, col='r')
draw_sphere(r, b, col='b')
plt.show()
