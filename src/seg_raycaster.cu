#include "octree/seg_raycaster.h"
#include "octree/kernels.h"
#include "octree/seg_kernels.h"

namespace octree {

void init_seg_module(py::module_ &m) {
  py::class_<SegRaycaster>(m, "SegRaycaster")
    .def(py::init<std::string &, std::vector<float> &, int, int>())
    .def("renderSegmentation", &SegRaycaster::renderSegmentationTh, pybind11::return_value_policy::take_ownership)
    .def("projectSegmentation", &SegRaycaster::projectSegmentationTh, pybind11::return_value_policy::take_ownership);
}

void
SegRaycaster::init(octree::MetaOctree & tree, CameraConfig & cfg) {
  assert(tree.getDepth() <= MAX_DEPTH);
  // METADATA_TAG
  tree.initializeType(ADE_SEG);
  octree = &tree;
  tx = 16;
  ty = 16;
  size_t size = tree.numNodes()*sizeof(octree::OctreeNode);
  printf("Tree size: %zu\n", size);
  this->cfg = cfg;

  OctreeConfig h_cfg = {
    .max_depth=tree.getDepth(), 
    .num_leafs=tree.numLeafs(),
    .num_nodes=tree.numNodes(),
    .res = tree.getScale(),
    .origin = {tree.getOrigin().x, tree.getOrigin().y, tree.getOrigin().z}
  };

  state_width = tx;
  checkCudaErrors(cudaMalloc(&states, tx*ty*sizeof(curandState_t)));
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  if (num_blocks.x != 0 && num_blocks.y != 0) {
    init_randoms<<<num_blocks, block_size>>>(time(0), states, state_width);
    checkCudaErrors(cudaGetLastError());
  } else {
    printf("Unable to initialize random states because the desired block size is 0\n");
  }

  printf("Metadata Size: %zu\n", tree.metadata_size());
  checkCudaErrors(cudaMalloc(&doctree_data, size));
  checkCudaErrors(cudaMalloc(&dmetadata, tree.metadata_size()));
  checkCudaErrors(cudaMalloc(&dmetamap, tree.numLeafs()*sizeof(OctreeKey)));
  checkCudaErrors(cudaMalloc(&dtree_cfg, sizeof(OctreeConfig)));
  checkCudaErrors(cudaMalloc(&dcfg, sizeof(CameraConfig)));

  // IO Buffers
  checkCudaErrors(cudaMalloc(&ddepth, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&dcount, cfg.im_cols*cfg.im_rows*sizeof(int32_t)));
  checkCudaErrors(cudaMalloc(&dseg, NUM_LABELS*cfg.im_cols*cfg.im_rows*sizeof(float)));

  checkCudaErrors(cudaMemcpy(dtree_cfg, &h_cfg, sizeof(OctreeConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dmetadata, tree.metadata(), tree.metadata_size(), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dmetamap, tree.metamap.data(), tree.numLeafs()*sizeof(OctreeKey), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(doctree_data, tree.tree.data.data(), size, cudaMemcpyHostToDevice));
}

SegRaycaster::~SegRaycaster() {
  // Random states
  // checkCudaErrors(cudaFree(states));
  // Octree stuff
  checkCudaErrors(cudaFree(doctree_data));
  checkCudaErrors(cudaFree(dmetadata));
  checkCudaErrors(cudaFree(dmetamap));
  checkCudaErrors(cudaFree(dtree_cfg));
  checkCudaErrors(cudaFree(dcfg));

  checkCudaErrors(cudaFree(ddepth));
  checkCudaErrors(cudaFree(dseg));
  checkCudaErrors(cudaFree(dcount));

}

void SegRaycaster::renderDepth(CameraConfig & cfg, float *image)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);
  checkCudaErrors(cudaMemset(ddepth, 0, im_size));
  depth_projection_kernel<<<num_blocks, block_size>>>(
    dcfg, dtree_cfg,
    doctree_data,
    ddepth,
    states, state_width);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());
  checkCudaErrors(cudaMemcpy(image, ddepth, cfg.im_cols*cfg.im_rows*sizeof(float), cudaMemcpyDeviceToHost));
}

void SegRaycaster::projectSegmentation(
        const CameraConfig & cfg, const float *depth, const float *seg, const int n)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);

  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(ddepth, depth, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dseg, seg, NUM_LABELS*im_size, cudaMemcpyHostToDevice));
  for (int i=0; i<n; i++) {
    seg_kernels::seg_projection_kernel<<<num_blocks, block_size>>>(
      dcfg, dtree_cfg,
      doctree_data, dmetamap, dmetadata, // output
      ddepth,
      dseg,
      states, state_width);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }
}

void 
SegRaycaster::projectSegmentationTh(
    const torch::Tensor &pose, const torch::Tensor &depth, const torch::Tensor &seg, const int n)
{
  // seg shape: h, w, num_labels
  memcpy(cfg.transform_matrix, pose.flatten().contiguous().data_ptr<float>(), 16*sizeof(float));
  projectSegmentation(
      cfg, 
      depth.flatten().contiguous().data_ptr<float>(),
      seg.flatten().contiguous().data_ptr<float>(),
      n);
}

void SegRaycaster::renderSegmentation(const CameraConfig & cfg, const float *depth, float *seg, int32_t *count, const int n)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);
  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(ddepth, depth, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemset(dseg, 0, NUM_LABELS*im_size));
  checkCudaErrors(cudaMemset(dcount, 0, im_size));
  for (int i=0; i<n; i++) {
    seg_kernels::seg_render_kernel<<<num_blocks, block_size>>>(
      dcfg, dtree_cfg,
      doctree_data, dmetamap, dmetadata,
      ddepth,
      dseg, dcount,
      states, state_width);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }
  checkCudaErrors(cudaMemcpy(seg, dseg, NUM_LABELS*im_size, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(count, dcount, im_size, cudaMemcpyDeviceToHost));
}

std::vector<torch::Tensor>
SegRaycaster::renderSegmentationTh(
    const torch::Tensor &pose, const torch::Tensor &depth, const int n)
{
  assert(pose.dtype() == torch::kFloat32);
  assert(depth.dtype() == torch::kFloat32);
  torch::Device cpu("cpu");
  // seg shape: h, w, num_labels
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  memcpy(cfg.transform_matrix, pose.flatten().contiguous().data_ptr<float>(), 16*sizeof(float));
  torch::Tensor seg = torch::zeros({cfg.im_rows, cfg.im_cols, NUM_LABELS}, options);
  torch::Tensor count = torch::zeros({cfg.im_rows, cfg.im_cols}, torch::TensorOptions().dtype(torch::kInt32).device(cpu));
  renderSegmentation(
      cfg, 
      depth.flatten().contiguous().data_ptr<float>(),
      seg.flatten().contiguous().data_ptr<float>(),
      count.flatten().contiguous().data_ptr<int32_t>(),
      n);
  return {seg, count};
}
};

