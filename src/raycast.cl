typedef long int64_t;

typedef struct vec3 {
  float x, y, z;
} vec3;
/* typedef float3 vec3; */


typedef struct OctreeNode {
  int64_t children[8];
  int64_t data_ptr;
  int64_t parent_idx;
  int64_t leaf_idx;
} OctreeNode;

typedef struct OctreeConfig {
  int max_depth;
  float res;
  vec3 origin;
} OctreeConfig;

typedef struct CameraConfig {
  int im_rows, im_cols;
  float fx, fy, cx, cy;
  float transform_matrix[16];
} CameraConfig;

// Here is how the voxels are numbered
__constant vec3 VOXEL_NUMBERING[] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};
__constant int64_t NULL_VOX = -1;
__constant float EPS = 1e-6;
__constant size_t NUM_ELEMENT_PER_NODE = 11;
/* __constant float EPS = 0; */

typedef struct FLinkedList {
  float score;
  int64_t address;
  int64_t child;
} FLinkedList;

int
sort_insert(
    FLinkedList * list,
    const int64_t head_index,
    const int64_t desired_address,
    const float score)
{
  // Returns the new head index
  FLinkedList item = {score, desired_address, -1};

  int64_t new_head_index = head_index;
  int64_t selection = head_index;
  int64_t parent = head_index;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    list[0] = item;
    return 0;
  } else {
    while (selection != -1 && list[selection].score < item.score) {
      parent = selection;
      selection = list[selection].child;
    }
    if (selection == parent) {
      if (list[selection].score > item.score) {
        item.child = head_index;
        new_head_index = item.address;
      } else {
        list[parent].child = desired_address;
      }
    } else {
      item.child = selection;
      list[parent].child = desired_address;
    }
  }
  list[item.address] = item;
  return new_head_index;
}

bool
rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, float * ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  vec3 dirfrac = {1/rdir.x, 1/rdir.y, 1/rdir.z};
  // rpos is ORIGIN of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      *ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      *ray_length = tmax;
      return false;
  }

  *ray_length = tmin;
  return true;
}

int64_t getNodeChild(__global int64_t * data, int64_t i, int64_t j) {
  return data[i*NUM_ELEMENT_PER_NODE+j];
}

int64_t
raycast(__global OctreeConfig* tree_cfg,
        const vec3 pos,
        const vec3 dir,
        float * dist,
        __global int64_t * data)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with ORIGIN and depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  float length = sqrt(dir.x*dir.x+dir.y*dir.y+dir.z*dir.z);
  vec3 ray = {
    dir.x/length,
    dir.y/length,
    dir.z/length,
  };

  // initialize stack
  vec3 voxel_centers[STACK_SIZE];
  int depths[STACK_SIZE];
  // Initialize the depths so we know when we run out
  for (int64_t i=0; i<STACK_SIZE; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[STACK_SIZE];

  int64_t stack_head = 0;
  voxel_centers[stack_head] = tree_cfg->origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;
  int stop_depth = tree_cfg->max_depth;

  while (depths[stack_head] != -1) {
    vec3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int depth = depths[stack_head];
    float mul = (depth == stop_depth) ? 1 : -1;

    int64_t head = -1;
    float s = tree_cfg->res*pow(2.0f, tree_cfg->max_depth-depth);
    FLinkedList list[8];
    vec3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNodeChild(data, voxel_ind, i) == NULL_VOX) {
        continue;
      }
      vec3 bound = VOXEL_NUMBERING[i];

      /* centers[i] = voxel_origin + s*bound/2.0f; */
      vec3 center = {
        voxel_origin.x + s*bound.x/2.0f,
        voxel_origin.y + s*bound.y/2.0f,
        voxel_origin.z + s*bound.z/2.0f,
      };
      centers[i] = center;
      /* vec3 vmin = centers[i] - s/2.0f-EPS; */
      vec3 vmin = {
        centers[i].x - s/2.0f-EPS,
        centers[i].y - s/2.0f-EPS,
        centers[i].z - s/2.0f-EPS,
      };
      /* vec3 vmax = centers[i] + s/2.0f+EPS; */
      vec3 vmax = {
        centers[i].x + s/2.0f+EPS,
        centers[i].y + s/2.0f+EPS,
        centers[i].z + s/2.0f+EPS,
      };

      float ray_length;
      bool collision = rayBoxIntersect(pos, ray, vmin, vmax, &ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(list, head, i, mul*ray_length);
    }
    int64_t selection = head;

    // Iterate through the sorted voxels
    while (selection != -1) {
      // Update index
      int64_t j = list[selection].address;
      float d = mul*list[selection].score;
      selection = list[selection].child;
      int64_t new_ind = getNodeChild(data, voxel_ind, j);

      // Check if voxel is empty
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum depth, return
      if (depth == stop_depth) {
        *dist = d;
        return voxel_ind*NUM_ELEMENT_PER_NODE+j;
      }
      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = depth+1;
      stack_head = (stack_head-1 + STACK_SIZE) % STACK_SIZE;
    }
    stack_head = (stack_head+1)%STACK_SIZE;
  }
  *dist = 0;
  return NULL_VOX;
}

void
matmul(float * a, float * b, float * c, int n, int m, int l) {
  // n x m * m x l
  for (int i=0; i<n; i++) {
    for (int j=0; j<l; j++) {
      float v = 0;
      for (int k=0; k<m; k++) {
        v += a[i*m+k] * b[j+k*l];
      }
      c[i*l+j] = v;
    }
  }
}

vec3
rotateVec(__global float * mat, vec3 v) {
  vec3 out;
  out.x = mat[0] * v.x + mat[4] * v.y + mat[8] * v.z;
  out.y = mat[1] * v.x + mat[5] * v.y + mat[9] * v.z;
  out.z = mat[2] * v.x + mat[6] * v.y + mat[10] * v.z;
  return out;
}

void kernel raycast_score(__global int64_t* data, __global OctreeConfig* tree_cfg, __global CameraConfig* cam_cfg, __global float* image, __global float* test) {
  // Ray trace value for each pixel
  int i = get_global_id(0);
  int j = get_global_id(1);
  int ind = i*cam_cfg->im_cols + j;
  vec3 forward_ray = {
    (float)(j - cam_cfg->cx) / cam_cfg->fx,
    (float)(i - cam_cfg->cy) / cam_cfg->fy,
    1.0f};
  // Rotate ray
  float dist;
  vec3 ray = rotateVec(cam_cfg->transform_matrix, forward_ray);
  vec3 pos = {cam_cfg->transform_matrix[12], cam_cfg->transform_matrix[13], cam_cfg->transform_matrix[14]};

  test[0] = pos.x+1;
  test[1] = pos.y;
  test[2] = pos.z;

  /* int64_t hit = raycast(tree_cfg, cam_cfg->pos, ray, &dist, data); */
  int64_t hit = raycast(tree_cfg, pos, ray, &dist, data);
  /* image[ind] = dist; */
  if (hit != NULL_VOX) {
    int raw_score = data[hit] >> 32;
    float * score = (float *)(&raw_score);
    int count = data[hit] & (int64_t)(pow(2.0f, 32.0f)-1);
    count = (count > 1) ? count : 1;
    image[ind] = (*score*1e5)/(float)(count);
    /* image[ind] = *score; */
  } else {
    image[ind] = 0;
  }
}

void kernel raycast_count(__global int64_t* data, __global OctreeConfig* tree_cfg, __global CameraConfig* cam_cfg, __global int* image, __global float* test) {
  // Ray trace value for each pixel
  int i = get_global_id(0);
  int j = get_global_id(1);
  int ind = i*cam_cfg->im_cols + j;
  vec3 forward_ray = {
    (float)(j - cam_cfg->cx) / cam_cfg->fx,
    (float)(i - cam_cfg->cy) / cam_cfg->fy,
    1.0f};
  // Rotate ray
  float dist;
  vec3 ray = rotateVec(cam_cfg->transform_matrix, forward_ray);
  vec3 pos = {cam_cfg->transform_matrix[12], cam_cfg->transform_matrix[13], cam_cfg->transform_matrix[14]};

  test[0] = pos.x;
  test[1] = pos.y;
  test[2] = pos.z;

  /* int64_t hit = raycast(tree_cfg, cam_cfg->pos, ray, &dist, data); */
  int64_t hit = raycast(tree_cfg, pos, ray, &dist, data);
  if (hit != NULL_VOX) {
    int raw_score = data[hit] >> 32;
    float * score = (float *)(&raw_score);
    int count = data[hit] & (int64_t)(pow(2.0f, 32.0f)-1);
    image[ind] = count;
  } else {
    image[ind] = 0;
  }
}

void kernel raycast_depth(__global int64_t* data, __global OctreeConfig* tree_cfg, __global CameraConfig* cam_cfg, __global float* image, __global float* test) {
  // Ray trace value for each pixel
  int i = get_global_id(0);
  int j = get_global_id(1);
  int ind = i*cam_cfg->im_cols + j;
  vec3 forward_ray = {
    (float)(j - cam_cfg->cx) / cam_cfg->fx,
    (float)(i - cam_cfg->cy) / cam_cfg->fy,
    1.0f};
  // Rotate ray
  float dist;
  vec3 ray = rotateVec(cam_cfg->transform_matrix, forward_ray);
  vec3 pos = {cam_cfg->transform_matrix[12], cam_cfg->transform_matrix[13], cam_cfg->transform_matrix[14]};

  test[0] = pos.x;
  test[1] = pos.y;
  test[2] = pos.z;

  /* int64_t hit = raycast(tree_cfg, cam_cfg->pos, ray, &dist, data); */
  int64_t hit = raycast(tree_cfg, pos, ray, &dist, data);
  image[ind] = dist;
}

void kernel raycast_index(__global int64_t* data, __global OctreeConfig* tree_cfg, __global CameraConfig* cam_cfg, __global int64_t* image, __global float* test) {
  // Ray trace value for each pixel
  int i = get_global_id(0);
  int j = get_global_id(1);
  int ind = i*cam_cfg->im_cols + j;
  vec3 forward_ray = {
    (float)(j - cam_cfg->cx) / cam_cfg->fx,
    (float)(i - cam_cfg->cy) / cam_cfg->fy,
    1.0f};
  // Rotate ray
  float dist;
  vec3 ray = rotateVec(cam_cfg->transform_matrix, forward_ray);
  vec3 pos = {cam_cfg->transform_matrix[12], cam_cfg->transform_matrix[13], cam_cfg->transform_matrix[14]};

  test[0] = pos.x;
  test[1] = pos.y;
  test[2] = pos.z;

  /* int64_t hit = raycast(tree_cfg, cam_cfg->pos, ray, &dist, data); */
  int64_t hit = raycast(tree_cfg, pos, ray, &dist, data);
  image[ind] = hit;
}
