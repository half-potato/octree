#include <cstdio>

#include <cuda.h>
#include <iostream>
#include <curand.h>
#include <curand_kernel.h>

#include "octree/octree.h"
#include "octree/cuda_raycaster.h"

#define PASS_OR_FAIL(x) (x) ? "\x1B[0;32mPASS\x1B[0m" : "\x1B[1;31mFAIL\x1B[0m"

using namespace octree;

const std::vector<vec3> edge_sources = {
  {-9.944485, -7.774033, 3.065767},
  {9.028589, 0.453688, 6.832436},
  {8.016354, 9.650476, 9.019857},
  {-0.805279, 1.663546, 0.107616},
  {-0.744965, -9.307243, 4.565362},
};
const std::vector<vec3> edge_voxels = {
  {-9.392727, -7.767507, 3.619267},
  {15.524971, -9.188999, -1.179890}, // This one voxel causes issues
  {0.011543, 2.334883, 0.019962},
  {8.118879, 9.911964, 8.955095},
  {-8.964530, -8.524374, -10.912760},
};

template<typename T>
std::vector<T> randomVector(int n, T minv, T maxv) {
  std::vector<T> vals;
  for (int i = 0; i<n; i++) {
    
    vals.push_back(
      static_cast <T> (rand()) / static_cast <T> (RAND_MAX) * (maxv-minv) + minv);
  }
  return vals;
}

std::vector<vec3> randomVecs(int n, float minv, float maxv) {
  std::vector<vec3> vecs;
  for (int i = 0; i<n; i++) {
    vec3 v;
    v.x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    v.y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    v.z = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    vecs.push_back(v);
  }
  return vecs;
}

void printKey(std::vector<uint8_t> key) {
  for (auto k : key) {
    printf("%i,", k);
  }
  printf("\n");
}

bool test_raytrace_depth() {

  std::vector<vec3> sources = randomVecs(100, -10, 10);
  std::vector<vec3> voxels = randomVecs(100, -20, 20);
  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};

  // Adding edge cases
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      Octree tree(res);
      tree.insert(voxel, dat);
      for (auto source : sources) {
        auto ray = voxel - source;
        float dist;
        int64_t * result = tree.raycast(source, ray*randomVector<float>(1, 1e-5, 2)[0], &dist);
        if (std::abs(dist-distance(source, voxel)) > res*sqrt(3.0f)) {
          if (result == NULL) {
            printf("Failed to hit first.\n");
            return false;
          }
          printf("Failed to find correct distance\n");
          printf("Dist: %f vs Reported Dist: %f\n", distance(source, voxel), dist);
          printf("Diff: %f\n", std::abs(distance(source, voxel) - dist));
          printf("Voxel: ");
          vec3print(voxel);
          printf("\n");
          printf("Source: ");
          vec3print(source);
          printf("\n");
          printf("Ray: ");
          vec3print(ray);
          printf("\n");
          printf("Origin: ");
          vec3print(tree.origin);
          printf("\n");
          printf("Res: %f\n", res);
          return false;
        }
        // Test if we can hit the voxel using the given dist
        auto coord = ray/glm::length(ray)*(dist+res*1e-2f) + source;
        float eps = res/10;
        bool found = false;
        for (float x=-eps; x<eps+eps/2; x+=eps) {
          for (float y=-eps; y<eps+eps/2; y+=eps) {
            for (float z=-eps; z<eps+eps/2; z+=eps) {
              int64_t * ret_res = tree.get(coord+vec3{x, y, z});
              found |= (ret_res != NULL);
            }
          }
        }
        if (!found) {
          printf("Failed to hit voxel using dist given\n");
          printf("Dist: %f vs Reported Dist: %f\n", distance(source, voxel), dist);
          printf("Diff: %f\n", std::abs(distance(source, voxel) - dist));
          auto k1 = tree.keyFromPt(voxel);
          auto k2 = tree.keyFromPt(coord);
          printf("Original Key: ");
          printKey(k1);
          printf("New Key: ");
          printKey(k2);
          printf("Voxel: ");
          vec3print(voxel);
          printf("\n");
          printf("Pred coord: ");
          vec3print(coord);
          printf("\n");
          printf("Source: ");
          vec3print(source);
          printf("\n");
          printf("Ray: ");
          vec3print(ray);
          printf("\n");
          printf("Origin: ");
          vec3print(tree.origin);
          printf("\n");
          printf("Res: %f\n", res);
          printf("Tried:\n");
          for (float x=-eps; x<eps+eps/2; x+=eps) {
            for (float y=-eps; y<eps+eps/2; y+=eps) {
              for (float z=-eps; z<eps+eps/2; z+=eps) {
                int64_t * ret_res = tree.get(coord+vec3{x, y, z});
                printf("Coord: ");
                vec3print(coord+vec3{x, y, z});
                printf("\nAnd got: %zu\n", ret_res);
              }
            }
          }
          return false;
        }
      }
    }
  }
  return true;
}

// __global__ bool
// nms_3d(
//     OctreeConfig * dtree_cfg,
//     long * doctree_data,
//     OctreeData * dmetadata)
// {
// }

// __global__
bool 
test_raytrace_ordering() {
  float maxv = 20;
  std::vector<vec3> sources = randomVecs(50, -10, 10);
  std::vector<vec3> voxels = randomVecs(50, -maxv, maxv);
  // Adding edge cases
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  const std::vector<float> ranges = {std::sqrt(2.0f)+EPS, 1.5, 1.7, 2};

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      for (auto source : sources) {
        auto ray = voxel - source;
        for (size_t i=0; i<ranges.size(); i++) {
          Octree tree(res);
          tree.insert(voxel, dat);
          float dist;
          int64_t * result;
          result = tree.raycast(source, ray, &dist);
          if (result == NULL || *result != dat) {
            printf("Failed initial check\n");
            printf("Result: ");
            if (result == NULL) {
              printf("NULL\n");
            } else {
              printf("%i\n", (int)*result);
            }
            return false;
          }
          tree.insert(voxel + ray*ranges[i], dat+1);
          result = tree.raycast(source, ray, &dist);
          if (tree.get(voxel) == NULL || *tree.get(voxel) != dat) {
            printf("WTF\n");
            return false;
          }
          if (result == NULL || *result != dat) {
            printf("Radius: %f\n", tree.getRadius());
            printf("Res: %f\n", res);
            printf("Result: %i\n", (int)*result);
            printf("Dat: %i\n", (int)dat);
            printf("Close Voxel: ");
            vec3print(voxel);
            printf("\n");
            printf("Far Voxel: ");
            vec3print(voxel + ray*ranges[i]);
            printf("\n");
            printf("Source: ");
            vec3print(source);
            printf("\n");
            printf("Ray: ");
            vec3print(ray);
            printf("\n");
            printf("Reported Dist: %f\n", dist);
            printf("Closer Voxel Dist: %f\n", distance(voxel, source));
            printf("Further Voxel Dist: %f\n", distance(voxel + ray*ranges[i], source));
            return false;
          }
        }
      }
    }
  }
  return true;
}

int main() {
  // Ordered in such a way that the first failure should be checked before progressing
  // Insertion Retrieval Tests
  /*
  printf("Voxel Coordinate Test: %s\n",                       PASS_OR_FAIL(test_voxel_coords()));
  printf("Insertion Retrieval Occupancy Test: %s\n",          PASS_OR_FAIL(test_insertion_retrieval_occupancy()));
  printf("Insertion Retrieval Value Test: %s\n",              PASS_OR_FAIL(test_insertion_retrieval_value()));
  printf("Insertion Retrieval Uniquenesss Test: %s\n",        PASS_OR_FAIL(test_insertion_retrieval_uniqueness()));
  printf("Value Change Test: %s\n",                           PASS_OR_FAIL(test_change_value()));

  // Resize
  printf("Resize Test: %s\n",                                 PASS_OR_FAIL(test_resize()));
  printf("Resize During Insertion Test: %s\n",                PASS_OR_FAIL(test_resize_insertion()));
  printf("Correct Size Test: %s\n",                           PASS_OR_FAIL(test_correct_size()));

  // Ray tracing
  printf("Ray Intersection Test: %s\n",                       PASS_OR_FAIL(test_ray_intersect()));
  printf("Ray Intersection Sides Test: %s\n",                 PASS_OR_FAIL(test_ray_intersect_sides()));
  printf("Linked List Sorting Test: %s\n",                    PASS_OR_FAIL(test_sorted_list()));
  printf("Raytrace Hit Test: %s\n",                           PASS_OR_FAIL(test_raytrace_hit()));
  printf("Iterator Test: %s\n",                               PASS_OR_FAIL(test_iterator()));
  printf("Neighborhood Test: %s\n",                           PASS_OR_FAIL(test_neighborhood()));
  */
  printf("Raytrace Depth Test: %s\n",                         PASS_OR_FAIL(test_raytrace_depth()));
  printf("Raytrace Ordering Test: %s\n",                      PASS_OR_FAIL(test_raytrace_ordering()));

  // printf("Metadata Association Test: %s\n",                   PASS_OR_FAIL(test_metadata_association()));
  // printf("Convert Key Test: %s\n",                            PASS_OR_FAIL(test_convert_key()));
  // printf("Metadata Point Conversion Test: %s\n",              PASS_OR_FAIL(test_ptkeyconv()));

  /*
  // Other
  printf("Save Test: %s\n",                                   PASS_OR_FAIL(test_saving()));
  printf("MetaOctree Save Test: %s\n",                        PASS_OR_FAIL(test_meta_saving()));
  */

  return 0;
}
