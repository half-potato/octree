#include <torch/extension.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <octree/cuda_raycaster.h>

__device__ const float EPS = 1e-8;

__device__
inline float3 operator-(const float3 &a, const float3 &b) {
  return {a.x-b.x, a.y-b.y, a.z-b.z};
}

__device__
inline float3 operator+(const float3 &a, const float3 &b) {
  return {a.x+b.x, a.y+b.y, a.z+b.z};
}

__device__
inline float3 operator+(const float3 &a, const float &b) {
  return {a.x+b, a.y+b, a.z+b};
}

__device__
inline float3 operator*(const float &a, const float3 &b) {
  return {a*b.x, a*b.y, a*b.z};
}

__device__
float3 cross(float3 & a, float3 & b) {
  float3 c {
    a.y*b.z - a.z*b.y,
    a.z*b.x - a.x*b.z,
    a.x*b.y - a.y*b.x
  };
  return c;
}

__device__
float dot(float3 a, float3 b) {
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

__device__
float intersect_triangle(float3 Rorigin, float3 Rdir, float3 A, float3 B, float3 C) {

  float3 E1 = B-A;
  float3 E2 = C-A;
  float3 h = cross(Rdir, E2);
  float a = dot(E1, h);
  if (a > -EPS && a < EPS) {
      return false;
  }
  float f = 1.0/a;
  float3 s = Rorigin - A;
  float u = f * dot(s, h);
  if (u < 0 || u > 1) {
      return false;
  }
  float3 q = cross(s, E1);
  float v = f * dot(Rdir, q);
  if (v < 0 || u + v > 1) {
      return false;
  }
  float t = f * dot(E2, q);
  return t > EPS;
}

__device__
int get_containing_face(float3 pt, float3 *verts, int3 *faces, int num_faces) {
  for (int i=0; i<num_faces; i++) {
    int3 &face = faces[i];
    if (intersect_triangle({0, 0, 0}, pt, verts[face.x], verts[face.y], verts[face.z])) {
      return i;
    }
  }
  return 0;
}

__global__ void
kern_get_face_inds(int32_t *face_inds, float3 *pts, size_t num_pts, float3 *verts, int3 *faces, int num_faces) {
  size_t i = threadIdx.x + blockIdx.x * blockDim.x;
  if (i >= num_pts) {
    return;
  }
  int ind = get_containing_face(pts[i], verts, faces, num_faces);
  face_inds[i] = ind;
}

torch::Tensor
cuda_get_face_inds(
    torch::Tensor & pts,
    torch::Tensor & faces,
    torch::Tensor & vertices)
{
  size_t num_pts = pts.size(0);
  size_t num_faces = faces.size(0);
  size_t num_verts = vertices.size(0);

  float3 *c_pts = (float3 *)malloc(sizeof(float3)*num_pts);
  int3 *c_faces = (int3 *)malloc(sizeof(int3)*num_faces);
  float3 *c_verts = (float3 *)malloc(sizeof(float3)*num_verts);

  memcpy(c_pts, pts.to(torch::kFloat32).flatten().contiguous().data_ptr<float>(), sizeof(float3)*num_pts);
  memcpy(c_verts, vertices.to(torch::kFloat32).flatten().contiguous().data_ptr<float>(), sizeof(float3)*num_verts);
  memcpy(c_faces, faces.to(torch::kInt32).flatten().contiguous().data_ptr<int32_t>(), sizeof(int3)*num_faces);

  // float3 *c_pts = (float3 *)pts.to(torch::kFloat32).flatten().contiguous().data_ptr<float>();
  // float3 *c_verts = (float3 *)vertices.to(torch::kFloat32).flatten().contiguous().data_ptr<float>();
  // int3 *c_faces = (int3 *)faces.to(torch::kInt32).flatten().contiguous().data_ptr<int32_t>();

  /*
  for (size_t i=0; i<num_pts; i++) {
    if (c_pts[i].x != pts[i][0].item<float>()) {
      printf("%i: (%f, %f, %f) vs (%f, %f, %f)", i, c_pts[i].x, c_pts[i].y, c_pts[i].z, pts[i][0].item<float>(), pts[i][1].item<float>(), pts[i][2].item<float>());
      break;
    }
    c_pts[i].x = pts[i][0].item<float>();
    c_pts[i].y = pts[i][1].item<float>();
    c_pts[i].z = pts[i][2].item<float>();
  }
  for (size_t i=0; i<num_verts; i++) {
    c_verts[i].x = vertices[i][0].item<float>();
    c_verts[i].y = vertices[i][1].item<float>();
    c_verts[i].z = vertices[i][2].item<float>();
  }
  for (size_t i=0; i<num_faces; i++) {
    c_faces[i].x = faces[i][0].item<int32_t>();
    c_faces[i].y = faces[i][1].item<int32_t>();
    c_faces[i].z = faces[i][2].item<int32_t>();
  }
  */

  int32_t *d_face_inds; 
  float3 *d_pts, *d_verts;
  int3 *d_faces;
  checkCudaErrors(cudaMalloc(&d_face_inds, sizeof(int32_t)*num_pts));
  checkCudaErrors(cudaMalloc(&d_pts, sizeof(float3)*num_pts));
  checkCudaErrors(cudaMalloc(&d_verts, sizeof(float3)*num_verts));
  checkCudaErrors(cudaMalloc(&d_faces, sizeof(int3)*num_faces));

  checkCudaErrors(cudaMemcpy(d_pts, c_pts, sizeof(float3)*num_pts, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(d_verts, c_verts, sizeof(float3)*num_verts, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(d_faces, c_faces, sizeof(int3)*num_faces, cudaMemcpyHostToDevice));

  size_t block_size = 1000;
  size_t num_blocks = size_t(num_pts / block_size+1);
  kern_get_face_inds<<<num_blocks, block_size>>>(d_face_inds, d_pts, num_pts, d_verts, d_faces, num_faces);

  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kInt32).device(cpu);
  torch::Tensor face_inds = torch::zeros({(long)num_pts}, options);
  int32_t *out = (int32_t*)malloc(sizeof(int32_t)*num_pts);
  checkCudaErrors(cudaMemcpy(face_inds.contiguous().data_ptr<int32_t>(), d_face_inds, sizeof(int32_t)*num_pts, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(out, d_face_inds, sizeof(int32_t)*num_pts, cudaMemcpyDeviceToHost));
  cudaGetLastError();
  // for (size_t i=0; i<num_pts; i++) {
    // std::cout << out[i];
  // }
  // checkCudaErrors(cudaMemcpy(out, d_face_inds, sizeof(int32_t)*num_pts, cudaMemcpyDeviceToHost));

  checkCudaErrors(cudaFree(d_pts));
  checkCudaErrors(cudaFree(d_verts));
  checkCudaErrors(cudaFree(d_faces));
  checkCudaErrors(cudaFree(d_face_inds));
  free(c_pts);
  free(c_verts);
  free(c_faces);

  return face_inds;
}
