// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <cmath>
#include <cstring>
#include <string>
#include <queue>
#include <iostream>
#include <fstream>
#include "octree/octree.h"
#include "cpptoml.h"

namespace octree {

OctreeNode emptyNode(int64_t parent_idx, int64_t leaf_idx) {
  return OctreeNode{
    {NULL_VOX,NULL_VOX,NULL_VOX,NULL_VOX,
               NULL_VOX,NULL_VOX,NULL_VOX,NULL_VOX},
    0, parent_idx, leaf_idx,
  };
}

Octree::Octree(float scale, vec3 origin)
  : scale(scale), origin(origin), depth(0), version("octree-v2")
{
  data.push_back(emptyNode());
}

// pure
bool
Octree::withinBounds(vec3 pt)
{
  float min_x = origin.x - getRadius();
  float max_x = origin.x + getRadius();

  float min_y = origin.y - getRadius();
  float max_y = origin.y + getRadius();

  float min_z = origin.z - getRadius();
  float max_z = origin.z + getRadius();
  return (min_x < pt.x && max_x > pt.x) &&
         (min_y < pt.y && max_y > pt.y) &&
         (min_z < pt.z && max_z > pt.z);
}

// pure
float
Octree::getRadius() {
  return float(std::pow(2, depth)) * scale;
}

float
Octree::voxelCoord(float v, int local_depth) {
  // assumes 0 origin
  float res = scale*pow(2, depth-local_depth+1);
  // Add 0.5 to get to the center of the voxel
  float voxelCenter = (floorf(v/res)+0.5)*res;
  // printf("%f\n", voxelCenter);
  if (local_depth == 0) {
    return v/res;
  }
  return (v - voxelCenter)/res;
}

// pure
int
Octree::childIndex(vec3 pt, int local_depth) {
  // Function assumes point is within bounds
  // First, get location within the parent voxel
  float vx = voxelCoord(pt.x-origin.x, local_depth);
  float vy = voxelCoord(pt.y-origin.y, local_depth);
  float vz = voxelCoord(pt.z-origin.z, local_depth);

  // printf("Input: ");
  // vec3print(pt);
  // printf("\nVx: %f, Vy: %f, Vz: %f\n", vx, vy, vz);
  // This is the location with in the voxel
  // This bit works for sure
  int index = 0;
  index |= (vx < 0) << 2;
  index |= (vy < 0) << 1;
  index |= (vz < 0) << 0;
  return index;
  /*
  for (int i=0; i<8; i++) {
    const vec3 & bounds = VOXEL_NUMBERING[i];
    if ((vx*bounds.x <= 1 && vx*bounds.x >= 0) &&
        (vy*bounds.y <= 1 && vy*bounds.y >= 0) &&
        (vz*bounds.z <= 1 && vz*bounds.z >= 0))
    {
      printf(", i: %i versus %i\n", i, index);
      return i;
    }
  }
  return 0;
  */
}

void
Octree::incrementDepth() {
  // this operation expands the tree from the center
  OctreeNode old_tree = *getNode(0);
  *getNode(0) = emptyNode();
  depth++;
  // Iterated through each child and assign it to the inner child position
  for (int i=0; i<8; i++) {
    int64_t step_child = old_tree.children[i];
    if (step_child == NULL_VOX)
      continue;
    vec3 pt = VOXEL_NUMBERING[i] * getRadius()/4.0f + origin;
    int j = childIndex(pt, 1);
    int64_t new_child_ind = data.size();
    OctreeNode new_child = emptyNode(0, i);
    new_child.children[j] = step_child;
    if (step_child != NULL_VOX && depth != 1) {
      getNode(step_child)->parent_idx = new_child_ind;
      getNode(step_child)->leaf_idx = j;
    }
    data.push_back(new_child);
    getNode(0)->children[i] = new_child_ind;
  }
}

OctreeKey 
Octree::insert(vec3 pt, int64_t val) {
  if (!withinBounds(pt)) {
    // Reroot tree

    float max_diff = std::max( std::max(
          std::abs(pt.x - origin.x),
          std::abs(pt.y - origin.y)),
        std::abs(pt.z - origin.z));
    float diff = int(ceil(max_diff/getRadius()));
    // int required_depth = ceil(log(max_diff/scale)/log(2));
    int iterations = log(diff)/log(2)+1;
    // if (iterations != required_depth-depth) {
    //   printf("Required Depth: %i\n", required_depth);
    //   printf("Current Depth: %i\n", depth);
    //   printf("Iterations: %i\n", iterations);
    // }
    for (int i=0; i<iterations; i++) {
      incrementDepth();
    }
  }
  // Traverse tree, creating nodes
  // Get leaf
  int64_t current_branch = 0;
  for (int i=0; i < depth; i++) {
    int j = childIndex(pt, i);
    if (getNode(current_branch)->children[j] == NULL_VOX) {
      getNode(current_branch)->children[j] = data.size();
      data.push_back(emptyNode(current_branch, j));
    }
    current_branch = getNode(current_branch)->children[j];
  }
  // set data
  int val_j = childIndex(pt, depth);
  getNode(current_branch)->children[val_j] = val;
  return {
    .node_idx = current_branch, .child_idx = val_j
  };
}

int64_t *
Octree::get(vec3 pt) {
  if (!withinBounds(pt)) {
    return NULL;
  }
  int64_t current_branch = 0;
  for (int i=0; i < depth; i++) {
    int j = childIndex(pt, i);

    if (getNode(current_branch)->children[j] == NULL_VOX) {
      return NULL;
    }
    current_branch = getNode(current_branch)->children[j];
  }
  // set data
  int val_j = childIndex(pt, depth);
  if (getNode(current_branch)->children[val_j] == NULL_VOX) {
    return NULL;
  }
  return &getNode(current_branch)->children[val_j];
}

int64_t *
Octree::raycast(const vec3 & source,
                const vec3 & dir,
                float * dist)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with origin and local_depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  vec3 ray = glm::normalize(dir);

  // initialize stack
  int max_len = int(pow(2, depth));
  vec3 voxel_centers[max_len];
  int depths[max_len];
  // Initialize the depths so we know when we run out
  for (int i=0; i<max_len; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[max_len];

  int stack_head = 0;
  voxel_centers[stack_head] = origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;
  int stop_depth = depth;

  while (depths[stack_head] != -1) {
    vec3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int local_depth = depths[stack_head];
    // if you figure out why this is correct, I will legit give you money
    float mul = (local_depth == stop_depth) ? 1 : -1;

    int head = -1;
    float s = scale*pow(2, depth-local_depth);
    FLinkedList list[8];
    vec3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNode(voxel_ind)->children[i] == NULL_VOX) {
        continue;
      }
      auto & bound = VOXEL_NUMBERING[i];

      centers[i] = voxel_origin + s*bound/2.0f;
      vec3 vmin = centers[i] - s/2.0f-EPS;
      vec3 vmax = centers[i] + s/2.0f+EPS;

      float ray_length;
      bool collision = rayBoxIntersect(source, ray, vmin, vmax, &ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(list, head, i, mul*ray_length);
    }
    int selection = head;

    // Iterate through the sorted voxels
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      float d = mul*list[selection].score;
      selection = list[selection].child;
      int64_t & new_ind = getNode(voxel_ind)->children[j];

      // Check if voxel is empty
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum local_depth, return
      if (local_depth == stop_depth) {
        *dist = d;
        return &new_ind;
      }
      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = local_depth+1;
      stack_head = (stack_head-1 + max_len) % max_len;
    }
    stack_head = (stack_head+1)%max_len;
  }
  return NULL;
}

std::string
Octree::generateHeader()
{
  auto root = cpptoml::make_table();
  root->insert("origin_x", origin.x);
  root->insert("origin_y", origin.y);
  root->insert("origin_z", origin.z);
  root->insert("scale", scale);
  root->insert("depth", depth);
  root->insert("version", version);
  root->insert("num_nodes", numNodes());
  std::stringstream test;
  test << PREHEADER;
  test << *root;
  test << "\n";

  // counter number of lines and add it to the header
  int number_of_lines = 1; // +1 for the line we are about to add
  std::string line;
  while (std::getline(test, line))
    ++number_of_lines;
  root->insert("header_length", number_of_lines);

  std::stringstream ss;
  ss << PREHEADER;
  ss << *root;
  ss << "\n";
  return ss.str();
}

std::shared_ptr<cpptoml::table>
Octree::parseHeader(std::ifstream & is, int *header_size, int *num_nodes)
{
  // Find header length
  std::string line;
  int header_length = 0;
  is.seekg(0);
  while (std::getline(is, line, '\n'))
  {
    std::stringstream ss{line};
    cpptoml::parser p{ss};
    auto root = p.parse();
    if (root->get_as<int>("header_length")) {
      header_length = root->get_as<int>("header_length").value_or(0);
      break;
    }
  }
  // Reset and read header to stream
  is.seekg (0, is.beg);
  std::stringstream ss;
  for (int i=0; i<header_length; ++i) {
    std::getline(is, line);
    ss << line << std::endl;
  }
  *header_size = is.tellg();

  // Parse toml and set values in octree
  cpptoml::parser p{ss};
  auto root = p.parse();
  origin.x = root->get_as<double>("origin_x").value_or(0);
  origin.y = root->get_as<double>("origin_y").value_or(0);
  origin.z = root->get_as<double>("origin_z").value_or(0);
  scale = root->get_as<double>("scale").value_or(0);
  depth = root->get_as<int>("depth").value_or(0);
  version = root->get_as<std::string>("version").value_or("");
  *num_nodes = root->get_as<int>("num_nodes").value_or(0);
  return root;
}

Octree::Octree(std::string fname, size_t max_chunk_size) {
  // This is just the sample code for reading and writing
  std::ifstream is(fname, std::ifstream::binary);
  // std::ifstream is(fname);
  if (is) {
    // get length of file:
    is.seekg (0, is.end);
    size_t length = is.tellg();
    is.seekg (0, is.beg);

    printf("Reading %zu characters...\n", length);

    int header_size, num_nodes;
    parseHeader(is, &header_size, &num_nodes);
    // is.seekg (header_size, is.beg);

    // read data in chunks
    // size_t num_nodes = (length-header_size)/sizeof(OctreeNode);
    data.resize(num_nodes);
    for (size_t i=0; i<sizeof(OctreeNode)*num_nodes; i+=max_chunk_size) {
      size_t chunk_size = std::min(sizeof(OctreeNode)*num_nodes-i, max_chunk_size);
      is.read (((char *)data.data())+i, chunk_size);
    }
    is.close();

  } else {
    std::cout << "File " << fname << " was not found." << std::endl;
  }
}

int 
Octree::save(std::string fname, size_t max_chunk_size) {
  std::cout << "Saved tree with nodes: " << numNodes() << std::endl;
  std::ofstream outfile (fname, std::ofstream::binary);

  outfile << generateHeader();

  // Write body
  size_t num_node_per_buffer = max_chunk_size/sizeof(OctreeNode);

  for (size_t i=0; i<data.size(); i+=num_node_per_buffer) {
    size_t num_nodes = std::min(num_node_per_buffer, data.size()-i);
    outfile.write((char *)(data.data()+i), num_nodes*sizeof(OctreeNode));
  }

  outfile.close();
  return 0;
}

std::vector<uint8_t>
Octree::keyFromPt(vec3 pt) {
  std::vector<uint8_t> key;
  int64_t current_branch = 0;
  for (int i=0; i <= depth; i++) {
    int j = childIndex(pt, i);
    if (getNode(current_branch)->children[j] == NULL_VOX) {
      return key;
    }
    key.push_back(j);
    current_branch = getNode(current_branch)->children[j];
  }
  return key;
}

vec3 
Octree::ptFromKey(std::vector<uint8_t> key) {
  vec3 output = origin;
  for (size_t i=0; i<key.size(); i++) {
    float s = scale*pow(2, depth-i);
    auto & bound = VOXEL_NUMBERING[key[i]];
    output += s*bound/2.0f;
  }
  return output;
}

int64_t * 
Octree::get(OctreeKey key) {
  assert(key.node_idx >=0 && size_t(key.node_idx) < numNodes());
  return &getNode(key.node_idx)->children[key.child_idx];
}

OctreeKey
Octree::convertKey(std::vector<uint8_t> key) {
  if (key.size() == 0) {
    return {
        .node_idx = 0, .child_idx=-1,
    };
  }
  if (key.size() == 1) {
    return {
        .node_idx = 0, .child_idx=key[0],
    };
  }
  auto child_idx = key.back();
  OctreeKey okey {
      .node_idx = *indexTree(key, key.size()-2), .child_idx=child_idx
  };
  return okey;
}

std::vector<uint8_t> 
Octree::convertKey(OctreeKey okey) {
  std::vector<uint8_t> key (depth+1, 0);
  key[depth] = uint8_t(okey.child_idx);

  auto node = getNode(okey.node_idx);
  for (int i=depth-1; i>=0; i--) {
    key[i] = uint8_t(node->leaf_idx);
    if (node->parent_idx == 0) {
      return key;
    }
    node = getNode(node->parent_idx);
  }
  printf("Error: convert key ended before reaching root node\n");
  printf("Node: %zu, child: %zu, end parent: %zu\n", okey.node_idx, okey.child_idx, node->parent_idx);
  printf("Key: ");
  for (auto k : key) {
    printf("%i,", k);
  }
  printf("Depth: %i\n", depth);
  return std::vector<uint8_t>();
}

std::vector<OctreeKey> 
Octree::neighborhood(vec3 center, float radius)
{
  auto key = keyFromPt(center);
  int vox_radius = radius / scale;
  int levels_up = std::min(int(ceil(log(vox_radius)/log(2))+2), depth);
  std::vector<OctreeKey> output;

  if (levels_up < 1) {
    // Then there is no space to iterate
    output.push_back(convertKey(key));
    return output;
  }
  // Add self because sometimes it can be missing
  if (key.size() == depth+1) {
    // output.push_back(convertKey(key));
  } else {
    levels_up = depth + 1 - key.size();
  }

  key.resize(depth-levels_up+1);
  OctreeIterator a (key, std::vector<uint8_t>(levels_up, 0), this);
  OctreeIterator b (std::vector<uint8_t>(1, 7), std::vector<uint8_t>(depth, 7), this);

  for (OctreeIterator k=a; a!=b; a++) {
    // check if in radius
    vec3 loc = ptFromKey(*k);
    if (radius+scale > glm::distance(center, loc)) {
      output.push_back(convertKey(*k));
    }
  }
  return output;
}

int64_t *
Octree::indexTree(std::vector<uint8_t> key, int max_depth)
{
  assert(key.size() >= 1);
  if (max_depth == -1) {
    max_depth = key.size()-1;
  }
  int64_t ind = 0;
  for (int i=0; i<max_depth; i++) {
    ind = this->getNode(ind)->children[key[i]];
    if (ind == NULL_VOX) {
      return NULL;
    }
  }
  return &this->getNode(ind)->children[key[max_depth]];
}

// Mutable recursion
bool 
Octree::OctreeIterator::goDown(int64_t ind, int local_depth) {
  int64_t new_ind = tree->getNode(ind)->children[key[local_depth]];
  if (new_ind == NULL_VOX) {
    // If we can't go down, then go to the side
    return incrementKey(ind, local_depth);
  } else {
    if (local_depth == tree->depth) {
      return true;
    } else {
      // If we can go down, try to go down again
      return goDown(new_ind, local_depth+1);
    }
  }
}

// Mutable recursion
bool 
Octree::OctreeIterator::goUp(int64_t ind, int local_depth) {
  // Go up a level, then try to go to the side
  for (size_t j=local_depth; j<key.size(); j++) {
    key[j] = 0;
  }
  // printf("%zu, %i\n", tree->getNode(ind)->parent_idx, local_depth-1);
  return incrementKey(tree->getNode(ind)->parent_idx, local_depth-1);
}

// Mutable recursion
bool 
Octree::OctreeIterator::incrementKey(int64_t ind, int local_depth) {
  // We wouldn't want to increment past 7
  if (key[local_depth] >= 7) {
    if (local_depth == root_size) {
      // Finished iterating
      return false;
    } else {
      return goUp(ind, local_depth);
    }
  } else {
    // Go to the side
    key[local_depth]++;
    if (tree->getNode(ind)->children[key[local_depth]] == NULL_VOX) {
      // Go to the side again
      return incrementKey(ind, local_depth);
    } else {
      if (local_depth==key.size()-1) {
        return true;
      } else {
        return goDown(ind, local_depth);
      }
    }
  }
}

};
