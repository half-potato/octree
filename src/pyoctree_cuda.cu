#include <torch/extension.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include "octree/octree.h"
#define LOG() printf("%s: %i\n", __FILE__, __LINE__)
#include "octree/cuda_raycaster.h"
#include "octree/seg_raycaster.h"

#include <vector>

using glm::vec3;
using namespace octree;

void
gaussian_kernel_3d(
    float *kernel,
    const size_t size,
    const float mu,
    const float std)
{
  float multi = pow(2*M_PI*std, -3.0/2);
  float s = 0;
  for (size_t i=0; i<size; i++) {
    for (size_t j=0; j<size; j++) {
      for (size_t k=0; k<size; k++) {
        float coord = i - size/2.0 - mu;
        float val = multi*exp(-0.5*coord*coord/std);
        s += val;
      }
    }
  }
  // normalize to have sum of 1
  for (size_t i=0; i<size; i++) {
    for (size_t j=0; j<size; j++) {
      for (size_t k=0; k<size; k++) {
        float coord = i - size/2.0 - mu;
        float val = multi*exp(-0.5*coord*coord/std);
        kernel[i*size*size + j*size + k] = val/s;
      }
    }
  }
}

std::vector<torch::Tensor>
cuda_render_depth(
    std::string octomap_path,
    int im_cols, int im_rows,
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib)
{
  torch::Device cpu("cpu");
  torch::Device gpu("cuda");
  const int batch_size = pose_tensors.size();

  CameraConfig cfg {
    .im_rows = im_rows, .im_cols=im_cols,
    .fx = calib[0], .fy = calib[1], .cx = calib[2], .cy = calib[3],
  };
  octree::MetaOctree tree (octomap_path);
  octree::CudaRaycaster caster (tree, cfg);

  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  std::vector<torch::Tensor> depth_maps(batch_size);
  for(int query_index=0; query_index<batch_size; query_index++) {
    for (int i=0; i<16; i++) {
      cfg.transform_matrix[i] = pose_tensors[query_index].flatten().contiguous().data_ptr<float>()[i];
    }
    depth_maps[query_index] = torch::zeros({im_rows, im_cols}, options);
    caster.renderDepth(cfg, depth_maps[query_index].data_ptr<float>());
  }
  return depth_maps;
}

torch::Tensor
cuda_raycast_nms(
    std::string octomap_path,
    std::vector<torch::Tensor> & depth_maps,
    std::vector<torch::Tensor> & feature_maps, // [(score, subx, suby, size), im_rows, im_cols]
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib,
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio,
    float threshold,
    float min_seen_size,
    float max_unseen_size,
    int kernel_size, 
    int cell_size)
{
  torch::Device cpu("cpu");
  torch::Device gpu("cuda");
  const int batch_size = feature_maps.size();
  const int im_rows = feature_maps[0].size(1);
  const int im_cols = feature_maps[0].size(2);

  CameraConfig cfg {
    .im_rows = im_rows, .im_cols=im_cols,
    .fx = calib[0], .fy = calib[1], .cx = calib[2], .cy = calib[3],
  };
  octree::MetaOctree tree (octomap_path);
  octree::CudaRaycaster caster (tree, cfg, (size_t)cell_size);

  // vec3 tree_origin {tree_origin_v[0], tree_origin_v[1], tree_origin_v[2]};

  printf("Projecting Score\n");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  for(int query_index=0; query_index<batch_size; query_index++) {
    memcpy(cfg.transform_matrix, pose_tensors[query_index].flatten().contiguous().data_ptr<float>(), 16*sizeof(float));
    caster.projectScore(
        cfg, 
        depth_maps[query_index].flatten().contiguous().data_ptr<float>(),
        feature_maps[query_index][0].flatten().contiguous().data_ptr<float>(),
        feature_maps[query_index][1].flatten().contiguous().data_ptr<float>(),
        feature_maps[query_index][2].flatten().contiguous().data_ptr<float>(),
        feature_maps[query_index][3].flatten().contiguous().data_ptr<float>(),
        threshold, min_seen_size, max_unseen_size, cell_size);
  }

  // Construct kernel
  float kernel[kernel_size*kernel_size*kernel_size];
  gaussian_kernel_3d(kernel, kernel_size, 0.0, kernel_size/3.0);

  // convolve + nms
  printf("Convolving\n");
  if (kernel_size > 1) {
    caster.convolve3d(kernel, kernel_size, tree.getScale()/2);
  }
  printf("Running NMS\n");
  // return caster.runNMS(min_nms_radius, max_nms_radius, size_radius_multi, iou_thres, allowed_count_ratio);
  return caster.extract_points(threshold, -1);

}


torch::Tensor
cuda_neighborhood_sparsity_enforcement(
    torch::Tensor points,
    float neighborhood_radius,
    int max_features_in_neighborhood,
    float res)
{
  // First, construct an octree using the feature locations
  // points: (N, [x, y, z, score, size, count])
  torch::Tensor mean = points.mean(0);
  vec3 center {mean[0].item<float>(), mean[1].item<float>(), mean[2].item<float>()};
  // vec3 center {-0.316600, -0.267399, -1.552820};
  // vec3print(center);
  octree::MetaOctree tree (res, center);
  // METADATA_TAG
  tree.initializeType(MetaOctreeType::POINT_DATA_V2);
  long N = points.size(0);
  std::cout << "Constructing feature octree" << std::endl;
  for (long i=0; i<N; i++) {
    torch::Tensor pt = points[i];
    vec3 loc {pt[0].item<float>(), pt[1].item<float>(), pt[2].item<float>()};
    OctreeData *res = (OctreeData *)tree.get(loc);
    int64_t ind;
    if (res == NULL) {
      OctreeData data {
          .score=pt[3].item<float>(),
          .size=pt[4].item<float>(),
          .x=loc.x,
          .y=loc.y,
          .z=loc.z,
          .count=1,
          .val=0,
      };
      ind = tree.insert(loc, (void *)&data);
    } else {
      if (pt[3].item<float>() > res->score) {
        res->score = pt[3].item<float>();
        res->size = pt[4].item<float>();
        res->x = loc.x;
        res->y = loc.y;
        res->z = loc.z;
        res->count = 1;
      }
    }
  }

  // Next, run neighborhood operator on it
  CameraConfig cfg { };
  octree::CudaRaycaster caster (tree, cfg);

  // Last, convert the tree data back to points
  std::cout << "Running neighborhood sparsity enforcer" << std::endl;
  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  return caster.runSparsityEnforcer(neighborhood_radius, max_features_in_neighborhood);
}
