#include "octree/util.h"
#include <iostream>


void vec3print(vec3 a) {
  printf("(%f, %f, %f)", a.x, a.y, a.z);
}

int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score)
{
  // Returns the new head index

  FLinkedList item = {score, desired_address, -1};

  int new_head_index = head_index;
  int selection = head_index;
  int parent = head_index;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    list[0] = item;
    return 0;
  } else {
    while (selection != -1 && list[selection].score < item.score) {
      parent = selection;
      selection = list[selection].child;
    }
    if (selection == parent) {
      if (list[selection].score > item.score) {
        item.child = head_index;
        new_head_index = item.address;
      } else {
        list[parent].child = desired_address;
      }
    } else {
      item.child = selection;
      list[parent].child = desired_address;
    }
  }
  list[item.address] = item;
  return new_head_index;
}

bool
rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, float * ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  vec3 dirfrac = {1/rdir.x, 1/rdir.y, 1/rdir.z};
  // rpos is origin of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      *ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      *ray_length = tmax;
      return false;
  }

  *ray_length = tmin;
  return true;
}
