#include <GL/glew.h>
#include <fstream>
#include <sstream>
#include "octree/cl_raycaster.h"
#include <Eigen/Geometry>

#include <iostream>

#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include <GL/glut.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

// Temp includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "octree/octree.h"
#include "octree/metaoctree.h"

octree::MetaOctree tree;

GLuint vbo;
glm::vec3 camera_pos, camera_orientation;
octree::CameraConfig camera_config;
octree::CLRaycaster caster;
float *image;
int64_t *index_image;
GLubyte *image_data;
// float *image_data;

// mouse event handlers
int lastX = 0, lastY = 0;
int theButtonState = 0;
int theModifierState = 0;

// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 0.0, rotate_y = 0.0;
float translate_z = -30.0;

// keyboard interaction
void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
  glm::vec4 dir = {0, 0, 0, 1};
  switch (key) {
  case(27)  : exit(0);
  case('a') : dir.x += -0.15f; break;
  case('d') : dir.x += +0.15f; break;
  case('r') : dir.y += +0.15f; break;
  case('f') : dir.y += -0.15f; break;
  case('w') : dir.z += +0.15f; break;
  case('s') : dir.z += -0.15f; break;
  }
  glm::mat4 trans = glm::mat4(1.0f);
  trans = glm::rotate(trans, camera_orientation.x, glm::vec3(1, 0, 0));
  trans = glm::rotate(trans, camera_orientation.y, glm::vec3(0, 1, 0));
  trans = glm::rotate(trans, camera_orientation.z, glm::vec3(0, 0, 1));
  glm::vec4 result = trans * dir;
  camera_pos = {
    camera_pos.x+result.x,
    camera_pos.y+result.y,
    camera_pos.z+result.z
  };
  glutPostRedisplay();
}

void specialkeys(int key, int, int)
{
  switch (key) {
  case GLUT_KEY_LEFT:  camera_orientation.x += +0.02f; break;
  case GLUT_KEY_RIGHT: camera_orientation.x += -0.02f; break;
  case GLUT_KEY_UP:    camera_orientation.z += -0.02f; break;
  case GLUT_KEY_DOWN:  camera_orientation.z += -0.02f; break;
  }
}

// camera mouse controls in X and Y direction
void motion(int x, int y)
{
  int deltaX = lastX - x;
  int deltaY = lastY - y;

  if (deltaX != 0 || deltaY != 0) {
    if (theButtonState == GLUT_LEFT_BUTTON) {
      camera_orientation.y += deltaX * 0.01;
      camera_orientation.x -= deltaY * 0.01;
    }

    lastX = x;
    lastY = y;
    glutPostRedisplay();
  }
}

void mouse(int button, int state, int x, int y) {
  theButtonState = button;
  theModifierState = glutGetModifiers();
  lastX = x;
  lastY = y;

  motion(x, y);
}

float indexToCol(int64_t index) {
  int64_t node_ind = floor(index / 8);
  int64_t child = index % 8;
  int64_t dat = tree.getNode(node_ind)->children[child];
  int raw_score = dat >> 32;
  float * score = (float *)(&raw_score);
  return *score;
}

void render() {
  int w = glutGet(GLUT_WINDOW_WIDTH);
  int h = glutGet(GLUT_WINDOW_HEIGHT);
  camera_config.im_cols = w;
  camera_config.im_rows = h;
  camera_config.cx = w/2;
  camera_config.cy = h/2;
  camera_config.fx = w/2;
  camera_config.fy = w/2;

  // Construct transform matrix
  Eigen::Translation3f trans (camera_pos.x, camera_pos.y, camera_pos.z);
  printf("Camera pos: %f, %f, %f\n", camera_pos.x, camera_pos.y, camera_pos.z);
  Eigen::Quaternionf rot = 
    Eigen::AngleAxisf(camera_orientation.x, Eigen::Vector3f::UnitX()) *
    Eigen::AngleAxisf(camera_orientation.y, Eigen::Vector3f::UnitY()) *
    Eigen::AngleAxisf(camera_orientation.z, Eigen::Vector3f::UnitZ());
  Eigen::Matrix4f mat = (trans * rot).matrix();
  float *dat = mat.data();
  for (int i=0; i<16; i++) {
    camera_config.transform_matrix[i] = dat[i];
  }

  glClear(GL_COLOR_BUFFER_BIT);

  // Convert image
  caster.renderDepth(camera_config, image);
  // caster.renderIndex(camera_config, index_image);
  // caster.renderScore(camera_config, image);

  float cmax = -9999;
  float cmin = 9999;
  for (int i=0; i<camera_config.im_rows; i++) {
    for (int j=0; j<camera_config.im_cols; j++) {
      size_t ind = i*camera_config.im_cols + j;
      float val = image[ind];
      // int64_t index = index_image[ind];
      // float val = indexToCol(index);
      cmax = std::max(cmax, val);
      cmin = std::min(cmin, val);
    }
  }
  printf("Max: %f, min: %f\n", cmax, cmin);

  for (int i=0; i<camera_config.im_rows; i++) {
    for (int j=0; j<camera_config.im_cols; j++) {
      int ind = i*camera_config.im_cols + j;
      // int64_t index = index_image[ind];
      // float val = indexToCol(index);
      float val = image[ind];
      val = (val + cmin)/(cmax-cmin);
      image_data[ind*3 + 0] = (GLubyte)(val*255);
      image_data[ind*3 + 1] = (GLubyte)(val*255);
      image_data[ind*3 + 2] = (GLubyte)(val*255);
    }
  }

  /*
  // Convert image
  caster.renderIndex(camera_config, index_image);
  for (size_t i=0; i<camera_config.im_rows; i++) {
    for (size_t j=0; j<camera_config.im_cols; j++) {
      size_t ind = i*camera_config.im_cols + j;
      size_t ii = index_image[ind] / 8;
      size_t ij = index_image[ind] - ii * 8;
      int64_t v = tree.getNode(ii)->children[ij];
      int raw_score = v >> 32;
      float * val = (float *)(&raw_score);
      image_data[ind*3 + 0] = (GLubyte)(*val*20);
      image_data[ind*3 + 1] = (GLubyte)(*val*20);
      image_data[ind*3 + 2] = (GLubyte)(*val*20);
    }
  }
  */

  glDrawPixels(camera_config.im_cols, camera_config.im_rows, GL_RGB, GL_UNSIGNED_BYTE, image_data);
  
  // flip backbuffer to screen
  glutSwapBuffers();
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void initGL(int argc, char** argv)
{
  // init GLUT for OpenGL viewport
  glutInit(&argc, argv);
  // specify the display mode to be RGB and single buffering
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  // specify the initial window position
  glutInitWindowPosition(50, 50);
  // specify the initial window size
  glutInitWindowSize(camera_config.im_cols, camera_config.im_rows);
  // create the window and set title
  glutCreateWindow("MetaOctree Vis");

  // functions for user interaction
  glutKeyboardFunc(keyboard);
  // glutSpecialFunc(specialkeys);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutDisplayFunc(render);

  // initialise OpenGL extensions
  glewInit();

  // initialise OpenGL
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(0.0, camera_config.im_cols, 0.0, camera_config.im_rows);

  //create vertex buffer object
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  //initialise VBO
  glBufferData(GL_ARRAY_BUFFER, camera_config.im_cols * camera_config.im_rows * sizeof(float), 0, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glFinish();
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: vis path_to_octomap\n");
    return -1;
  }
  std::string path = argv[1];
  std::cout << "Loading octree from: " << path << std::endl;
  tree = octree::MetaOctree(path);
  printf("Depth: %i\n", tree.getDepth());
  printf("Resolution: %f\n", tree.getScale());
  caster = octree::CLRaycaster (tree);

  camera_config = {
    .im_rows = 1440, .im_cols = 2560,
    .fx = 720, .fy = 720, .cx = 1280, .cy = 720,
  };

  camera_pos = {0.000000, -1.134990, -2.151347};
  initGL(argc, argv);
  image = new float[camera_config.im_cols * camera_config.im_rows];
  index_image = new int64_t[camera_config.im_cols * camera_config.im_rows];
  image_data = new GLubyte[camera_config.im_cols * camera_config.im_rows * 3];

  // render();
  glutMainLoop();

  // register GLUT callback function to display graphics:

  // ==========================================================
  // Parse Output
  // ==========================================================
  delete[] image;
  delete[] image_data;

  return 0;
}

