// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <iostream>
#include <fstream>
#include "octree/octree.h"
#include "octree/metaoctree.h"

#define PASS_OR_FAIL(x) (x) ? "\x1B[0;32mPASS\x1B[0m" : "\x1B[1;31mFAIL\x1B[0m"

using namespace octree;

const std::vector<vec3> edge_sources = {
  {-9.944485, -7.774033, 3.065767},
  {9.028589, 0.453688, 6.832436},
  {8.016354, 9.650476, 9.019857},
  {-0.805279, 1.663546, 0.107616},
  {-0.744965, -9.307243, 4.565362},
};
const std::vector<vec3> edge_voxels = {
  {-9.392727, -7.767507, 3.619267},
  {15.524971, -9.188999, -1.179890}, // This one voxel causes issues
  {0.011543, 2.334883, 0.019962},
  {8.118879, 9.911964, 8.955095},
  {-8.964530, -8.524374, -10.912760},
};


template<typename T>
std::vector<T> randomVector(int n, T minv, T maxv) {
  std::vector<T> vals;
  for (int i = 0; i<n; i++) {
    
    vals.push_back(
      static_cast <T> (rand()) / static_cast <T> (RAND_MAX) * (maxv-minv) + minv);
  }
  return vals;
}

std::vector<vec3> randomVecs(int n, float minv, float maxv) {
  std::vector<vec3> vecs;
  for (int i = 0; i<n; i++) {
    vec3 v;
    v.x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    v.y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    v.z = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) * (maxv-minv) + minv;
    vecs.push_back(v);
  }
  return vecs;
}

void printTree(Octree & tree) {
  for (size_t i=0; i<tree.numNodes(); i++) {
    printf("%i: ", (int)i);
    for (size_t j=0; j<8; j++) {
      printf("%i, ", (int)tree.getNode(i)->children[j]);
    }
    printf("Parent: leaf %zu at node %zu\n", tree.getNode(i)->leaf_idx, tree.getNode(i)->parent_idx);
    printf("\n");
  }
}

void printKey(std::vector<uint8_t> key) {
  for (auto k : key) {
    printf("%i,", k);
  }
  printf("\n");
}

bool checkParents(Octree & tree) {
  for (size_t i=0; i<tree.numNodes(); i++) {
    int64_t pidx = tree.getNode(i)->parent_idx;
    if (pidx == -1) {
      // Root node
      continue;
    }
    if (size_t(tree.getNode(pidx)->children[tree.getNode(i)->leaf_idx]) == i) {
      continue;
    }
    printf("Error when checking parents: node %zu thinks %zu is it's parent\n", i, pidx);
    // printTree(tree);
    return false;
  }
  return true;
}

// =============================================================
// BASIC INSERTION TESTS
// =============================================================

bool test_voxel_coords() {
  const float v = 0.09;
  const float res = 0.1;
  const std::vector<vec3> dat = {
    {res, res, res},
    {2*res+0.001, 2*res+0.001, 2*res+0.001},
    {2*res, 2*res, 2*res},
    {0, 0, 0},
    {0.26, 0.08, 0.26},
    {0.26, 0.26, 0.26},
    {6.0/100, 6.0/100, 6.0/100},
    { v*3, -v*3, -v*3},
    { v*3,  v*3, -v*3},
    { v*3,  v*3,  v*3},
    {-v*3, -v*3, -v*3},
    {v, v, v}
  };
  Octree tree(res);
  for (int max_depth = 2; max_depth < 10; ++max_depth) {
    tree.depth = max_depth;
    for (size_t i = 0; i < dat.size(); ++i) {
      vec3 voxel_center = tree.origin;
      for (int depth = 0; depth <= max_depth; ++depth) {
        int j = tree.childIndex(dat[i], depth);
        float s = res*powf(2, max_depth-depth);
        voxel_center += s*VOXEL_NUMBERING[j]/2.0f;
        // printf("(%i) Add: ", j);
        // vec3print(s*VOXEL_NUMBERING[j]/2.0f);
        // printf("\n");
      }
      vec3 diff = abs(voxel_center - dat[i]);
      if ((diff.x > res/2+EPS) || (diff.y > res/2+EPS) || (diff.z > res/2+EPS)) {
        printf("\nDiff: ");
        vec3print(diff);
        printf("\n");
        printf("Voxel center: ");
        vec3print(voxel_center);
        printf("\n");
        printf("Dat[i]: ");
        vec3print(dat[i]);
        printf("\nRes: %f\n", res);
        return false;
      }
      // std::cout << diff << std::endl;
    }
  }
  return true;
}


bool test_insertion_retrieval_occupancy() {
  float maxv = 10;
  auto voxels = randomVecs(100, -maxv, maxv);
  std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  for (auto res : reses) {
    // Create tree with appropriate size
    Octree tree(res);
    int iters = log(10/res)/log(2) + 2;
    for (int i = 0; i<iters; i++) {
      tree.incrementDepth();
    }
    if (!checkParents(tree)) {
      return false;
    }
    for (size_t i = 0; i<voxels.size(); i++) {
      tree.insert(voxels[i], i);
    }
    if (!checkParents(tree)) {
      return false;
    }
    for (size_t i = 0; i<voxels.size(); i++) {
      int64_t * result = tree.get(voxels[i]);
      if (result == NULL) 
        return false;
    }
  }
  return true;
}

bool
test_insertion_retrieval_value() {
  float maxv = 10;
  auto voxels = randomVecs(100, -maxv, maxv);
  std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  for (auto res : reses) {
    for (size_t i=0; i<voxels.size(); i++) {
      vec3 & voxel = voxels[i];
      Octree tree(res);
      tree.insert(voxel, i);
      if (!checkParents(tree)) {
        return false;
      }
      int64_t * res = tree.get(voxel);
      if (res == NULL || *res != (int64_t)i) {
        return false;
      }
    }
  }
  return true;
}

bool
test_change_value() {
  float maxv = 10;
  auto voxels = randomVecs(100, -maxv, maxv);
  std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  for (auto res : reses) {
    for (size_t i=0; i<voxels.size(); i++) {
      vec3 & voxel = voxels[i];
      Octree tree(res);
      tree.insert(voxel, 0);
      int64_t * ptr = tree.get(voxel);
      if (ptr == NULL) {
        return false;
      }
      *ptr = i;
      int64_t * result = tree.get(voxel);
      if (result == NULL || *result != (int64_t)i) {
        return false;
      }
      if (!checkParents(tree)) {
        return false;
      }
    }
  }
  return true;
}

bool test_insertion_retrieval_uniqueness() {
  const float maxv = 1;
  // This test takes a while
  const auto voxels = randomVecs(20, -maxv, maxv);
  
  const std::vector<float> reses = {0.1};
  const int64_t dat = 1377;
  for (size_t i = 0; i<voxels.size(); i++) {
    for (auto res : reses) {
      // Create tree with appropriate size
      Octree tree(res);
      int iters = log(maxv/res)/log(2) + 2;
      for (int i = 0; i<iters; i++) {
        tree.incrementDepth();
      }
      tree.insert(voxels[i], dat);
      // Check the entire tree to see if anything doesn't match
      // Then, make sure it isn't duplicated;
      int count = 0;
      for (float x = -tree.getRadius()-res/2; x<tree.getRadius()+res/2; x += res) {
        for (float y = -tree.getRadius()-res/2; y<tree.getRadius()+res/2; y += res) {
          for (float z = -tree.getRadius()-res/2; z<tree.getRadius()+res/2; z += res) {
            int64_t * result = tree.get({x, y, z});
            if (result != NULL && *result == dat) {
              count++;
            }
          }
        }
      }
      if (!checkParents(tree)) {
        return false;
      }
      if (count != 1) {
        return false;
      }
    }
  }
  return true;
}

bool test_iterator() {
  bool success = true;
  const float maxv = 1;
  const std::vector<float> reses = {0.5, 0.1, 0.05, 0.01};
  const int64_t dat = 1337;
  for (auto res : reses) {
    for (int i=0; i<20; i++) {
      auto voxel = randomVecs(1, -maxv, maxv)[0];
      Octree tree(res);
      tree.insert(voxel, dat);
      // printTree(tree);
      bool found = false;
      for (auto it : tree) {
        // for (auto k : it) {
        //   printf("%i,", k);
        // }
        // printf("\n");
        int64_t *val = tree.indexTree(it);
        if (val != NULL) {
          if (*val == dat) {
            if (found) {
              printf("Duplicate found in iterator\n");
              return false;
            }
            found = true;
          }
        } else {
          printf("Got null value from iterator\n");
          return false;
        }
      }
      if (!found) {
        printf("Iterator failed to find value. \n");
        return false;
      }
    }
  }
  return success;
}

// =============================================================
// SIZE TESTS
// =============================================================

bool test_resize() {
  const int max_resizes = 3;
  const std::vector<vec3> voxels = randomVecs(100, -1, 1);
  const std::vector<float> reses = {0.1, 0.05};
  const int64_t dat = 1337;
  bool success = true;

  for (auto res : reses) {
    // Create tree with appropriate size
    for (auto base_voxel : voxels) {
      vec3 voxel = base_voxel*res;
      Octree tree(res);
      tree.insert(voxel, dat);
      for (int i=0; i<max_resizes; i++) {
        tree.incrementDepth();
        // First, check if the voxel is there
        int64_t * result = tree.get(voxel);
        if (result == NULL || *result != dat) {
          printf("Res: %f\n", res);
          printf("Lost\n");
          printf("Original: ");
          vec3print(voxel);
          printf("\n");
          success = false;
        }
        // Then, make sure it isn't duplicated
        int count = 0;
        float maxv = tree.getRadius();
        for (float x = -maxv-res/2; x<maxv+res/2; x += res) {
          for (float y = -maxv-res/2; y<maxv+res/2; y += res) {
            for (float z = -maxv-res/2; z<maxv+res/2; z += res) {
              int64_t * result = tree.get({x, y, z});
              if (result != NULL && *result == dat) {
                if (!success) {
                  printf("Found value at: ");
                  vec3print({x, y, z});
                  printf("\n");
                }
                count++;
              }
            }
          }
        }

        if (count > 1) {
          printf("Duplication\n");
          return false;
        }
        if (!success) {
          printf("Count: %i\n", count);
          return success;
        }
      }
    }
  }
  return true;
}

bool
test_correct_size() {
  const float res = 0.1;
  const std::vector<float> sizes = {res, 2*res, 8*res, 10, 100, 1000, 100000};
  std::vector<vec3> test_points = randomVecs(100, -1, 1);
  test_points.insert(test_points.end(), VOXEL_NUMBERING, VOXEL_NUMBERING+8);

  int64_t dat = 1337;
  for (auto size : sizes) {
    for (auto pt : test_points) {
      Octree tree(res);
      tree.insert(pt*size, dat);
      int64_t * res = tree.get(pt*size);
      if (res == NULL || *res != dat) {
        return false;
      }
    }
  }
  return true;
}

bool
test_resize_insertion() {
  float maxv = 20;
  std::vector<vec3> sources = randomVecs(100, -10, 10);
  std::vector<vec3> voxels = randomVecs(100, -maxv, maxv);
  // Adding edge cases
  const std::vector<vec3> edge_sources = {
    {-9.944485, -7.774033, 3.065767},
    {9.028589, 0.453688, 6.832436},
    {8.016354, 9.650476, 9.019857},
    {-0.805279, 1.663546, 0.107616},
    {-0.744965, -9.307243, 4.565362},
  };
  const std::vector<vec3> edge_voxels = {
    {-9.392727, -7.767507, 3.619267},
    {15.524971, -9.188999, -1.179890},
    {0.011543, 2.334883, 0.019962},
    {8.118879, 9.911964, 8.955095},
    {-8.964530, -8.524374, -10.912760},
  };
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  const std::vector<float> ranges = {std::sqrt(2.0f)+EPS, 1.5, 1.7, 2};

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      for (auto source : sources) {
        auto ray = voxel - source;
        for (size_t i=0; i<ranges.size(); i++) {
          Octree tree(res);
          tree.insert(voxel, dat);
          tree.insert(voxel + ray*ranges[i], dat+1);
          if (tree.get(voxel) == NULL || *tree.get(voxel) != dat) {
            return false;
          }
          if (!checkParents(tree)) {
            return false;
          }
        }
      }
    }
  }
  return true;
}

bool
test_convert_key() {
  const std::vector<vec3> voxels = randomVecs(5, -1, 1);
  const std::vector<float> reses = {0.1, 0.05};
  const int64_t dat = 1337;
  for (auto res : reses) {
    Octree tree(res);
    for (auto base_voxel : voxels) {
      vec3 voxel = base_voxel;
      tree.insert(voxel, dat);
    }
    for (auto okey : tree) {
      auto conv1 = tree.convertKey(okey);
      auto conv = tree.convertKey(conv1);
      if (conv.size() != okey.size()) {
        printf("Convert key is not self inverse; the key length is not the same.\n");
        printf("Original key length: %zu\n", okey.size());
        printf("Converted key length: %zu\n", conv.size());
        return false;
      }
      for (size_t i=0; i<conv.size(); i++) {
        if (conv[i] != okey[i]) {
          printf("Convert key is not self inverse; mismatch at %zu.\n", i);
          printf("Original leaf %i vs. new leaf %i\n", okey[i], conv[i]);
          return false;
        }
      }
    }
  }
  return true;
}

bool test_metadata_association() {
  const std::vector<float> reses { 0.1, 0.2 };
  float maxv = 2;
  for (auto res : reses) {
    MetaOctree tree(res);
    tree.initializeType(POINT_DATA_V1);
    int i = 0;
    for (float x = -maxv-res/2; x<maxv+res/2; x += res) {
      for (float y = -maxv-res/2; y<maxv+res/2; y += res) {
        for (float z = -maxv-res/2; z<maxv+res/2; z += res) {
          vec3 voxel = {x, y, z};
          PointDataV1 dat {.count=0};
          tree.insert(voxel, (void *)&dat);
          PointDataV1 *d = (PointDataV1 *)tree.get(voxel);
          if (d == NULL) {
            printf("Retrieved voxel is null at %f, %f, %f\n", voxel.x, voxel.y, voxel.z);
            printf("Index into metadata: %zu at %p\n", *tree.tree.get(voxel), tree.tree.get(voxel));
            printf("Length of metadata: %zu\n", tree.get_metadata<PointDataV1>().size());
            printTree(tree.tree);
            return false;
          }
          d->count = i;
          i++;
        }
      }
    }
    i = 0;
    for (float x = -maxv-res/2; x<maxv+res/2; x += res) {
      for (float y = -maxv-res/2; y<maxv+res/2; y += res) {
        for (float z = -maxv-res/2; z<maxv+res/2; z += res) {
          vec3 voxel = {x, y, z};
          PointDataV1 *d = (PointDataV1 *)tree.get(voxel);
          d->count = i;
          i++;
        }
      }
    }
    i = 0;
    for (float x = -maxv-res/2; x<maxv+res/2; x += res) {
      for (float y = -maxv-res/2; y<maxv+res/2; y += res) {
        for (float z = -maxv-res/2; z<maxv+res/2; z += res) {
          vec3 voxel = {x, y, z};
          PointDataV1 *d = (PointDataV1 *)tree.get(voxel);
          int64_t *ret = tree.tree.get(voxel);
          OctreeKey key = tree.metamap[*ret];
          if (d == NULL) {
            printf("Metadata is null. Basic tree failure.\n");
            return false;
          }
          if (d->count != i) {
            printf("Metadata is wrong. %i vs %i.\n", d->count, i);
            return false;
          }
          int64_t ind = tree.getNode(key.node_idx)->children[key.child_idx];
          if (ind == NULL_VOX) {
            printf("Octree data key points to null node. %zu: %zu.\n", key.node_idx, key.child_idx);
            return false;
          }
          PointDataV1 *alt_ret = (PointDataV1 *)tree.get(ind);
          if (alt_ret->count != i) {
            printf("Octree data key points to node that points to wrong metadata. %zu: %i\n", ind, alt_ret->count);
            return false;
          }
          i++;
        }
      }
    }
  }
  return true;
}

bool test_ray_intersect_sides() {
  vec3 ray_origin = {0, 3./2, 0};
  vec3 ray = {3./2, 0, 0};
  vec3 vmin1 = {1, 1, 1};
  vec3 vmax1 = {2, 2, 2};
  vec3 vmin2 = {2, 1, 1};
  vec3 vmax2 = {3, 2, 2};
  float rlen1, rlen2;
  rayBoxIntersect(ray_origin, ray, vmin1, vmax1, &rlen1);
  rayBoxIntersect(ray_origin, ray, vmin2, vmax2, &rlen2);
  // printf("Len 1: %f, vs Len 2: %f\n", rlen1, rlen2);
  return rlen1 < rlen2;
}

// =============================================================
// RAYTRACING TESTS
// =============================================================

bool test_ray_intersect() {
  // Basically, if a ray r intersects box b with dist d, then b should contain
  // r*d
  const float v = 0.98;
  const std::vector<vec3> sources = randomVecs(100, -10, 10);
  const std::vector<vec3> rays {
    { v, v, v},
    { v, v,-v},
    { v,-v, v},
    { v,-v,-v},
    {-v, v, v},
    {-v, v,-v},
    {-v,-v,-v},
  };
  const std::vector<vec3> voxels = randomVecs(100, -1, 1);
  const std::vector<float> reses { 0.1, 0.01, 0.05, 0.2 };
  const std::vector<float> ranges { 1, 2, 0.1, 3, 7 };
  // generate test points
  // Run tests
  for (auto source : sources) {
    for (auto ray : rays) {
      for (auto voxel : voxels) {
        for (auto res : reses) {
          for (auto range : ranges) {
            vec3 vmin = range*voxel - res;
            vec3 vmax = range*voxel + res;
            // The individual test
            float dist;
            bool hit = rayBoxIntersect(source, ray, vmin, vmax, &dist);
            if (!hit)
              continue;
            vec3 pt = source + dist * ray;
            if (!((vmin.x-EPS <= pt.x) && (vmax.x+EPS >= pt.x)) ||
                !((vmin.y-EPS <= pt.y) && (vmax.y+EPS >= pt.y)) ||
                !((vmin.z-EPS <= pt.z) && (vmax.z+EPS >= pt.z))) {

              printf("\nFailed: \n");
              printf("Source: ");
              vec3print(source);
              printf("\n");

              printf("Ray: ");
              vec3print(ray);
              printf("\n");

              printf("Vmin: ");
              vec3print(vmin);
              printf("\n");

              printf("Vmax: ");
              vec3print(vmax);
              printf("\n");

              printf("Ray Point: ");
              vec3print(pt);
              printf("\n");

              return false;
            }
          }
        }
      }
    }
  }
  return true;
}

bool test_sorted_list() {
  // Routine:
  // Create a random list
  // loop:
  //   add an element
  //   check if sorted
  //   jump loop
  const int n = 20;
  std::vector<float> data = randomVector<float>(n, 0, 1);
  FLinkedList list[n];
  int head = -1;
  // loop
  for (size_t i = 0; i<data.size(); i++) {
    // add an element
    head = sort_insert(list, head, i, data[i]);
    // check if sorted
    int sel_ind = head;
    for (float j = 0; j<i; j++) {
      FLinkedList & item = list[sel_ind];

      sel_ind = item.child;
      if (list[sel_ind].score < item.score) {
        return false;
      }
    }
  }
  return true;
}

bool test_raytrace_hit() {

  std::vector<vec3> sources = randomVecs(100, -10, 10);
  std::vector<vec3> voxels = randomVecs(100, -20, 20);
  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  // Adding edge cases
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      Octree tree(res);
      tree.insert(voxel, dat);
      if (tree.get(voxel) == NULL || *tree.get(voxel) != dat) {
        printf("Failed initial check.\n");
        return false;
      }
      for (auto source : sources) {
        auto ray = voxel - source;
        float dist;
        int64_t * result = tree.raycast(source, ray, &dist);
        if (result == NULL || dat != *result) {
          printf("Voxel: ");
          vec3print(voxel);
          printf("\n");
          printf("Source: ");
          vec3print(source);
          printf("\n");
          printf("Ray: ");
          vec3print(ray);
          printf("\n");
          printf("Res: %f\n", res);
          // printf("Original: %i vs Found: %i\n", (int)dat, (int)(*result));
          return false;
        }
      }
    }
  }
  return true;
}

bool test_ptkeyconv() {
  std::vector<vec3> voxels = randomVecs(100, -20, 20);
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());
  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      MetaOctree tree(res);
      tree.initializeType(POINT_DATA_V1);
      PointDataV1 data {.count=dat};
      tree.insert(voxel, (void *)&data);
      PointDataV1 *d = (PointDataV1 *)tree.get(voxel);
      int64_t *ret = tree.tree.get(voxel);
      OctreeKey okey = tree.metamap[*ret];
      if (d == NULL) {
        printf("Failed to get voxel that was just inserted.\n");
        return false;
      }
      long ind = tree.getNode(okey.node_idx)->children[okey.child_idx];
      if (ind == NULL_VOX) {
        printf("Ind pointed to is null.\n");
        return false;
      }
      PointDataV1 *alt_d = (PointDataV1 *)tree.get(ind);
      if (alt_d->count != dat) {
        printf("Failed to get metadata for voxel that was just inserted.\n");
        return false;
      }
      auto key = tree.tree.convertKey(okey);
      vec3 center = tree.tree.ptFromKey(key);
      PointDataV1 *d2 = (PointDataV1 *)tree.get(center);
      if (d2 == NULL) {
        printf("Converted key does not retrieve any value.\n");
        printf("Pt from Key: ");
        vec3print(center);
        printf("\nOriginal: ");
        vec3print(voxel);
        printf("\n");
        return false;
      }
      if (d->count != d2->count) {
        printf("Converted key does not retrieve original value.\n");
        return false;
      }
    }
  }
  return true;
}

bool test_raytrace_depth() {

  std::vector<vec3> sources = randomVecs(100, -10, 10);
  std::vector<vec3> voxels = randomVecs(100, -20, 20);
  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};

  // Adding edge cases
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      Octree tree(res);
      tree.insert(voxel, dat);
      for (auto source : sources) {
        auto ray = voxel - source;
        float dist;
        int64_t * result = tree.raycast(source, ray*randomVector<float>(1, 1e-5, 2)[0], &dist);
        if (std::abs(dist-distance(source, voxel)) > res*sqrt(3.0f)) {
          if (result == NULL) {
            printf("Failed to hit first.\n");
            return false;
          }
          printf("Failed to find correct distance\n");
          printf("Dist: %f vs Reported Dist: %f\n", distance(source, voxel), dist);
          printf("Diff: %f\n", std::abs(distance(source, voxel) - dist));
          printf("Voxel: ");
          vec3print(voxel);
          printf("\n");
          printf("Source: ");
          vec3print(source);
          printf("\n");
          printf("Ray: ");
          vec3print(ray);
          printf("\n");
          printf("Origin: ");
          vec3print(tree.origin);
          printf("\n");
          printf("Res: %f\n", res);
          return false;
        }
        // Test if we can hit the voxel using the given dist
        auto coord = ray/glm::length(ray)*(dist+res*1e-2f) + source;
        float eps = res/10;
        bool found = false;
        for (float x=-eps; x<eps+eps/2; x+=eps) {
          for (float y=-eps; y<eps+eps/2; y+=eps) {
            for (float z=-eps; z<eps+eps/2; z+=eps) {
              int64_t * ret_res = tree.get(coord+vec3{x, y, z});
              found |= (ret_res != NULL);
            }
          }
        }
        if (!found) {
          printf("Failed to hit voxel using dist given\n");
          printf("Dist: %f vs Reported Dist: %f\n", distance(source, voxel), dist);
          printf("Diff: %f\n", std::abs(distance(source, voxel) - dist));
          auto k1 = tree.keyFromPt(voxel);
          auto k2 = tree.keyFromPt(coord);
          printf("Original Key: ");
          printKey(k1);
          printf("New Key: ");
          printKey(k2);
          printf("Voxel: ");
          vec3print(voxel);
          printf("\n");
          printf("Pred coord: ");
          vec3print(coord);
          printf("\n");
          printf("Source: ");
          vec3print(source);
          printf("\n");
          printf("Ray: ");
          vec3print(ray);
          printf("\n");
          printf("Origin: ");
          vec3print(tree.origin);
          printf("\n");
          printf("Res: %f\n", res);
          printf("Tried:\n");
          for (float x=-eps; x<eps+eps/2; x+=eps) {
            for (float y=-eps; y<eps+eps/2; y+=eps) {
              for (float z=-eps; z<eps+eps/2; z+=eps) {
                int64_t * ret_res = tree.get(coord+vec3{x, y, z});
                printf("Coord: ");
                vec3print(coord+vec3{x, y, z});
                printf("\nAnd got: %zu\n", (long)ret_res);
              }
            }
          }
          return false;
        }
      }
    }
  }
  return true;
}

bool test_raytrace_ordering() {
  float maxv = 20;
  std::vector<vec3> sources = randomVecs(50, -10, 10);
  std::vector<vec3> voxels = randomVecs(50, -maxv, maxv);
  // Adding edge cases
  sources.insert(sources.end(), edge_sources.begin(), edge_sources.end());
  voxels.insert(voxels.end(), edge_voxels.begin(), edge_voxels.end());

  const std::vector<float> reses = {0.1, 0.05, 0.01, 0.001};
  const std::vector<float> ranges = {std::sqrt(2.0f)+EPS, 1.5, 1.7, 2};

  const int64_t dat = 1;
  for (auto res : reses) {
    for (auto voxel : voxels) {
      for (auto source : sources) {
        auto ray = voxel - source;
        for (size_t i=0; i<ranges.size(); i++) {
          Octree tree(res);
          tree.insert(voxel, dat);
          float dist;
          int64_t * result;
          result = tree.raycast(source, ray, &dist);
          if (result == NULL || *result != dat) {
            printf("Failed initial check\n");
            printf("Result: ");
            if (result == NULL) {
              printf("NULL\n");
            } else {
              printf("%i\n", (int)*result);
            }
            return false;
          }
          tree.insert(voxel + ray*ranges[i], dat+1);
          result = tree.raycast(source, ray, &dist);
          if (tree.get(voxel) == NULL || *tree.get(voxel) != dat) {
            printf("WTF\n");
            return false;
          }
          if (result == NULL || *result != dat) {
            printf("Radius: %f\n", tree.getRadius());
            printf("Res: %f\n", res);
            printf("Result: %i\n", (int)*result);
            printf("Dat: %i\n", (int)dat);
            printf("Close Voxel: ");
            vec3print(voxel);
            printf("\n");
            printf("Far Voxel: ");
            vec3print(voxel + ray*ranges[i]);
            printf("\n");
            printf("Source: ");
            vec3print(source);
            printf("\n");
            printf("Ray: ");
            vec3print(ray);
            printf("\n");
            printf("Reported Dist: %f\n", dist);
            printf("Closer Voxel Dist: %f\n", distance(voxel, source));
            printf("Further Voxel Dist: %f\n", distance(voxel + ray*ranges[i], source));
            return false;
          }
        }
      }
    }
  }
  return true;
}

bool
test_raytrace_ordering_sides() {
  return true;
}

const std::vector<vec3> edge_centers {
  {-0.144001, -0.156051, 0.255726},
};

bool
test_neighborhood() {
  const std::vector<float> reses = {0.1, 0.05, 0.01};
  const float maxv = 1;
  auto voxels = randomVecs(10, -maxv, maxv);
  voxels.insert(voxels.begin(), edge_centers.begin(), edge_centers.end());
  const int64_t v = 1337;
  const std::vector<float> ranges { 3, 4, 5, 7 };
  for (auto voxel : voxels) {
    for (auto res : reses) {
      // first test that neighborhood captures self
      Octree tree(res);
      tree.insert(voxel, 0);
      // printTree(tree);
      auto nb = tree.neighborhood(voxel, res*3);
      // printf("NB Size: %zu\n", nb.size());
      // auto lk = tree.keyFromPt(voxel);
      // printf("Original Key:");
      // printKey(lk);
      for (auto key : nb) {
        // printf("%zu, %zu\n", key.node_idx, key.child_idx);
        // printf("NB Key:");
        // printKey(tree.convertKey(key));
        int64_t *val = tree.get(key);
        if (val == NULL) {
          printf("Neighborhood returns null.\n");
          return false;
        }
        *val = v;
      }
      // printTree(tree);
      if (*tree.get(voxel) != v) {
        printf("Neighborhood does not capture self.\n");
        printf("Center: ");
        vec3print(voxel);
        printf("\n");
        printf("Tree radius: %f\n", tree.getRadius());
        return false;
      }
      // second test that neighborhood captures the voxel as long as center is within range
      for (auto rad : ranges) {
        for (int i=0; i<20; i++) {
          bool found = false;
          vec3 offset = randomVecs(1, -1, 1)[0];
          // printTree(tree);
          for (auto key : tree.neighborhood(voxel+rad*offset*res, 2*rad*res)) {
            int64_t *val = tree.get(key);
            if (val != NULL) {
              found |= (*val == v);
            }
          }
          if (!found) {
            printf("Failed to find value within the neighborhood.\n");
            return false;
          }
        }
      }
    }
  }
  return true;
}
// =============================================================
// OTHER TESTS
// =============================================================

bool
test_saving() {
  int n = 100;
  float res = 0.1;
  const std::vector<float> data = randomVector<float>(n*n*n, -1, 1);

  // First, create a tree
  Octree tree1(res);
  tree1.origin = randomVecs(1, -res/4, res/4)[0];
  for (float x=0; x<n; x++) {
    for (float y=0; y<n; y++) {
      for (float z=0; z<n; z++) {
        int i = x*n*n+y*n+z;
        int64_t dat = int64_t(data[i]*255);
        vec3 v {x*res+res/2, y*res+res/2, z*res+res/2};
        tree1.insert(v, dat);
      }
    }
  }

  tree1.save("test.octree", 2e6);
  Octree tree2("test.octree");
  // Lastly, test that the tree is the same
  if (tree2.scale != tree1.scale ||
      tree2.depth != tree1.depth ||
      tree2.origin != tree1.origin) {
    printf("Failed to fill out parameters\n");
    printf("Scale: %f vs %f\n", tree1.scale, tree2.scale);
    printf("Current Max Depth: %i vs %i\n", tree1.depth, tree2.depth);
    printf("Origin: ");
    vec3print(tree1.origin);
    printf(" vs ");
    vec3print(tree2.origin);
    printf("\n");
    return false;
  }
  if (tree1.numNodes() != tree2.numNodes()) {
    printf("Mismatch in number of nodes in trees.\n");
    printf("%zu vs %zu\n", tree1.numNodes(), tree2.numNodes());
    return false;
  }
  for (size_t i=0; i<tree1.numNodes(); i++) {
    OctreeNode n1 = *tree1.getNode(i);
    OctreeNode n2 = *tree2.getNode(i);
    for (size_t j=0; j<8; j++) {
      if (n1.children[j] != n2.children[j]) {
        printf("Mismatch in node data at %i.\n", (int)i);
        return false;
      }
    }
  }
  return true;
}

bool
test_meta_saving() {
  int n = 100;
  float res = 0.1;
  const std::vector<float> data = randomVector<float>(n*n*n, -1, 1);

  // First, create a tree
  MetaOctree tree1(res, randomVecs(1, -res/4, res/4)[0], octree::POINT_DATA_V1);
  for (float x=0; x<n; x++) {
    for (float y=0; y<n; y++) {
      for (float z=0; z<n; z++) {
        int i = x*n*n+y*n+z;
        int32_t dat = int32_t(data[i]*255);
        vec3 v {x*res+res/2, y*res+res/2, z*res+res/2};
        PointDataV1 data {
            .score = dat,
            .size = 0.0,
            .x = 0.0,
            .y = 0.0,
            .z = 0.0,
            .count = dat,
        };
        tree1.insert(v, (void *) &data);
      }
    }
  }

  tree1.save("test.octree", 2e6);
  MetaOctree tree2("test.octree", 2e6);
  // Lastly, test that the tree is the same
  if (tree2.tree.scale != tree1.tree.scale ||
      tree2.tree.depth != tree1.tree.depth ||
      tree2.tree.origin != tree1.tree.origin ||
      tree2.datatype != tree1.datatype) {
    printf("Failed to fill out parameters\n");
    printf("Scale: %f vs %f\n", tree1.tree.scale, tree2.tree.scale);
    printf("Current Max Depth: %i vs %i\n", tree1.tree.depth, tree2.tree.depth);
    std::cout << "Datatype: " << tree1.datatype << " vs " << tree2.datatype << std::endl;
    printf("Origin: ");
    vec3print(tree1.tree.origin);
    printf(" vs ");
    vec3print(tree2.tree.origin);
    printf("\n");
    return false;
  }
  if (tree1.numNodes() != tree2.numNodes()) {
    printf("Mismatch in number of nodes in trees.\n");
    printf("%zu vs %zu\n", tree1.numNodes(), tree2.numNodes());
    return false;
  }
  for (size_t i=0; i<tree1.numNodes(); i++) {
    OctreeNode n1 = *tree1.getNode(i);
    OctreeNode n2 = *tree2.getNode(i);
    for (size_t j=0; j<8; j++) {
      if (n1.children[j] != n2.children[j]) {
        printf("Mismatch in node data at %i.\n", (int)i);
        return false;
      }
    }
  }
  if (tree1.numLeafs() != tree2.numLeafs()) {
    printf("Mismatch in number of leafs in trees.\n");
    printf("%zu vs %zu\n", tree1.numLeafs(), tree2.numLeafs());
    return false;
  }
  for (size_t i=0; i<tree1.numLeafs(); i++) {
    OctreeKey k1 = tree1.metamap[i];
    OctreeKey k2 = tree2.metamap[i];
    if (k1.child_idx != k2.child_idx || k1.node_idx != k2.node_idx) {
      printf("Mismatch in octree metamap at %zu. CI: %zu vs %zu. Node Idx: %zu vs %zu\n", i, k1.child_idx, k2.child_idx, k1.node_idx, k2.node_idx);
      return false;
    }

    PointDataV1 *d1 = (PointDataV1 *)tree1.get(i);
    PointDataV1 *d2 = (PointDataV1 *)tree2.get(i);
    if (d1->score != d2->score || d1->count != d2->count)
    {
      printf("Mismatch in octree leaf data at %zu. %f vs %f, %i vs %i\n", i, d1->score, d2->score, d1->count, d2->count);
      return false;
    }
  }
  for (float x=0; x<n; x++) {
    for (float y=0; y<n; y++) {
      for (float z=0; z<n; z++) {
        vec3 v {x*res+res/2, y*res+res/2, z*res+res/2};
        PointDataV1 *d1 = (PointDataV1 *)tree1.get(v);
        PointDataV1 *d2 = (PointDataV1 *)tree2.get(v);
        if (d1->score != d2->score || d1->count != d2->count)
        {
          int i = x*n*n+y*n+z;
          int32_t dat = int32_t(data[i]*255);
          printf("Mismatch in octree data at (%f, %f, %f).\n", v.x, v.y, v.z);
          printf("Data1: %f, %i\n", d1->score, d1->count);
          printf("Data2: %f, %i\n", d2->score, d2->count);
          printf("Supposed to be: %i\n", dat);
          return false;
        }
      }
    }
  }
  return true;
}

bool
test_meta_initialize() {
  int n = 100;
  float res = 0.1;
  const std::vector<float> data = randomVector<float>(n*n*n, -1, 1);

  // First, create a tree
  MetaOctree tree1(res, randomVecs(1, -res/4, res/4)[0]);
  for (float x=0; x<n; x++) {
    for (float y=0; y<n; y++) {
      for (float z=0; z<n; z++) {
        int i = x*n*n+y*n+z;
        int32_t dat = int32_t(data[i]*255);
        vec3 v {x*res+res/2, y*res+res/2, z*res+res/2};
        tree1.insert(v, NULL);
      }
    }
  }

  tree1.save("test.octree", 2e6);
  MetaOctree tree2("test.octree", 2e6);
  tree2.initializeType(octree::POINT_DATA_V1);
  for (float x=0; x<n; x++) {
    for (float y=0; y<n; y++) {
      for (float z=0; z<n; z++) {
        int i = x*n*n+y*n+z;
        int32_t dat = int32_t(data[i]*255);
        vec3 v {x*res+res/2, y*res+res/2, z*res+res/2};
        PointDataV1 *res = (PointDataV1 *)tree2.get(v);
        if (res == NULL) {
          printf("Could not retrieve voxel where it should exist.\n");
          return false;
        }
        res->count = 2;
      }
    }
  }
  return true;
}


int main() {
  // Ordered in such a way that the first failure should be checked before progressing
  // Insertion Retrieval Tests
  /*
  printf("Voxel Coordinate Test: %s\n",                       PASS_OR_FAIL(test_voxel_coords()));
  printf("Insertion Retrieval Occupancy Test: %s\n",          PASS_OR_FAIL(test_insertion_retrieval_occupancy()));
  printf("Insertion Retrieval Value Test: %s\n",              PASS_OR_FAIL(test_insertion_retrieval_value()));
  printf("Insertion Retrieval Uniquenesss Test: %s\n",        PASS_OR_FAIL(test_insertion_retrieval_uniqueness()));
  printf("Value Change Test: %s\n",                           PASS_OR_FAIL(test_change_value()));

  // Resize
  printf("Resize Test: %s\n",                                 PASS_OR_FAIL(test_resize()));
  printf("Resize During Insertion Test: %s\n",                PASS_OR_FAIL(test_resize_insertion()));
  printf("Correct Size Test: %s\n",                           PASS_OR_FAIL(test_correct_size()));

  // Ray tracing
  printf("Ray Intersection Test: %s\n",                       PASS_OR_FAIL(test_ray_intersect()));
  printf("Ray Intersection Sides Test: %s\n",                 PASS_OR_FAIL(test_ray_intersect_sides()));
  printf("Linked List Sorting Test: %s\n",                    PASS_OR_FAIL(test_sorted_list()));
  printf("Raytrace Hit Test: %s\n",                           PASS_OR_FAIL(test_raytrace_hit()));
  printf("Iterator Test: %s\n",                               PASS_OR_FAIL(test_iterator()));
  printf("Neighborhood Test: %s\n",                           PASS_OR_FAIL(test_neighborhood()));
  printf("Raytrace Depth Test: %s\n",                         PASS_OR_FAIL(test_raytrace_depth()));
  printf("Raytrace Ordering Test: %s\n",                      PASS_OR_FAIL(test_raytrace_ordering()));

  printf("Metadata Association Test: %s\n",                   PASS_OR_FAIL(test_metadata_association()));
  printf("Convert Key Test: %s\n",                            PASS_OR_FAIL(test_convert_key()));
  printf("Metadata Point Conversion Test: %s\n",              PASS_OR_FAIL(test_ptkeyconv()));

  // Other
  printf("Save Test: %s\n",                                   PASS_OR_FAIL(test_saving()));
  */
  printf("MetaOctree Save Test: %s\n",                        PASS_OR_FAIL(test_meta_saving()));
  printf("MetaOctree Initialize Test: %s\n",                  PASS_OR_FAIL(test_meta_initialize()));

  return 0;
}
