// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "octree.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>
#include <iostream>
#include <fstream>

using namespace octree;

int main(int argc, char** argv) {
  // Octree octree(res);
  // Octree octree("/data/unrealcv/octomaps/RealisticRendering.oct");
  if (argc < 2) {
    printf("Usage: vis path_to_octomap\n");
  }
  std::string path = argv[1];
  std::cout << "Loading octree from: " << path << std::endl;
  Octree octree (path);
  // Draw tree
  int im_rows = 512;
  int im_cols = 512;
  // int im_rows = 100;
  // int im_cols = 100;
  float calib[4] = {im_cols/2.0f, im_rows/2.0f, im_cols/3.0f, im_rows/3.0f};

  std::cout << "Creating tree" << std::endl;

  // Print Tree
  // for (size_t i = 0; i < octree.data.size(); ++i) {
  //   OctreeNode & node = octree.data[i];
  //   printf("%i: ", (int)i);
  //   for (size_t j = 0; j < 8; ++j) {
  //     printf("%i, ", (int)node.children[j]);
  //   }
  //   printf("\n");
  // }

  // Save to shitty binary
  // std::ofstream ofile ("octree.csv", std::ios::out | std::ios::trunc);
  // for (float x=-1; x<1; x+=p1) {
  //   for (float y=-1; y<1; y+=p1) {
  //     for (float z=-1; z<1; z+=p1) {
  //       int64_t * val = octree.get({x, y, z});
  //       if (val != NULL) {
  //         // printf("%f, %f, %f, %i\n", x, y, z, (int)val);
  //         ofile << "1,";
  //       } else {
  //         ofile << "0,";
  //       }
  //     }
  //   }
  //   ofile << "\n";
  // }
  // ofile.close();

  printf("Max depth: %i\n", octree.current_max_depth);
  std::cout << "Ray tracing" << std::endl;
  cv::Mat dat = cv::Mat::zeros(im_rows, im_cols, CV_8UC3);
  cv::Mat dep = cv::Mat::zeros(im_rows, im_cols, CV_32FC1);
  vec3 origin {-1.88895490e+02, -1.57342680e+02, -3.13097200e+0};
  // vec3 origin {-2.0f, 0.0f, -28.9f};
  float dist;
  int64_t * hit = octree.raycast(origin, vec3(0, 0, -1), &dist);
  printf("Hit: %i, dist: %f\n", hit, dist);
  for (int i=0; i < im_rows; i++) {
    for (int j=0; j < im_cols; j++) {
      // Ray trace value for each pixel
      vec3 ray{float(j - calib[2]) / calib[0],
                float(i - calib[3]) / calib[1],
                1.0f};
      float dist;
      int64_t * hit = octree.raycast(origin, ray, &dist);

      if (hit == NULL)
        continue;

      int64_t r = *hit & 255;
      int64_t g = (*hit >> 3) & 255;
      int64_t b = (*hit >> 6) & 255;
      dat.at<cv::Vec3b>(i, j) = cv::Vec3b(r, g, b);
      std::cout << dist << std::endl;
      dep.at<float>(i, j) = dist/8;
    }
  }
  // cv::normalize(dep, dep, 1, 0);
  cv::imshow("col", dat);
  cv::imshow("depth", dep);
  cv::waitKey(0);
  return 0;
}
