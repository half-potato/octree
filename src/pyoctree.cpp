#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>

#include "octree/metaoctree.h"
#include "octree/pyoctree.h"
#include "octree/pyoctree_cuda_reg.h"


int64_t
generate_octree(
    std::vector<torch::Tensor> depth_maps,
    const std::vector<torch::Tensor> pose_tensors,
    const std::string & outpath,
    const std::vector<float> & calib,
    const float & res)
{

  /*!
   * A variation of the SuperPoint algorithm applied to bounding boxes
   *
   * the depth_maps must be prescaled by their depth factor and type float
   * pose_paths: list of paths to poses for each image in the batch
   * calib: [fx, fy, cx, cy]
   **/

  // ----------------------------- LOADING ---------------------------------- //

  const int batch_size = depth_maps.size();
  const int im_rows = depth_maps[0].size(0);
  const int im_cols = depth_maps[0].size(1);

  // -------------------------- DONE LOADING -------------------------------- //

  // Compile a map using all viewpoints
  octree::MetaOctree tree (res, {0, 0, 0});

  int i = 0;
#pragma omp parallel for
  for(int b=0; b<batch_size; b++) {
#pragma omp critical
    {
      int bar_width = 70;
      std::cout << "[";
      float progress = float(i)/batch_size;
      int pos = bar_width*progress;
      for (int b=0; b<bar_width; b++) {
        if (b<pos) std::cout << "=";
        else if (b==pos) std::cout << ">";
        else std::cout << " ";
      }
      std::cout << "] " << int(progress*100.0) << " %\r";
      std::cout.flush();
    }

#pragma omp atomic update
    i++;

    for (int i = 0; i < im_rows; i++) {
      for (int j = 0; j < im_cols; j++) {
        // float dep = float(depth.at<ushort>(i, j)) / factor;
        float dep = depth_maps[b][i][j].item<float>();
        if (!(dep > 0.02f && dep < 10.0f))
          continue;
        // SUBPIXEL_TAG
        for (float sx : {-0.5, 0.0, 0.5, 1.0, 1.5})
        {
          for (float sy : {-0.5, 0.0, 0.5, 1.0, 1.5})
          {
            // Need to insert a voxel at each subpixel coordinate to make sure that all rays land on the map
            torch::Tensor pt = torch::tensor({
              float(j - calib[2] + sx) * dep / calib[0],
              float(i - calib[3] + sy) * dep / calib[1],
              dep,
              1.0f,
            });

            pt = torch::matmul(pose_tensors[b], pt.reshape({4, 1})).reshape({-1});
#pragma omp critical
            {
              tree.insert({pt[0].item<float>(), pt[1].item<float>(), pt[2].item<float>()}, NULL);
            }
          }
        }
      }
    }
  }
  tree.save(outpath);
  return 0;
}


PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  m.def("generate_octree", &generate_octree);
#ifdef BUILD_CUDA
  init_cuda_module(m);
#endif

  // Struct exports
  // py::class_<octree::Data>(m, "Data")
  //   .def_readwrite("score", &octree::Data::score)
  //   .def_readwrite("size", &octree::Data::size)
  //   .def_readwrite("count", &octree::Data::count);
  // py::class_<octree::OctreeKey>(m, "OctreeKey")
  //   .def_readwrite("node_idx", &octree::OctreeKey::node_idx)
  //   .def_readwrite("child_idx", &octree::OctreeKey::child_idx);
  // py::class_<octree::OctreeData>(m, "OctreeData")
  //   .def_readwrite("key", &octree::OctreeData::key)
  //   .def_readwrite("data", &octree::OctreeData::data);
  // py::class_<glm::vec3>(m, "vec3")
  //   .def_readwrite("x", &glm::vec3::x);

  // py::class_<octree::MetaOctree>(m, "MetaOctree")
  //   .def(py::init<float, float, float, float>(),
  //       py::arg("scale"),
  //       py::arg("x") = 0,
  //       py::arg("y") = 0,
  //       py::arg("z") = 0
  //       )
  //   .def("getRadius", &octree::MetaOctree::getRadius)
  //   .def("getScale", &octree::MetaOctree::getScale)
  //   .def("numNodes", &octree::MetaOctree::numNodes)
  //   .def("numLeafs", &octree::MetaOctree::numLeafs)
  //   .def("save", &octree::MetaOctree::save);
}

