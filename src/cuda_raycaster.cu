#include "octree/cuda_raycaster.h"
#include "octree/kernels.h"

void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line) {
  if (result) {
    std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
        file << ":" << line << " '" << func << "' \n";
    // Make sure we call CUDA Device Reset before exiting
    cudaDeviceReset();
    exit(99);
  }
}

namespace octree {

void init_point_module(py::module_ &m) {
  py::class_<CudaRaycaster>(m, "CudaRaycaster")
    .def(py::init<std::string &, std::vector<float> &, int, int, int>(), py::arg("octree_path"), py::arg("calib"), py::arg("h"), py::arg("w"), py::arg("cell_size"))
    .def("project_features", &CudaRaycaster::projectFeaturesTh, py::arg("pose"), py::arg("depth"), py::arg("feature_map"), py::arg("threshold"), py::arg("min_seen_size"), py::arg("max_unseen_size"), py::arg("cell_size"), pybind11::return_value_policy::take_ownership)
    .def("extract_points", &CudaRaycaster::extract_points, py::arg("threshold"), py::arg("max_num_points"), pybind11::return_value_policy::take_ownership)
    .def("renderScoreCount", &CudaRaycaster::renderScoreCountTh, py::arg("pose"), py::arg("depth"), py::arg("n"), pybind11::return_value_policy::take_ownership)
    .def("convolve3d", &CudaRaycaster::convolve3dTh, py::arg("kernel"), py::arg("kernel_res"))
    .def("run_nms", &CudaRaycaster::runNMS, py::arg("min_nms_radius"), py::arg("max_nms_radius"), py::arg("size_radius_multi"), py::arg("iou_thres"), py::arg("allowed_count_ratio"), pybind11::return_value_policy::take_ownership)
    .def("run_sparsity_enforcer", &CudaRaycaster::runSparsityEnforcer, py::arg("neighborhood_radius"), py::arg("max_features_in_neighborhood"), pybind11::return_value_policy::take_ownership);
}

void
CudaRaycaster::init(octree::MetaOctree tree, CameraConfig & cfg, size_t cell_size) {
  octree = tree;
  assert(octree.getDepth() <= MAX_DEPTH);
  // METADATA_TAG
  octree.initializeType(MetaOctreeType::POINT_DATA_V2);
  tx = 16;
  ty = 16;
  size_t size = octree.numNodes()*sizeof(octree::OctreeNode);
  printf("Tree size: %zu\n", size);
  this->cfg = cfg;

  OctreeConfig h_cfg = {
    .max_depth=octree.getDepth(), 
    .num_leafs=octree.numLeafs(),
    .num_nodes=octree.numNodes(),
    .res = octree.getScale(),
    .origin = {octree.getOrigin().x, octree.getOrigin().y, octree.getOrigin().z}
  };

  state_width = tx;
  // This block of code causes some weird memory leak that prevents accumulating gradients
  // checkCudaErrors(cudaMalloc(&states, tx*ty*sizeof(curandState_t)));
  // dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  // dim3 block_size (tx, ty);
  // if (num_blocks.x != 0 && num_blocks.y != 0) {
  //   init_randoms<<<num_blocks, block_size>>>(time(0), states, state_width);
  //   checkCudaErrors(cudaGetLastError());
  // } else {
  //   printf("Unable to initialize random states because the desired block size is 0\n");
  // }

  printf("Metadata Size: %zu\n", octree.metadata_size());
  checkCudaErrors(cudaMalloc(&doctree_data, size));
  checkCudaErrors(cudaMalloc(&dmetadata, octree.metadata_size()));
  checkCudaErrors(cudaMalloc(&dmetamap, octree.numLeafs()*sizeof(OctreeKey)));
  checkCudaErrors(cudaMalloc(&dtree_cfg, sizeof(OctreeConfig)));
  checkCudaErrors(cudaMalloc(&dcfg, sizeof(CameraConfig)));

  // IO Buffers
  checkCudaErrors(cudaMalloc(&ddepth, cfg.im_cols*cfg.im_rows*cell_size*cell_size*sizeof(float)));
  checkCudaErrors(cudaMalloc(&dcount, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&dscore, cfg.im_cols*cfg.im_rows*sizeof(float)));

  checkCudaErrors(cudaMalloc(&dsubx, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&dsuby, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&dsize, cfg.im_cols*cfg.im_rows*sizeof(float)));

  checkCudaErrors(cudaMemcpy(dtree_cfg, &h_cfg, sizeof(OctreeConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dmetadata, octree.metadata(), octree.metadata_size(), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dmetamap, octree.metamap.data(), octree.numLeafs()*sizeof(OctreeKey), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(doctree_data, octree.tree.data.data(), size, cudaMemcpyHostToDevice));
}

CudaRaycaster::~CudaRaycaster() {
  // Random states
  // checkCudaErrors(cudaFree(states));
  // Octree stuff
  checkCudaErrors(cudaFree(doctree_data));
  checkCudaErrors(cudaFree(dmetadata));
  checkCudaErrors(cudaFree(dmetamap));
  checkCudaErrors(cudaFree(dtree_cfg));
  checkCudaErrors(cudaFree(dcfg));

  checkCudaErrors(cudaFree(ddepth));
  checkCudaErrors(cudaFree(dscore));
  checkCudaErrors(cudaFree(dcount));

  checkCudaErrors(cudaFree(dsubx));
  checkCudaErrors(cudaFree(dsuby));
  checkCudaErrors(cudaFree(dsize));
}

void CudaRaycaster::renderDepth(CameraConfig & cfg, float *image)
{
  // asdf
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);
  checkCudaErrors(cudaMemset(ddepth, 0, im_size));
  depth_projection_kernel<<<num_blocks, block_size>>>(
    dcfg, dtree_cfg,
    doctree_data,
    ddepth,
    states, state_width);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());
  checkCudaErrors(cudaMemcpy(image, ddepth, cfg.im_cols*cfg.im_rows*sizeof(float), cudaMemcpyDeviceToHost));
}

void CudaRaycaster::renderScoreCount(CameraConfig & cfg, float *depth, float *score, float *count, int n)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 block_size (tx, ty);
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);
  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(ddepth, depth, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemset(dscore, 0, im_size));
  checkCudaErrors(cudaMemset(dcount, 0, im_size));
  for (int i=0; i<n; i++) {
    point_kernels::score_count_projection_kernel<<<num_blocks, block_size>>>(
      dcfg, dtree_cfg,
      doctree_data, dmetamap, dmetadata,
      ddepth,
      dscore, dcount,
      states, state_width);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }
  checkCudaErrors(cudaMemcpy(score, dscore, im_size, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(count, dcount, im_size, cudaMemcpyDeviceToHost));
}

std::vector<torch::Tensor>
CudaRaycaster::renderScoreCountTh(
    const torch::Tensor &pose,
    const torch::Tensor &depth,
    const int n)
{
  memcpy(cfg.transform_matrix, pose.flatten().contiguous().data_ptr<float>(), 16*sizeof(float));

  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  torch::Tensor count = torch::zeros({cfg.im_rows, cfg.im_cols}, options);;
  torch::Tensor score = torch::zeros({cfg.im_rows, cfg.im_cols}, options);;

  renderScoreCount(
      cfg, 
      depth.flatten().contiguous().data_ptr<float>(),
      score.flatten().contiguous().data_ptr<float>(),
      count.flatten().contiguous().data_ptr<float>(),
      n);
  return {score, count};
}

void CudaRaycaster::projectScore(
    const CameraConfig & cfg, const float *depth,
    const float *score, const float *subx, const float *suby, const float *size,
    const float threshold, const float min_seen_size, const float max_unseen_size, const float cell_size)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 num_blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  assert(num_blocks.x != 0);
  assert(num_blocks.y != 0);
  dim3 block_size (tx, ty);
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);

  checkCudaErrors(cudaMemcpy(dcfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(ddepth, depth, im_size*cell_size*cell_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dscore, score, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dsubx, subx, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dsuby, suby, im_size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dsize, size, im_size, cudaMemcpyHostToDevice));
  point_kernels::map_projection_kernel<<<num_blocks, block_size>>>(
    dcfg, dtree_cfg,
    doctree_data, dmetamap, dmetadata, // output
    ddepth, dscore, dsubx, dsuby, dsize, // input
    states, state_width, threshold, min_seen_size, max_unseen_size, cell_size);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());
}

void CudaRaycaster::projectFeaturesTh(
    const torch::Tensor &pose, const torch::Tensor &depth, const torch::Tensor &feature_map,
    const float threshold,
    const float min_seen_size,
    const float max_unseen_size,
    const float cell_size)
{
  memcpy(cfg.transform_matrix, pose.flatten().contiguous().data_ptr<float>(), 16*sizeof(float));

  projectScore(
      cfg, 
      depth.flatten().contiguous().data_ptr<float>(),
      feature_map[0].flatten().contiguous().data_ptr<float>(),
      feature_map[1].flatten().contiguous().data_ptr<float>(),
      feature_map[2].flatten().contiguous().data_ptr<float>(),
      feature_map[3].flatten().contiguous().data_ptr<float>(),
      threshold, min_seen_size, max_unseen_size, cell_size);
}

void CudaRaycaster::convolve3dTh(
    const torch::Tensor kernel,
    float kernel_res)
{
  assert(kernel.size(0) == kernel.size(1) && kernel.size(1) == kernel.size(2));
  convolve3d(kernel.flatten().contiguous().data_ptr<float>(), kernel.size(0), kernel_res);
}

void CudaRaycaster::convolve3d(
    float * kernel,
    size_t kernel_dim,
    float kernel_res)
{
  // determine block size for GPU to prevent OOM
  int block_size = 128;
  int max_grid_size = 1024;
  int total_num_blocks = int(float(octree.numLeafs()) / block_size+1);
  float *dkernel;
  int kernel_dim3 = kernel_dim*kernel_dim*kernel_dim;
  OctreeData *dnew_metadata;

  checkCudaErrors(cudaMalloc(&dkernel, kernel_dim3*sizeof(float)));
  checkCudaErrors(cudaMemcpy(dkernel, kernel, kernel_dim3*sizeof(float), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMalloc(&dnew_metadata, octree.metadata_size()));
  // checkCudaErrors(cudaMemcpy(dnew_metadata, dmetadata, metadata_size, cudaMemcpyDeviceToDevice));
  checkCudaErrors(cudaMemset(dnew_metadata, 0, octree.metadata_size()));

  for (int i=0; i < total_num_blocks; i += max_grid_size) {
    int num_blocks = fmin(total_num_blocks-i, max_grid_size);
    printf("Num blocks: %i\n", num_blocks);
    point_kernels::convolve3d<<<num_blocks, block_size>>>(
        dtree_cfg,
        doctree_data,
        dmetamap,
        dmetadata,
        dnew_metadata,
        dkernel,
        kernel_dim,
        kernel_res,
        i*block_size
    );
  }
  checkCudaErrors(cudaMemcpy(dmetadata, dnew_metadata, octree.metadata_size(), cudaMemcpyDeviceToDevice));
  checkCudaErrors(cudaFree(dkernel));
  checkCudaErrors(cudaFree(dnew_metadata));
}

torch::Tensor CudaRaycaster::extract_points(float threshold, int max_num_points)
{
  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  // Transfer
  OctreeData *metadata = (OctreeData *)malloc(octree.numLeafs()*sizeof(OctreeData));
  checkCudaErrors(cudaMemcpy(metadata, dmetadata, octree.numLeafs()*sizeof(OctreeData), cudaMemcpyDeviceToHost));
  // Count points
  std::vector<float> scores;
  scores.reserve(octree.numLeafs());
  size_t count = 0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    OctreeData &d = metadata[i];
    if (d.score / d.count < threshold || d.val == 0 || d.count < 2)
      continue;
    count++;
    scores.push_back(d.score / d.count);
  }

  if (max_num_points > 0 && scores.size() > max_num_points) {
    std::sort(scores.begin(), scores.end());
    threshold = scores[scores.size() - max_num_points - 1];
    count = max_num_points;
  }

  // Now, convert the selected points to a list of points
  torch::Tensor targets = torch::zeros({(long)count, 6}, options);

  size_t j = 0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    OctreeData &d = metadata[i];
    if (d.score / d.count < threshold || d.val == 0 || d.count < 2)
      continue;

    targets[j][0] = d.x / d.val;
    targets[j][1] = d.y / d.val;
    targets[j][2] = d.z / d.val;
    targets[j][3] = d.score / d.count;
    targets[j][4] = d.size / d.val;
    targets[j][5] = d.count;

    j++;
    if (j >= count) break;
  }
  free(metadata);

  printf("Extracted %zu points out of %zu\n", count, octree.numLeafs());
  return targets;
}

torch::Tensor CudaRaycaster::runNMS(
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio)
{
  // determine block size for GPU to prevent OOM
  int block_size = 128;
  int max_grid_size = 1024;
  int total_num_blocks = int(float(octree.numLeafs()) / block_size+1);

  // Initialize selection
  size_t sel_size = octree.numLeafs()*sizeof(bool);
  bool *dselection;
  bool *selection = (bool*)malloc(sel_size);;

  checkCudaErrors(cudaMalloc(&dselection, sel_size));
  checkCudaErrors(cudaMemset(dselection, true, sel_size));

  checkCudaErrors(cudaGetLastError());
  for (int i=0; i < total_num_blocks; i += max_grid_size) {
    int num_blocks = fmin(total_num_blocks-i, max_grid_size);
    printf("Num blocks: %i\n", num_blocks);
    point_kernels::nms_3d<<<num_blocks, block_size>>>(
        dtree_cfg, doctree_data,
        dmetamap, dmetadata,
        dselection, min_nms_radius, max_nms_radius, size_radius_multi, iou_thres, allowed_count_ratio,
        i*block_size);

    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }

  // Transfer
  checkCudaErrors(cudaMemcpy(selection, dselection, sel_size, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaFree(dselection));

  OctreeData *metadata = (OctreeData *)malloc(octree.numLeafs()*sizeof(OctreeData));
  checkCudaErrors(cudaMemcpy(metadata, dmetadata, 
        octree.numLeafs()*sizeof(OctreeData), cudaMemcpyDeviceToHost));

  // Now, convert the selected points to a list of points
  size_t count = 0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    count += selection[i];
  }
  printf("Extracted %zu points out of %zu\n", count, octree.numLeafs());

  // extract to torch tensor
  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  torch::Tensor targets = torch::zeros({(long)count, 6}, options);
  size_t j=0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    if (!selection[i])
      continue;

    OctreeData &d = metadata[i];
    targets[j][0] = d.x / d.val;
    targets[j][1] = d.y / d.val;
    targets[j][2] = d.z / d.val;
    targets[j][3] = d.score / d.count;
    targets[j][4] = d.size / d.val;
    targets[j][5] = d.count;

    j++;
  }

  free(selection);
  free(metadata);
  return targets;
}

torch::Tensor CudaRaycaster::runSparsityEnforcer(
    float neighborhood_radius,
    int max_features_in_neighborhood)
{
  // determine block size for GPU to prevent CUDA ERROR 9
  int block_size = 128;
  int max_grid_size = 1024;
  int total_num_blocks = int(float(octree.numLeafs()) / block_size+1);

  // Initialize selection
  size_t sel_size = octree.numLeafs()*sizeof(bool);
  bool *dselection;
  bool *selection = (bool*)malloc(sel_size);

  checkCudaErrors(cudaMalloc(&dselection, sel_size));
  checkCudaErrors(cudaMemset(dselection, true, sel_size));

  for (int i=0; i < total_num_blocks; i += max_grid_size) {
    int num_blocks = fmin(total_num_blocks-i, max_grid_size);
    printf("Num blocks: %i, Block size: %i\n", num_blocks, block_size);
    point_kernels::sparsity_enforcer<<<num_blocks, block_size>>>(
        dtree_cfg, doctree_data, dmetamap, dmetadata,
        dselection, neighborhood_radius, max_features_in_neighborhood,
        i*block_size);

    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }

  // Transfer
  checkCudaErrors(cudaMemcpy(selection, dselection, sel_size, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaFree(dselection));

  OctreeData *metadata = (OctreeData *)malloc(octree.numLeafs()*sizeof(OctreeData));
  checkCudaErrors(cudaMemcpy(metadata, dmetadata, 
        octree.numLeafs()*sizeof(OctreeData), cudaMemcpyDeviceToHost));

  // Now, convert the selected points to a list of points
  size_t count = 0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    count += selection[i];
  }
  printf("Extracted %zu points out of %zu\n", count, octree.numLeafs());

  torch::Device cpu("cpu");
  auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
  torch::Tensor targets = torch::zeros({(long)count, 6}, options);
  size_t j = 0;
  for (size_t i=0; i<octree.numLeafs(); i++) {
    if (!selection[i])
      continue;

    OctreeData &d = metadata[i];
    targets[j][0] = d.x;
    targets[j][1] = d.y;
    targets[j][2] = d.z;
    targets[j][3] = d.score;
    targets[j][4] = d.size;
    targets[j][5] = d.count;

    j++;
  }

  free(selection);
  free(metadata);
  return targets;
}
};
