#include "octree/kernels.h"
#include "octree/seg_kernels.h"
#include "octree/raycast.cu"

using namespace octree;

__global__ void
init_randoms(
    unsigned int seed,
    curandState_t*states,
    int state_width)
{
  int rand_ind = threadIdx.x + threadIdx.y *state_width;
  curand_init(seed, rand_ind, 0, &states[rand_ind]);
}

__global__ void
depth_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    float *depth,
    curandState_t*states,
    int state_width)
{
  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;
  float dist = 0;
  float3 pos, ray;
  int pixel_index = i*cfg->im_cols + j;
  cudatree::calcRayPos(cfg, i, j, &pos, &ray);
  float dist2plane = norm(ray);
  long hit = cudatree::raycast(dtree_cfg, pos, ray, &dist, doctree_data);

  // similar triangles to get planar depth, then add margin so depth lands in voxel
  if (hit != cudatree::NULL_VOX) {
    long ind = doctree_data[hit];
    if (ind == cudatree::NULL_VOX) {
      return;
    }
    // It tends to be offset by this much, but that's the end user's job
    // depth[pixel_index] = dist/dist2plane + dtree_cfg->res*2;
    depth[pixel_index] = dist/dist2plane;
  }
}

namespace point_kernels {

__global__ void
map_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    float *depth,
    float *score, float *subx, float *suby, float *size,
    curandState_t*states,
    int state_width,
    float threshold,
    float min_seen_size,
    float max_unseen_size,
    int cell_size)
{
  int image_i = threadIdx.x + blockIdx.x *blockDim.x;
  int image_j = threadIdx.y + blockIdx.y *blockDim.y;
  float dist = 0;
  float3 pos, ray;
  int pixel_index = image_i*cfg->im_cols + image_j;
  int rand_ind = threadIdx.x + threadIdx.y *state_width;
  // SUBPIXEL_TAG CELLSIZE_TAG
  float si = image_i*cell_size+suby[pixel_index];
  float sj = image_j*cell_size+subx[pixel_index];

  // depth pixel. Clamp to prevent oob
  float di = fmax(fmin(si, cfg->im_rows*cell_size-1), 0);
  float dj = fmax(fmin(sj, cfg->im_cols*cell_size-1), 0);

  int dep_ind = int(int(di)*cfg->im_cols*cell_size+int(dj));
  float dep = depth[dep_ind];
  if (dep < 1e-5 || score[pixel_index] == 0) {
    return;
  }

  cudatree::calcRayPos(cfg, di, dj, &pos, &ray);
  float3 loc = pos + dep*ray;
  float dist_dep = norm(ray)*dep;

  // Hit is the index of the node
  // The benefit of this one is that it is more consistent about where the surface of the
  // octree is. Essentially a bit of a crutch for error.
  long hit = cudatree::raycast(dtree_cfg, pos, ray, &dist, doctree_data);
  if (hit == cudatree::NULL_VOX) {
    return;
  }
  float dist2plane = dist_dep/dep;
  float pred_dep = dist/dist2plane;
  long ind_cast = doctree_data[hit];
  // This is faster and actually works better at steep angles where tiny amounts of pixel error
  // can result in massive changes in distance
  long ind_dep = cudatree::get(dtree_cfg, doctree_data, loc);

  // Decide which ind to use
  // long ind = ind_dep;
  long ind = ind_cast;
  /*
  if (ind_cast == cudatree::NULL_VOX) {
    ind = ind_dep;
  } else if ((ind_dep == cudatree::NULL_VOX) || (dist_dep > (dist + 0.3)) ){
    // trust the dist from the ray. The ray will always hit the closer object because of the growth of the octree
    ind = ind_cast;

    // Necessary because a corner lies between two vastly different errors
    int corner_correction_radius = 1;
    float err = abs(pred_dep-dep);
    float dep = pred_dep;
    for (int oi=-corner_correction_radius; oi<=corner_correction_radius; oi++) {
      for (int oj=-corner_correction_radius; oj<=corner_correction_radius; oj++) {
        int i = int(floor(di+oi));
        int j = int(floor(dj+oj));
        if (i < 0 || j < 0 || i >= cfg->im_rows*cell_size || j >= cfg->im_cols*cell_size) continue;
        size_t dep_ind = int(i*cfg->im_rows*cell_size+j);
        float cand_dep = depth[dep_ind];
        float new_err = abs(pred_dep - cand_dep);
        if (new_err < err) {
          err = new_err;
          dep = cand_dep;
        }
      }
    }
    if (err > 0.1) return;
    loc = pos + dep*ray;
    ind = cudatree::get(dtree_cfg, doctree_data, loc);

  } else {
    ind = ind_dep;
  }
  */


  if (ind == cudatree::NULL_VOX) {
    return;
  }
  OctreeData &d = dmetadata[ind];

  // Now, convert the key to the center location of the voxel
  uint8_t key[cudatree::MAX_DEPTH];
  if (0 != cudatree::convertKey(dtree_cfg, doctree_data, dmetamap[ind], key)) {
    // Convert key failed
    return;
  }

  float3 center = cudatree::ptFromKey(dtree_cfg, key);
  float3 subpix_center = {d.x/d.val, d.y/d.val, d.z/d.val};

  // if (!(d.count > 0 && distance(subpix_center, loc) < dtree_cfg->res) &&
  //      (d.count == 0 && distance(center, loc) > 2*dtree_cfg->res)) {
  //   // Filter out noise
  //
  //   // float3 pt = cudatree::ptFromKey(dtree_cfg, key);
  //   // long hit2 = cudatree::get(dtree_cfg, doctree_data, pt);
  //   // printf("Loc: (%f, %f, %f), Center: (%f, %f, %f), Pt: (%f, %f, %f), %zu, %zu, %zu, dep: %f\n",
  //   //     loc.x, loc.y, loc.z,
  //   //     center.x, center.y, center.z,
  //   //     pt.x, pt.y, pt.z,
  //   //     hit, ind, hit2, dep);
  //   return;
  // }
  // Estimate a lower bound for the size of the feature
  float sizem = min_seen_size / cfg->fx * dep;

  // These are weighted by score so that the viewpoints that see the point the best contribute the most to it's position
  // The hope is that this increases the accuracy with which the point is localized
  float score_val = score[pixel_index]/dep;
  atomicAdd(&d.x, loc.x*score_val);
  atomicAdd(&d.y, loc.y*score_val);
  atomicAdd(&d.z, loc.z*score_val);

  atomicAdd(&d.size, sizem*score_val);

  // Keep track of the score as usual
  atomicAdd(&d.score, score[pixel_index]);
  atomicAdd(&d.val, score_val);
  atomicAdd(&d.count, 1);
}

__global__ void
convolve3d(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    OctreeData *dnew_metadata,
    float *kernel,
    size_t kernel_dim,
    float kernel_res,
    size_t start_i)
{
  // Skip invalid indices
  int i = threadIdx.x + blockIdx.x *blockDim.x + start_i;
  if (i >= dtree_cfg->num_leafs || dmetadata[i].count == 0 || dmetadata[i].score == 0) {
    return;
  }
  OctreeData & cdata = dmetadata[i];
  OctreeData new_data { 0, 0, 0, 0, 0, 0 };
  float radius = kernel_dim*kernel_res/2;

  float3 csubvox = {
    cdata.x/cdata.val,
    cdata.y/cdata.val,
    cdata.z/cdata.val};

  // initialize local nb operator
  uint8_t key[cudatree::MAX_DEPTH+1];
  long root_ind;
  int local_depth;
  cudatree::init_neighborhood_key(
      dtree_cfg, doctree_data,
      csubvox, radius,
      &root_ind, &local_depth, key);
  local_depth = dtree_cfg->max_depth;

  // nb traversal
  int count = 0;
  int count2 = 0;
  do {
    count++;
    float3 pt = cudatree::ptFromKey(dtree_cfg, key);
    long ind = cudatree::get(dtree_cfg, doctree_data, pt);
    if (ind == cudatree::NULL_VOX || ind >= dtree_cfg->num_leafs) {
      continue;
    }
    if (distance(csubvox, pt) > radius) {
      continue;
    }

    count2++;

    // Get stats for other feature
    OctreeData & odata = dmetadata[ind];
    if (odata.val == 0) continue;
    float3 osubvox = {
      odata.x/odata.val,
      odata.y/odata.val,
      odata.z/odata.val
    };

    // Use subpixel coordinates to filter out of range features
    float dist = distance(csubvox, osubvox);
    if (dist >= radius) {
      continue;
    }

    // get index within kernel
    float3 kernel_coords = (1.0f/kernel_res)*(osubvox-csubvox + radius);
    int kernel_index = int(kernel_coords.x)*kernel_dim*kernel_dim + int(kernel_coords.y)*kernel_dim + int(kernel_coords.z);
    if (int(kernel_coords.x) < 0 || int(kernel_coords.x) >= kernel_dim ||
        int(kernel_coords.y) < 0 || int(kernel_coords.y) >= kernel_dim ||
        int(kernel_coords.z) < 0 || int(kernel_coords.z) >= kernel_dim) {
           continue;
    }

    // Update kernel info
    float w = kernel[kernel_index];
    // printf("%.3e, %.3e, %i, %i\n", w, odata.score, kernel_index, odata.count);
    new_data.score += w*odata.score;
    new_data.val += w*odata.val;
    new_data.x += w*odata.x;
    new_data.y += w*odata.y;
    new_data.z += w*odata.z;
    new_data.size += w*odata.size;
    new_data.count += odata.count;

  } while (cudatree::traverse(
        dtree_cfg, doctree_data,
        radius, csubvox,
        key, &root_ind, &local_depth, cudatree::KEY_INC));
  dnew_metadata[i] = new_data;
  printf("%i/%i. %f\n", count2, count, radius);
}


__global__ void
nms_3d(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    bool *selected_points, // initialize to 1s
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio,
    int start_i)
{
  // Skip invalid indices
  int i = threadIdx.x + blockIdx.x *blockDim.x + start_i;
  if (i >= dtree_cfg->num_leafs || !selected_points[i]) {
    return;
  }
  OctreeData & cdata = dmetadata[i];
  if (cdata.count == 0 || cdata.val == 0) {
    selected_points[i] = 0;
    return;
  }

  // Find radius for feature and clip it
  float cradius = cdata.size / cdata.val * size_radius_multi;
  float radius = fmax(
      fmin(cradius, max_nms_radius),
      min_nms_radius
    );

  // get subvoxel location for nb operator
  float3 csubvox = {
    cdata.x/cdata.val,
    cdata.y/cdata.val,
    cdata.z/cdata.val};
  float cscore = cdata.score / cdata.count;

  // initialize local nb operator
  uint8_t key[cudatree::MAX_DEPTH+1];
  long root_ind;
  int local_depth;
  cudatree::init_neighborhood_key(
      dtree_cfg, doctree_data,
      csubvox, radius,
      &root_ind, &local_depth, key);
  local_depth = dtree_cfg->max_depth;

  // nb traversal
  do {
    float3 pt = cudatree::ptFromKey(dtree_cfg, key);
    long ind = cudatree::get(dtree_cfg, doctree_data, pt);
    if (ind == cudatree::NULL_VOX || ind >= dtree_cfg->num_leafs) {
      continue;
    }

    // Get stats for other feature
    OctreeData & odata = dmetadata[ind];
    if (odata.count == 0 || odata.val == 0) {
      selected_points[ind] = 0;
      continue;
    }
    float oradius = odata.size / odata.val;
    float oscore = odata.score / odata.count;
    float3 osubvox = {
      odata.x/odata.val,
      odata.y/odata.val,
      odata.z/odata.val
    };

    // Use subpixel coordinates to filter out of range features
    float dist = distance(csubvox, osubvox);
    if (dist >= radius) {
      continue;
    }

    if (max(odata.count/cdata.count, cdata.count/odata.count) > allowed_count_ratio) {
      continue;
    }

    // Maximal suppresion
    if (oscore > cscore) {
      selected_points[i] = 0;
    }
  } while (cudatree::traverse(
        dtree_cfg, doctree_data,
        radius, csubvox,
        key, &root_ind, &local_depth, cudatree::KEY_INC));
}

__global__ void
sparsity_enforcer(
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    bool *selected_points, // initialize to 1s
    float neighborhood_radius,
    int max_features_in_neighborhood,
    int start_i)
{
  int i = threadIdx.x + blockIdx.x *blockDim.x + start_i;
  if (i >= dtree_cfg->num_leafs) {
    return;
  }
  OctreeData & cdata = dmetadata[i];
  // First, retrieve the coordinates for the key for traversal
  OctreeKey & cokey = dmetamap[i];
  uint8_t ckey[cudatree::MAX_DEPTH+1];
  if (0 != cudatree::convertKey(dtree_cfg, doctree_data, cokey, ckey)) {
    // Convert ckey failed
    printf("Error: convert key ended before reaching root node\n");
    selected_points[i] = 0;
    return;
  }
  float3 center = {cdata.x, cdata.y, cdata.z};

  // initialize local nb operator
  uint8_t key[cudatree::MAX_DEPTH+1];
  long root_ind;
  int local_depth;
  cudatree::init_neighborhood_key(
      dtree_cfg, doctree_data,
      center, neighborhood_radius, &root_ind, &local_depth, key);
  local_depth = dtree_cfg->max_depth;

  int num_higher = 0;

  // nb traversal
  do {
    float3 pt = cudatree::ptFromKey(dtree_cfg, key);
    // Do stuff with the nb voxel
    long ind = cudatree::get(dtree_cfg, doctree_data, pt);
    if (ind == cudatree::NULL_VOX) {
      continue;
    }
    // Get stats for other feature
    OctreeData & odata = dmetadata[ind];
    float3 ocenter = { odata.x, odata.y, odata.z };

    // Use subpixel coordinates to more accurately apply radius
    if (sq_distance(center, ocenter) >= neighborhood_radius*neighborhood_radius) {
      continue;
    }

    // Maximal suppresion
    if (odata.score > cdata.score) {
      num_higher++;
      if (num_higher > max_features_in_neighborhood) {
        selected_points[i] = 0;
        return;
      }
    }
  } while (cudatree::traverse(
        dtree_cfg, doctree_data,
        neighborhood_radius, center,
        key, &root_ind, &local_depth, cudatree::KEY_INC));
}

__global__ void
score_count_projection_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    OctreeData *dmetadata,
    float *depth,
    float *score, float *total_counts,
    curandState_t*states,
    int state_width)
{
  // Function is unused now. Left here for debugging
  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;
  float dist = 0;
  float3 pos, ray;
  int pixel_index = i*cfg->im_cols + j;
  int rand_ind = threadIdx.x + threadIdx.y *state_width;
  cudatree::calcRayPos(cfg,
      // i,//+curand_uniform(&states[rand_ind])-0.5,
      // j,//+curand_uniform(&states[rand_ind])-0.5,
      i,//+curand_uniform(&states[rand_ind])-0.5,
      j,//+curand_uniform(&states[rand_ind])-0.5,
      &pos, &ray);
  long hit = cudatree::raycast(dtree_cfg, pos, ray, &dist, doctree_data);

  if (hit != cudatree::NULL_VOX) {
    long ind = doctree_data[hit];
    if (ind == cudatree::NULL_VOX) {
      return;
    }
    // atomicAdd(&score[pixel_index], 1);
    // atomicAdd(&total_counts[pixel_index], ind-total_counts[pixel_index]);
    OctreeData &d = dmetadata[ind];
    // atomicAdd(&score[pixel_index], d.data.score);
    // atomicAdd(&total_counts[pixel_index], d.data.count);
    score[pixel_index] = max(d.score, score[pixel_index]);
    total_counts[pixel_index] = max((float)d.count, total_counts[pixel_index]);
    // score[pixel_index] = d.score;
    // total_counts[pixel_index] = d.count;
  }
}

}

namespace seg_kernels {

__global__ void
seg_projection_kernel(
    CameraConfig *cfg, OctreeConfig *dtree_cfg,
    long *doctree_data, OctreeKey *dmetamap, SegOctreeData *dmetadata,
    float *depth,
    float *seg,
    curandState_t* states, int state_width)
{
  // Function is unused now. Left here for debugging
  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;
  float dist = 0;
  float3 pos, ray;
  int pixel_index = i*cfg->im_cols + j;
  int rand_ind = threadIdx.x + threadIdx.y *state_width;
  cudatree::calcRayPos(cfg,
      // i,//+curand_uniform(&states[rand_ind])-0.5,
      // j,//+curand_uniform(&states[rand_ind])-0.5,
      i,//+curand_uniform(&states[rand_ind])-0.5,
      j,//+curand_uniform(&states[rand_ind])-0.5,
      &pos, &ray);
  long hit = cudatree::raycast(dtree_cfg, pos, ray, &dist, doctree_data);

  if (hit != cudatree::NULL_VOX) {
    long ind = doctree_data[hit];
    if (ind == cudatree::NULL_VOX) {
      return;
    }
    SegOctreeData &d = dmetadata[ind];

    for (size_t i=0; i<NUM_LABELS; i++) {
      // seg[pixel_index*NUM_LABELS+i] += d.probs[i];
      // atomicAdd(&d.probs[i], __float2half(seg[pixel_index*NUM_LABELS+i]));
      atomicAdd(&d.probs[i], seg[pixel_index*NUM_LABELS+i]);
    }
    atomicAdd(&d.count, 1);
  }
}

__global__ void
seg_render_kernel(
    CameraConfig *cfg,
    OctreeConfig *dtree_cfg,
    long *doctree_data,
    OctreeKey *dmetamap,
    SegOctreeData *dmetadata,
    float *depth, // input
    float *seg, int32_t *total_counts, // output
    curandState_t* states,
    int state_width)
{
  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;
  float dist = 0;
  float3 pos, ray;
  int pixel_index = i*cfg->im_cols + j;
  cudatree::calcRayPos(cfg, i, j, &pos, &ray);
  float dist2plane = norm(ray);
  long hit = cudatree::raycast(dtree_cfg, pos, ray, &dist, doctree_data);

  // similar triangles to get planar depth, then add margin so depth lands in voxel
  if (hit != cudatree::NULL_VOX) {
    long ind = doctree_data[hit];
    if (ind == cudatree::NULL_VOX) {
      return;
    }
    SegOctreeData &d = dmetadata[ind];
    // It tends to be offset by this much, but that's the end user's job
    // depth[pixel_index] = dist/dist2plane + dtree_cfg->res*2;
    for (size_t i=0; i<NUM_LABELS; i++) {
      // seg[pixel_index*NUM_LABELS+i] += d.probs[i];
      // atomicAdd(&seg[pixel_index*NUM_LABELS+i], float(d.probs[i]));
      atomicAdd(&seg[pixel_index*NUM_LABELS+i], d.probs[i]);
    }
    atomicAdd(&total_counts[pixel_index], d.count);
  }
}

}
