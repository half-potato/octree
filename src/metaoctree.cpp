#include "octree/octree.h"
#include "octree/metaoctree.h"

#include <iostream>
#include <fstream>
#include <cstring>

namespace octree {

MetaOctreeType stoe(std::string const& s) {
  std::string trimmed = trim_copy(s);
  std::transform(trimmed.begin(), trimmed.end(), trimmed.begin(), ::tolower);
  if (trimmed == "empty") {
    return MetaOctreeType::EMPTY;
  } else if (trimmed == "point_data_v1") {
    return MetaOctreeType::POINT_DATA_V1;
  } else if (trimmed == "point_data_v2") {
    return MetaOctreeType::POINT_DATA_V2;
  } else if (trimmed == "ade_seg") {
    return MetaOctreeType::ADE_SEG;
  }
  return MetaOctreeType::INVALID;
}

std::string etos(MetaOctreeType t) {
  switch (t) {
    case MetaOctreeType::EMPTY:
      return "empty";
    case MetaOctreeType::POINT_DATA_V1:
      return "point_data_v1";
    case MetaOctreeType::POINT_DATA_V2:
      return "point_data_v2";
    case MetaOctreeType::ADE_SEG:
      return "ade_seg";
    default:
      return "invalid";
  }
}

void * 
MetaOctree::get(size_t ind) {
  switch (this->datatype) {
    case POINT_DATA_V1:
      return &get_metadata<PointDataV1>()[ind];
    case POINT_DATA_V2:
      return &get_metadata<PointDataV2>()[ind];
    case ADE_SEG:
      return &get_metadata<ADESeg>()[ind];
    default:
      return NULL;
  }
}

void * 
MetaOctree::get(vec3 pt) {
  int64_t *ret = tree.get(pt);
  if (ret == NULL) {
    return NULL;
  } else {
    return get(size_t(*ret));
  }
}

template <typename T>
std::vector<T> &
MetaOctree::get_metadata() {
  return boost::get<std::vector<T>>(metadata_var);
}

int64_t 
MetaOctree::insert(vec3 pt, void *data)
{
  // First, check if there is existing metadata
  int64_t *ret = tree.get(pt);
  if (ret == NULL) {
    OctreeKey key = tree.insert(pt, numLeafs());
    metamap.push_back(key);
    switch (this->datatype) {
      case EMPTY:
        break;
      case POINT_DATA_V1:
        get_metadata<PointDataV1>().push_back(*((PointDataV1 *)data));
        break;
      case POINT_DATA_V2:
        get_metadata<PointDataV2>().push_back(*((PointDataV2 *)data));
        break;
      case ADE_SEG:
        get_metadata<ADESeg>().push_back(*((ADESeg *)data));
        break;
      case INVALID:
        throw 20;
    }
    return metamap.size()-1;
  } else {
    switch (this->datatype) {
      case EMPTY:
        break;
      case POINT_DATA_V1:
        get_metadata<PointDataV1>()[*ret] = *((PointDataV1 *)data);
        break;
      case POINT_DATA_V2:
        get_metadata<PointDataV2>()[*ret] = *((PointDataV2 *)data);
        break;
      case ADE_SEG:
        get_metadata<ADESeg>()[*ret] = *((ADESeg *)data);
        break;
      case INVALID:
        throw 20;
    }
    return *ret;
  }
}

void
MetaOctree::initializeType(MetaOctreeType t)
{
  if (datatype == t) {
    return;
  }
  datatype = t;
  switch (t) {
    case POINT_DATA_V1:
      metadata_var = std::vector<PointDataV1>();
      get_metadata<PointDataV1>().resize(metamap.size());
      break;
    case POINT_DATA_V2:
      metadata_var = std::vector<PointDataV2>();
      get_metadata<PointDataV2>().resize(metamap.size());
      break;
    case ADE_SEG:
      metadata_var = std::vector<ADESeg>();
      get_metadata<ADESeg>().resize(metamap.size());
      break;
    default:
      break;
  }
  return;
}

std::string
MetaOctree::generateHeader()
{
  auto root = cpptoml::make_table();
  root->insert("origin_x", tree.origin.x);
  root->insert("origin_y", tree.origin.y);
  root->insert("origin_z", tree.origin.z);
  root->insert("scale", tree.scale);
  root->insert("depth", tree.depth);
  root->insert("version", tree.version);
  root->insert("num_nodes", numNodes());
  root->insert("num_leafs", numLeafs());
  root->insert("datatype", etos(datatype));
  std::stringstream test;
  test << PREHEADER;
  test << *root;
  test << "\n";

  // counter number of lines and add it to the header
  int number_of_lines = 1; // +1 for the line we are about to add
  std::string line;
  while (std::getline(test, line))
    ++number_of_lines;
  root->insert("header_length", number_of_lines);

  std::stringstream ss;
  ss << PREHEADER;
  ss << *root;
  ss << "\n";
  return ss.str();
}

MetaOctree::MetaOctree(std::string fname, size_t max_chunk_size)
{
  // This is just the sample code for reading and writing
  std::ifstream is(fname, std::ifstream::binary);
  // std::ifstream is(fname);
  if (is) {
    // get length of file:
    is.seekg (0, is.end);
    size_t length = is.tellg();
    is.seekg (0, is.beg);

    printf("Reading %zu characters...\n", length);

    int header_size, num_nodes;
    std::string datatype_name;
    size_t num_leafs;
    try {
      auto root = tree.parseHeader(is, &header_size, &num_nodes);
      datatype_name = root->get_as<std::string>("datatype").value_or("empty");
      num_leafs = root->get_as<size_t>("num_leafs").value_or(0);
    } catch (const std::exception& e) {
      throw std::invalid_argument("Error reading header. Malformed\n");
    }
    datatype = stoe(datatype_name);
    std::cout << "Datatype: " << etos(datatype) << std::endl;

    // is.seekg (header_size, is.beg);
    printf("Tree resolution: %f\n", tree.scale);
    printf("Num leafs: %zu\n", num_leafs);

    // read data in chunks
    // size_t num_nodes = (length-header_size)/sizeof(OctreeNode);
    tree.data.resize(num_nodes);
    for (size_t i=0; i<sizeof(OctreeNode)*num_nodes; i+=max_chunk_size) {
      size_t chunk_size = std::min(sizeof(OctreeNode)*num_nodes-i, max_chunk_size);
      is.read (((char *)tree.data.data())+i, chunk_size);
    }

    metamap.resize(num_leafs);
    char *mk = (char *)metamap.data();
    size_t metamap_size = num_leafs*sizeof(OctreeKey);
    printf("Reading metamap with length %zu\n", metamap_size);
    for (size_t i=0; i<metamap_size; i+=max_chunk_size) {
      size_t chunk_size = std::min(metamap_size-i, max_chunk_size);
      is.read (mk+i, chunk_size);
    }

    if (getTypeSize() != 0) {
      // Now we read the metadata
      switch (datatype) {
        case POINT_DATA_V1:
          get_metadata<PointDataV1>().resize(num_leafs);
          break;
        case POINT_DATA_V2:
          get_metadata<PointDataV2>().resize(num_leafs);
          break;
        case ADE_SEG:
          get_metadata<ADESeg>().resize(num_leafs);
          break;
        default:
          break;
      }
      size_t metadata_size = num_leafs*getTypeSize();
      char *md = (char *)metadata();

      printf("Reading metadata with length %zu\n", metadata_size);
      for (size_t i=0; i<metadata_size; i+=max_chunk_size) {
        size_t chunk_size = std::min(metadata_size-i, max_chunk_size);
        is.read (md+i, chunk_size);
      }
    }
    is.close();
  } else {
    std::cout << "File " << fname << " was not found." << std::endl;
  }
}

int 
MetaOctree::save(std::string fname, size_t max_chunk_size) {
  std::cout << "Saved tree with nodes: " << numNodes() << std::endl;
  std::ofstream outfile (fname, std::ofstream::binary);

  outfile << generateHeader();

  // Write body
  size_t num_node_per_buffer = max_chunk_size/sizeof(OctreeNode);

  for (size_t i=0; i<tree.data.size(); i+=num_node_per_buffer) {
    size_t num_nodes = std::min(num_node_per_buffer, tree.data.size()-i);
    outfile.write((char *)(tree.data.data()+i), num_nodes*sizeof(OctreeNode));
  }

  // Write metadata
  char *mk = (char *)metamap.data();
  printf("Num leafs: %zu\n", numLeafs());
  printf("Metamap size: %zu\n", metamap.size());

  size_t metamap_size = numLeafs()*sizeof(OctreeKey);
  printf("Reading metamap with length %zu\n", metamap_size);
  for (size_t i=0; i<metamap_size; i+=max_chunk_size) {
    size_t chunk_size = std::min(metamap_size-i, max_chunk_size);
    outfile.write(mk+i, chunk_size);
  }


  if (getTypeSize() != 0) {
    size_t metadata_size = numLeafs()*getTypeSize();
    char *md = (char *)metadata();
    for (size_t i=0; i<metadata_size; i+=max_chunk_size) {
      size_t chunk_size = std::min(metadata_size-i, max_chunk_size);
      outfile.write(md+i, chunk_size);
    }
  }

  outfile.close();
  return 0;
}

}
