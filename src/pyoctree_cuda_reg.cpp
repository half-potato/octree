#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <cuda.h>

#include "octree/metaoctree.h"
#include "octree/pyoctree.h"
#include "octree/pyoctree_cuda_reg.h"
#include "octree/seg_raycaster.h"
#include "octree/size_est.h"

#define CHECK_CUDA(x) TORCH_CHECK(x.type().is_cuda(), #x " must be a CUDA tensor")

std::vector<torch::Tensor>
render_depth(
    std::vector<torch::Tensor> pose_tensors,
    int w, int h,
    std::string & octree_path,
    std::vector<float> & calib)
{
  std::vector<torch::Tensor> targets = cuda_render_depth(
      octree_path, w, h, pose_tensors, calib);
  return targets;
}

torch::Tensor
neighborhood_sparsity_enforcement(
    torch::Tensor & features,
    float neighborhood_radius,
    int max_features_in_neighborhood,
    float res)
{
  return cuda_neighborhood_sparsity_enforcement(
      features, neighborhood_radius, max_features_in_neighborhood, res);
}

torch::Tensor
get_face_inds(
    torch::Tensor & pts,
    torch::Tensor & faces,
    torch::Tensor & vertices)
{
  return cuda_get_face_inds(pts, faces, vertices);
}

torch::Tensor
raycast_nms(
    std::vector<torch::Tensor> & depth_maps,
    std::vector<torch::Tensor> & feature_maps, // [(score, subx, suby, size), im_rows, im_cols]
    std::vector<torch::Tensor> & pose_tensors,
    std::string & octree_path,
    std::vector<float> & calib,
    float min_nms_radius,
    float max_nms_radius,
    float size_radius_multi,
    float iou_thres,
    float allowed_count_ratio,
    float threshold,
    float min_seen_size,
    float max_unseen_size,
    int kernel_size, 
    int cell_size)
{
  // cell size is a parameter for when the feature_maps has different dimensions than the depth map
  // pass in the normal calib for the full depth and set cellsize to be the ratio between the depth_map and feature_map
  torch::Tensor targets = cuda_raycast_nms(
      octree_path, depth_maps, feature_maps, pose_tensors, calib, 
      min_nms_radius, max_nms_radius, size_radius_multi, iou_thres, allowed_count_ratio, threshold,
      min_seen_size, max_unseen_size, kernel_size, cell_size);
  return targets;
}

void init_cuda_module(py::module_ &m) {
  octree::init_seg_module(m);
  octree::init_point_module(m);
  m.def("render_depth", &render_depth, pybind11::return_value_policy::take_ownership);
  m.def("get_face_inds", &get_face_inds, pybind11::return_value_policy::take_ownership);
  m.def("raycast_nms", &raycast_nms,
      py::arg("depth_maps"), py::arg("score_maps"), py::arg("pose_tensors"), py::arg("octree_path"), py::arg("calib"), 
      py::arg("min_nms_radius"), py::arg("max_nms_radius"), py::arg("size_radius_multi"),
      py::arg("iou_thres"), py::arg("allowed_count_ratio"), py::arg("threshold"), py::arg("min_seen_size"), py::arg("max_unseen_size"), py::arg("kernel_size"), py::arg("cell_size")=1,
      pybind11::return_value_policy::take_ownership);
  m.def("neighborhood_sparsity_enforcement", &neighborhood_sparsity_enforcement,
      pybind11::return_value_policy::take_ownership);
}
