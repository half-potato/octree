#include "octree/cl_raycaster.h"
#include <math.h>
#include <filesystem>
#include <boost/filesystem.hpp>
#define logError(err) _logError(err, __LINE__)

namespace fs = boost::filesystem;
using namespace octree;

const char *getErrorString(cl_int error)
{
switch(error){
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}

void _logError(cl_int err, int line) {
  if (err != 0)
    printf("Error on line %i: %s\n", line, getErrorString(err));
}

CLRaycaster::CLRaycaster(octree::MetaOctree tree) {

  fs::path fpath (__FILE__);
  fpath = fpath.parent_path() / "raycast.cl";
  // fpath = fpath.replace_filename("raycast.cl");
  cl::Platform platform = getPlatform();
  device = getDevice(platform);
  context = cl::Context({device});

  // create buffers on device (allocate space on GPU)
  std::cout << "Max buffer size: " << CL_DEVICE_MAX_MEM_ALLOC_SIZE << std::endl;
  cl_int err;
  buffer_data = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(OctreeNode) * tree.numNodes(), NULL, &err);
  logError(err);
  buffer_octree_cfg = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(OctreeConfig), NULL, &err);
  logError(err);
  buffer_camera_cfg = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(CameraConfig), NULL, &err);
  logError(err);

  scoreKernel = createKernel(fpath.string(), "raycast_score", context, device, tree.getDepth(), tree.numNodes());
  countKernel = createKernel(fpath.string(), "raycast_count", context, device, tree.getDepth(), tree.numNodes());
  depthKernel = createKernel(fpath.string(), "raycast_depth", context, device, tree.getDepth(), tree.numNodes());
  indexKernel = createKernel(fpath.string(), "raycast_index", context, device, tree.getDepth(), tree.numNodes());
  queue = initQueue(context, device, tree);
}

cl::Platform
CLRaycaster::getPlatform() {
  // get all platforms (drivers)
  std::vector<cl::Platform> all_platforms;
  cl::Platform::get(&all_platforms);

  if (all_platforms.size()==0) {
    std::cout<<" No platforms found. Check OpenCL installation!\n";
    exit(1);
  }
  cl::Platform default_platform=all_platforms[0];
  std::cout << "Using platform: " << default_platform.getInfo<CL_PLATFORM_NAME>() << "\n";
  return default_platform;
}

cl::Device
CLRaycaster::getDevice(cl::Platform platform) {
  // get default device of the default platform
  std::vector<cl::Device> all_devices;
  platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
  if(all_devices.size()==0){
    std::cout << " No devices found. Check OpenCL installation!\n";
    exit(1);
  }

  // use device[1] because that's a GPU; device[0] is the CPU
  return all_devices[all_devices.size()-1];
}

cl::Kernel
CLRaycaster::createKernel(
    std::string kernel_path,
    std::string prog_name,
    cl::Context context,
    cl::Device device,
    int max_tree_depth,
    int numNodes)
{
  // create the program that we want to execute on the device
  cl::Program::Sources sources;

  // ==========================================================
  // Create source
  // ==========================================================
  std::ifstream file;
  std::stringstream stream;
  file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  file.open(kernel_path);
  stream << file.rdbuf();
  file.close();
  std::string kernel_code;

  // compile in max stack size
  int stack_size = (int)max_tree_depth*3+1;
  stack_size = std::min(CL_DEVICE_MAX_MEM_ALLOC_SIZE, stack_size);
  printf("Creating stack with size: %i\n", stack_size);
  char header[300];
  sprintf(header, 
      "#define STACK_SIZE %i\n",
      stack_size);
  kernel_code.append(header);
  kernel_code.append(stream.str());
  sources.push_back({kernel_code.c_str(), kernel_code.length()});

  cl_int err;
  cl::Program program(context, sources, &err);
  logError(err);
  if (program.build({device}) != CL_SUCCESS) {
    std::cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
    exit(1);
  }
  cl::Kernel kern (program, prog_name.c_str(), &err);
  logError(err);

  kern.setArg(0, buffer_data);
  kern.setArg(1, buffer_octree_cfg);
  kern.setArg(2, buffer_camera_cfg);
  return kern;
}

cl::CommandQueue
CLRaycaster::initQueue(cl::Context context, cl::Device device, octree::MetaOctree tree) {
  // Load tree
  octree_config = {
    .max_depth=tree.getDepth(), 
    .res = tree.getScale(),
    .origin = {tree.getOrigin().x, tree.getOrigin().y, tree.getOrigin().z}
  };

  int64_t * octree_data = new int64_t[tree.numNodes()*8];
  for (size_t i=0; i<tree.numNodes(); i++) {
    for (size_t j=0; j<8; j++) {
      octree_data[i*8+j] = tree.getNode(i)->children[j];
    }
  }
  cl_int err;
  // create a queue (a queue of commands that the GPU will execute)
  cl::CommandQueue queue (context, device, 0, &err);
  logError(err);
  err = queue.enqueueWriteBuffer(buffer_data, CL_TRUE, 0, sizeof(int64_t) * tree.numNodes() * 8, octree_data);
  logError(err);
  err = queue.enqueueWriteBuffer(buffer_octree_cfg, CL_TRUE, 0, sizeof(OctreeConfig), &octree_config);
  logError(err);
  return queue;
}

void CLRaycaster::renderScore(CameraConfig camera_config, float * image) {
  queue.enqueueWriteBuffer(buffer_camera_cfg, CL_TRUE, 0, sizeof(CameraConfig), &camera_config);

  // Output Buffers
  cl_int err;
  buffer_image = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * camera_config.im_cols * camera_config.im_rows, NULL, &err);
  logError(err);
  buffer_test = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * 20, NULL, &err);
  logError(err);
  scoreKernel.setArg(3, buffer_image);
  // raycast_image_op.setArg(3, cl_vbo);
  scoreKernel.setArg(4, buffer_test);

  err = queue.enqueueNDRangeKernel(scoreKernel, cl::NullRange, cl::NDRange(camera_config.im_rows, camera_config.im_cols), cl::NullRange);
  logError(err);
  err = queue.finish();
  logError(err);

  err = queue.enqueueReadBuffer(buffer_image, CL_TRUE, 0, sizeof(float) * camera_config.im_cols * camera_config.im_rows, image);
  logError(err);

  float *test = new float[20];
  err = queue.enqueueReadBuffer(buffer_test, CL_TRUE, 0, sizeof(float) * 20, test);
  logError(err);
  delete[] test;
}

void CLRaycaster::renderDepth(CameraConfig camera_config, float * image) {
  queue.enqueueWriteBuffer(buffer_camera_cfg, CL_TRUE, 0, sizeof(CameraConfig), &camera_config);

  // Output Buffers
  cl_int err;
  buffer_image = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * camera_config.im_cols * camera_config.im_rows, NULL, &err);
  logError(err);
  buffer_test = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * 20, NULL, &err);
  logError(err);
  depthKernel.setArg(3, buffer_image);
  // raycast_image_op.setArg(3, cl_vbo);
  depthKernel.setArg(4, buffer_test);

  err = queue.enqueueNDRangeKernel(depthKernel, cl::NullRange, cl::NDRange(camera_config.im_rows, camera_config.im_cols), cl::NullRange);
  logError(err);
  err = queue.finish();
  logError(err);

  err = queue.enqueueReadBuffer(buffer_image, CL_TRUE, 0, sizeof(float) * camera_config.im_cols * camera_config.im_rows, image);
  logError(err);

  float *test = new float[20];
  err = queue.enqueueReadBuffer(buffer_test, CL_TRUE, 0, sizeof(float) * 20, test);
  logError(err);
  delete[] test;
}

void CLRaycaster::renderCount(CameraConfig camera_config, int * image) {
  queue.enqueueWriteBuffer(buffer_camera_cfg, CL_TRUE, 0, sizeof(CameraConfig), &camera_config);

  // Output Buffers
  cl_int err;
  buffer_image = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(int) * camera_config.im_cols * camera_config.im_rows, NULL, &err);
  logError(err);
  buffer_test = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * 20, NULL, &err);
  logError(err);
  countKernel.setArg(3, buffer_image);
  // raycast_image_op.setArg(3, cl_vbo);
  countKernel.setArg(4, buffer_test);

  err = queue.enqueueNDRangeKernel(countKernel, cl::NullRange, cl::NDRange(camera_config.im_rows, camera_config.im_cols), cl::NullRange);
  logError(err);
  err = queue.finish();
  logError(err);

  err = queue.enqueueReadBuffer(buffer_image, CL_TRUE, 0, sizeof(int) * camera_config.im_cols * camera_config.im_rows, image);
  logError(err);

  float *test = new float[20];
  err = queue.enqueueReadBuffer(buffer_test, CL_TRUE, 0, sizeof(float) * 20, test);
  logError(err);
  delete[] test;
}

void CLRaycaster::renderIndex(CameraConfig camera_config, int64_t * image) {
  queue.enqueueWriteBuffer(buffer_camera_cfg, CL_TRUE, 0, sizeof(CameraConfig), &camera_config);

  // Output Buffers
  cl_int err;
  buffer_image = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(int64_t) * camera_config.im_cols * camera_config.im_rows, NULL, &err);
  logError(err);
  buffer_test = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * 20, NULL, &err);
  logError(err);
  indexKernel.setArg(3, buffer_image);
  // raycast_image_op.setArg(3, cl_vbo);
  indexKernel.setArg(4, buffer_test);

  err = queue.enqueueNDRangeKernel(indexKernel, cl::NullRange, cl::NDRange(camera_config.im_rows, camera_config.im_cols), cl::NullRange);
  logError(err);
  err = queue.finish();
  logError(err);

  err = queue.enqueueReadBuffer(buffer_image, CL_TRUE, 0, sizeof(int64_t) * camera_config.im_cols * camera_config.im_rows, image);
  logError(err);

  float *test = new float[20];
  err = queue.enqueueReadBuffer(buffer_test, CL_TRUE, 0, sizeof(float) * 20, test);
  logError(err);
  delete[] test;
}
