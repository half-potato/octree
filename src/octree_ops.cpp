#include "octree/octree_ops.h"

namespace octree {

float
sphere_intersection(vec3 a, float R, vec3 b, float r)
{
  // a, b are the centers of the spheres
  // R, r are the radii of the spheres, respectively

  // The idea is to center and rotate the spheres such that a lies at origin and b lies along
  // some translation along the x axis
  float d = distance(a, b);
  // Then, we find the plane of intersection
  float x = (d*d - r*r + R*R)/2/d;
  if (x > d) {
    return 0;
  }
  // This allows us to find the intersection volume as the sum of the two "caps"
  // (cut a sphere with a plane and it gives you a cap shape)
  // radius to base of caps
  float a_a = x;
  float a_b = d - x;
  // cap heights
  float h_a = R - x;
  float h_b = x - d + r;
  // cap volumes
  float v_a = M_PI/6/h_a*(3*a_a*a_a+h_a*h_a);
  float v_b = M_PI/6/h_b*(3*a_b*a_b+h_b*h_b);
  if (v_a < 0 || v_b < 0) {
    return 0;
  }
  return v_a + v_b;
}

void
nms(MetaOctree tree, float iou_thres, float max_nms_radius) {
  bool *selected_points = (bool*)malloc(sizeof(bool)*tree.numLeafs());
  for (size_t i=0; i<tree.numLeafs(); i++) {
    OctreeKey &okey = tree.metadata[i].key;
    auto key = tree.tree.convertKey(okey);
    vec3 center = tree.tree.ptFromKey(key);
    Data &cdata = tree.metadata[i].data;

    // Estimate radius of feature. This allows us to know what radius of sphere we should check
    float cradius = cdata.size / cdata.count;
    float cscore = cdata.score / cdata.count;
    // This is capped to avoid really long computations
    float radius = std::min(cradius, max_nms_radius);
    vec3 csubvox {center.x + cdata.x/cdata.count, center.y + cdata.y/cdata.count, center.z + cdata.z/cdata.count};
    auto nb = tree.neighborhood(center, radius);
    for (OctreeKey &okey : nb) {
      int64_t *ind = tree.tree.get(okey);
      if (ind == NULL) {
        // This shouldn't happen. Just playing safe.
        continue;
      }
      Data &odata = tree.metadata[*ind].data;
      vec3 osubvox {odata.x/odata.count, odata.y/odata.count, odata.z/odata.count};
      float oradius = odata.size / odata.count;
      float oscore = odata.score / odata.count;
      // First check if the subvoxel coordinates are within range of each other
      float dist = glm::distance(csubvox, osubvox);

      if (dist > cradius || dist > oradius) {
        continue;
      }

      // Now we check the IOU of the spheres
      // This is necessary because we wouldn't want to use the same radius for large features
      // as we do for small features
      float vinter = sphere_intersection(csubvox, cradius, osubvox, oradius);
      float cvol = 4*M_PI*cradius*cradius*cradius/3;
      float ovol = 4*M_PI*oradius*oradius*oradius/3;
      float vunion = cvol + ovol - vinter;
      float iou = vinter / vunion;

      if (iou < iou_thres) {
        continue;
      }
      // Maximal suppresion
      if (oscore <= cscore) {
        continue;
      }

      selected_points[i] = 0;
    }
  }
  free(selected_points);
}

}
