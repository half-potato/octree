import time
import json

import cv2
import torch
import pyoctree
import numpy as np
import torch.nn.functional as F
from pyquaternion import Quaternion

from pathlib import Path
from icecream import ic
import matplotlib.pyplot as plt

NUM_LABELS = 150
H = 256
W = 256
calib = [H/2, W/2, H/2, W/2]
octree_path = 'octrees/RealisticRendering.oct'

seg_ray = pyoctree.SegRaycaster(octree_path, calib, H, W)
seg = torch.rand(H, W, NUM_LABELS).float()
seg /= seg.sum(dim=2, keepdim=True)
pose = torch.tensor([[ 6.4307e-01, -3.6582e-02, -7.6493e-01, -7.6983e-01],
        [-7.6581e-01, -3.0719e-02, -6.4233e-01,  3.3141e+00],
        [ 1.7347e-18,  9.9886e-01, -4.7769e-02, -4.8236e-01],
        [ 0.0000e+00,  0.0000e+00,  0.0000e+00,  1.0000e+00]]).float()

seg_ray.projectSegmentation(pose, seg, seg, 10)
new_seg, counts = seg_ray.renderSegmentation(pose, seg, 10)
fig, axs = plt.subplots(2)
axs[0].imshow(seg[..., 0])
axs[1].imshow((new_seg / counts.reshape(H, W, 1))[..., 0])
plt.show()
