# Features
- GPU parallelized neighborhood iteration (scales only with radius)  
- O(d) GPU accelerated raycasting  

# TODO
- Refactor implementation to expose octree and cudaoctree to python
- Refactor implementation to expose cuda operators on these data structures to python
- Make it possible to select the device to use for CUDA
