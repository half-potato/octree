import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from icecream import ic

node_loc = (3.840000, -1.280000, -3.840000)
center = (2.550000, -3.410000, -2.230000)
closest = (2.593503, -3.338169, -2.284294)
dep = 3
res = 0.02
scale = res * 2**dep
radius = 0.1

fig = plt.figure()
ax = fig.gca(projection='3d')

def draw_sphere(r, c, col='r'):
    # draw sphere
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x = np.cos(u)*np.sin(v)*r+c[0]
    y = np.sin(u)*np.sin(v)*r+c[1]
    z = np.cos(v)*r+c[2]
    ax.plot_wireframe(x, y, z, color=col)

ax.scatter(*node_loc, c='green')
ax.scatter(*center, c='red')
ax.scatter(*closest, c='orange')
draw_sphere(radius, center)
plt.show()
