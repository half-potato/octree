import time
import json

import cv2
import torch
import pyoctree
import numpy as np
import torch.nn.functional as F
from pyquaternion import Quaternion

from pathlib import Path
from icecream import ic
import matplotlib.pyplot as plt

res = 0.02

cross_correlation = torch.tensor([
    [0., 0., 1., 0., 0.],
    [0., 0.,-8., 0., 0.],
    [0., 0., 0., 0., 0.],
    [0., 0., 8., 0., 0.],
    [0., 0.,-1., 0., 0.]]).view(1, 1, 5, 5).float()/12

# pytorch
def edgyness(score_map):
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    #  batch = gf1(batch)
    #  dii = F.conv2d(batch, dii_filter, padding=3, dilation=3, stride=1)
    #  dij = F.conv2d(batch, dij_filter, padding=3, dilation=3, stride=1)
    #  djj = F.conv2d(batch, djj_filter, padding=3, dilation=3, stride=1)
    kern = cross_correlation
    Ix = F.conv2d(batch, kern, padding=kern.shape[-1]//2, stride=1)
    Iy = F.conv2d(batch, kern.transpose(2, 3), padding=kern.shape[-1]//2, stride=1)

    w = 1
    s = w*2+1
    w_ix2 = F.avg_pool2d(Ix*Ix, s, stride=1, padding=w)*s*s
    w_iy2 = F.avg_pool2d(Iy*Iy, s, stride=1, padding=w)*s*s
    w_ixiy = F.avg_pool2d(Ix*Iy, s, stride=1, padding=w)*s*s
    tr = w_ix2 + w_iy2
    det = w_ix2*w_iy2 - w_ixiy**2
    J0 = (tr - torch.sqrt(tr**2 - 4*det))/2
    J0[torch.isnan(J0)] = 0

    return J0.squeeze(0)

def read_pose(path):
    with open(path, "r") as f:
        dat = json.load(f)
        ori = dat["pitchyawroll"]
        pos = np.array(dat["position"])*0.01
        pos[2] *= -1
        q = Quaternion(axis=[0,0,1], degrees=ori[1]) * \
            Quaternion(axis=[0,1,0], degrees=ori[0])
        corr = np.array([[0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [1, 0, 0, 0],
                         [0, 0, 0, 1]]).T
        ret = q.transformation_matrix.dot(corr)
        ret[:3, 3] = pos
        return ret

def resize_with_crop(image, shape):
    #  scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    #  image = cv2.resize(image,
    #                     (int(scale*image.shape[0]), int(scale*image.shape[1])),
    #                     interp)
    (h, w) = image.shape[:2]
    (nh, nw) = shape[:2]
    oh, ow = (h-nh)//2, (w-nw)//2
    return image[oh:oh+nh, ow:ow+nw]

def scale_then_crop(image, shape, interp=cv2.INTER_LINEAR):
    #  image = cv2.resize(image, (max(shape), max(shape)), interp)
    scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    image = cv2.resize(image,
                       (int(scale*image.shape[1]), int(scale*image.shape[0])),
                       interp)

    image = resize_with_crop(image, shape)
    return image

def convert_depth(calib, point_depth):
    H = point_depth.shape[0]
    W = point_depth.shape[1]
    #  fov = 67.5
    #  f = H / (2 * math.tan(fov * math.pi / 360))

    fx, fy, cx, cy = calib
    #  f = 360
    #  i_c = np.float(H) / 2 - 1
    #  j_c = np.float(W) / 2 - 1
    columns, rows = np.meshgrid(np.linspace(0, W-1, num=W), np.linspace(0, H-1, num=H))
    y = (rows - cy)/fy
    x = (columns - cx)/fx
    z = 1
    dist_to_plane = (x**2 + y**2 + z**2)**(0.5)
    #  depth = f * point_depth / np.sqrt(f**2 + (rows - i_c)**2 + (columns - j_c)**2)
    depth = point_depth/dist_to_plane
    return depth

def calib2k(calib):
    return torch.tensor([
        [calib[0], 0, calib[2]],
        [0, calib[1], calib[3]],
        [0, 0, 1]]).float()

def project_points(points, pose, K, depth):
    # Points: (N, 5) (x, y, z, score, size) in space frame
    # Pose: (4, 4) S->I
    # Ki: (3, 3) inverse of camera matrix K
    # depth[H, W]
    H, W = depth.shape[:2]
    trans = points[:, :3] @ pose[:3, :3].T + pose[:3, 3].reshape(1, 3)
    camera_space = trans @ K.T
    xhat = camera_space[:, :2]/camera_space[:, 2:3]
    # remove points behind the camera
    # remove points out of bounds
    inds = torch.round(xhat).long()
    cond = (inds[:, 0] >= H) | (inds[:, 1] >= W) | (inds[:, 0] < 0) | (inds[:, 1] < 0)
    xhatf = xhat[~cond]
    inds = inds[~cond]
    # remove points whose depth differs significantly (and mask these areas?)
    z = depth[inds[:, 1], inds[:, 0]]
    z_hat = camera_space[~cond, 2]
    # the difference between depths is caused by two issues:
    # - 2*res because of the discretization along the ray
    # - > because of discretization orthgonal to the ray. This is what we need to prevent the most
    good_dep = torch.abs(z-z_hat) < 3*res

    used_pts = points[~cond][good_dep]
    return torch.stack([
        xhatf[good_dep, 0],
        xhatf[good_dep, 1],
        used_pts[:, 3],
        used_pts[:, 4]/z[good_dep]*K[0, 0],
    ], dim=1)
    #  return torch.stack([xhatf[good_dep, 0], xhatf[good_dep, 1], used_pts[:, 3], used_pts[:, 4]], dim=1)

#  octree_path = "octrees/RealisticRendering.oct"
#  octree_path = "/data/unrealcv/octomaps/ArchinteriorsVol2Scene1.oct"
#  octree_path = "/data/unrealcv/octomaps/ArchinteriorsVol2Scene2.oct"
#  octree_path = "/data/unrealcv/octomaps/ArchVisInterior.oct.highres.new"
octree_path = "/data/unrealcv/octrees/ArchVisInterior.oct"
#  octree_path = "/data/unrealcv/octomaps/ArchVisInterior.oct"
#  h = w = 120
h = w = 480
ds_path = Path("/data/unrealcv/images/ArchVisInterior/")
out_path = Path("/data/output/painted/ArchVisInterior/run-8-13-archvisinterior_15")
#  h = w = 1080

calib = list(np.array([320, 320, 240, 240])*h/480)

K = calib2k(calib)
Ki = torch.inverse(K)

M = 0
N = 10
res = 0.005

# Initialize data
scaling_factors = torch.tensor([0.0, 0.0, 0]).reshape(3, 1, 1)
bias = torch.tensor([0.5, 0.5, 10]).reshape(3, 1, 1)

feature_maps = []
pose_tensors = []
depth_maps = []
for i in range(M, N+M):
    raw_dep = cv2.imread(str(ds_path / f"{i:04d}_depth.exr"), -1)
    conv_dep = convert_depth(calib, scale_then_crop(raw_dep, (h, w), cv2.INTER_NEAREST))
    depth_maps.append(torch.tensor(conv_dep).float())

    counts = torch.tensor(cv2.imread(str(out_path / f"data_counts_{i:05d}.exr"), -1))
    rs = torch.tensor(cv2.imread(str(out_path / f"reproj_score_{i:05d}.exr"), -1))
    counts[counts==0] = 1
    score_map = (rs / counts).reshape(1, h, w)
    score_map = edgyness(score_map)*10
    attrib = torch.rand(3, h, w) * scaling_factors + bias
    feature_map = torch.cat([score_map, attrib], dim=0)
    feature_maps.append(feature_map)

    pose = torch.tensor(read_pose(ds_path / f"{i:04d}_pose.json")).float()
    pose_tensors.append(pose)


for depth_map in depth_maps:
    depth_map[depth_map > 10.0] = 0

#  """
# GPU Ops
depth_maps_r = pyoctree.render_depth(pose_tensors, h, w, octree_path, calib)
#  out = pyoctree.reproject_score_maps(depth_maps, feature_maps, pose_tensors, octree_path, calib)
start_time = time.time()
out = pyoctree.raycast_score_maps(depth_maps, feature_maps, pose_tensors, octree_path, calib, max_nms_radius=3.0, iou_thres=0.1, min_count=1)
print(f'Raycasting in: {time.time()-start_time} seconds')
cscores = out[:N]
counts = out[N:]
scores = [cscore/torch.max(count, torch.ones_like(count)) for cscore, count in zip(cscores, counts)]
#  """

cmap = plt.get_cmap("viridis")

for i, (d, d2, s, c) in enumerate(zip(depth_maps, depth_maps2, scores, counts)):
    #  plt.imshow(c.cpu())
    #  plt.show()
    fig, axs = plt.subplots(2, 2)
    axs[0, 0].imshow(s.cpu() / torch.where(c.cpu() > 0, c.cpu(), torch.ones_like(c.cpu())))
    #  axs[0, 0].imshow(s.cpu())
    axs[0, 0].set_xlabel("Score")
    axs[0, 1].imshow(c.cpu())
    axs[0, 1].set_xlabel("Count")
    axs[1, 0].imshow(d.cpu())
    axs[1, 0].set_xlabel("Depth")
    #  axs[1, 1].imshow(d.cpu() - d2.cpu())
    axs[1, 1].imshow(d2.cpu())
    axs[1, 1].set_xlabel("Render Depth Diff")
    plt.show()
