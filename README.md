Tree must fit on the GPU for it to be rendered on the GPU.
Right now, `octreevis` uses OpenCL while the python install uses CUDA.
# Python Install
```
pip install .
```
# C++ Install
```
mkdir build
cd build
cmake -D ENABLE_VIS=ON -D ENABLE_OPENCL=ON -D ENABLE_CUDA=ON ..
make -j8
```

there was a memory leak in my c++ code, and the dead threads containing it are because OMP was linked. Unlinking OMP removed those dead threads but not the memory
changing the return policy for the pytorch tensors from automatic to take ownership fixed it
