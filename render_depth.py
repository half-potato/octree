import cv2
import torch
import pyoctree

import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np
import json
from pyquaternion import Quaternion

def read_pose(path):
    with open(path, "r") as f:
        dat = json.load(f)
        ori = dat["pitchyawroll"]
        pos = np.array(dat["position"])*0.01
        pos[2] *= -1
        q = Quaternion(axis=[0,0,1], degrees=ori[1]) * \
            Quaternion(axis=[0,1,0], degrees=ori[0])
        corr = np.array([[0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [1, 0, 0, 0],
                         [0, 0, 0, 1]]).T
        ret = q.transformation_matrix.dot(corr)
        ret[:3, 3] = pos
        return ret

def resize_with_crop(image, shape):
    #  scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    #  image = cv2.resize(image,
    #                     (int(scale*image.shape[0]), int(scale*image.shape[1])),
    #                     interp)
    (h, w) = image.shape[:2]
    (nh, nw) = shape[:2]
    oh, ow = (h-nh)//2, (w-nw)//2
    return image[oh:oh+nh, ow:ow+nw]

def scale_then_crop(image, shape, interp=cv2.INTER_LINEAR):
    #  image = cv2.resize(image, (max(shape), max(shape)), interp)
    scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    image = cv2.resize(image,
                       (int(scale*image.shape[1]), int(scale*image.shape[0])),
                       interp)

    image = resize_with_crop(image, shape)
    return image

def convert_depth(calib, point_depth):
    H = point_depth.shape[0]
    W = point_depth.shape[1]
    #  fov = 67.5
    #  f = H / (2 * math.tan(fov * math.pi / 360))

    fx, fy, cx, cy = calib
    #  f = 360
    #  i_c = np.float(H) / 2 - 1
    #  j_c = np.float(W) / 2 - 1
    columns, rows = np.meshgrid(np.linspace(0, W-1, num=W), np.linspace(0, H-1, num=H))
    y = (rows - cy)/fy
    x = (columns - cx)/fx
    z = 1
    dist_to_plane = (x**2 + y**2 + z**2)**(0.5)
    #  depth = f * point_depth / np.sqrt(f**2 + (rows - i_c)**2 + (columns - j_c)**2)
    depth = point_depth/dist_to_plane
    return depth

octree_path = "/data/unrealcv/octrees/ArchVisInterior.oct"
h = w = 480
calib = list(np.array([320, 320, 240, 240])*h/480)
ds_path = Path("/data/unrealcv/images/ArchVisInterior/")

feature_maps = []
pose_tensors = []
depth_maps = []
for i in range(0, 10):
    raw_dep = cv2.imread(str(ds_path / f"{i:04d}_depth.exr"), -1)
    conv_dep = convert_depth(calib, scale_then_crop(raw_dep, (h, w), cv2.INTER_NEAREST))
    depth_maps.append(torch.tensor(conv_dep).float())

    pose = torch.tensor(read_pose(ds_path / f"{i:04d}_pose.json")).float()
    pose_tensors.append(pose)

depth_maps_r = pyoctree.render_depth(pose_tensors, h, w, octree_path, calib)

for depth_map_r, depth_map in zip(depth_maps_r, depth_maps):
    fig, axs = plt.subplots(2, 1)
    diff = depth_map-depth_map_r
    diff[diff>1e2] = 0
    axs[0].imshow(diff)
    axs[1].imshow(depth_map_r)
    plt.show()
