import time

import torch
import pyoctree
import numpy as np
import torch.nn.functional as F

from pathlib import Path
from icecream import ic
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import re

with open("points2.csv", "r") as f:
    points = []
    center = None
    for line in f.readlines():
        line = line.strip()
        if not line:
            continue
        groups = re.findall(r'([0-9]*\.[0-9]*),? ?', line)
        if len(groups) < 6:
            continue
        vs = [float(v) for v in groups]
        center = vs[:3]
        points.append(vs[3:6])
    points = torch.tensor(points)
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(points[:, 0], points[:, 1], points[:, 2], c='green')
ax.scatter(center[0], center[1], center[2], c='red', s=60)
plt.show()


torch.set_printoptions(linewidth=200, threshold=2000, precision=3)

radius = 0.02
res = 0.01

rng = torch.linspace(-radius, radius, steps=int(radius*2/res)+1)
locs = torch.stack(torch.meshgrid(rng, rng, rng), dim=3).reshape(-1, 3)
print(locs)
locs = locs[torch.norm(locs, dim=1) < radius]
N = locs.shape[0]
offset = torch.tensor([2*radius, 0, 0]).reshape(1, 3)
locs = torch.cat([locs, locs+offset], dim=0)

size = torch.ones((2*N, 1))
score = torch.zeros((2*N, 1))
sel = torch.where((locs[:, 0] == 0) & (locs[:, 1] == 0) & (locs[:, 2] == 0))[0][0]
print(sel)
score[sel] = 1
score[sel+N] = 1
points = torch.cat([locs, score, size, size], dim=1)
out_points = pyoctree.neighborhood_sparsity_enforcement(points, 1.1*radius, 0, res/4)
dist = torch.norm(out_points[:, :3], dim=1, keepdim=True)
dist2 = torch.norm(out_points[:, :3] - offset, dim=1, keepdim=True)
ic(points.shape, out_points.shape, sel, points)
ic(points[sel], points[sel+N])
print(torch.cat([out_points, dist, dist2], dim=1))

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(points[:, 0], points[:, 1], points[:, 2], c='green')
ax.scatter(out_points[:, 0], out_points[:, 1], out_points[:, 2], c='red', s=100)
plt.show()
